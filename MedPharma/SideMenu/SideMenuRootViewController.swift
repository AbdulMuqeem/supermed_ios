//
//  SideMenuRootViewController.swift
//  Syllabit
//
//  Created by Protege Global on 21/03/2018.
//  Copyright © 2018 Protege Global. All rights reserved.
//

import UIKit
import LGSideMenuController

class SideMenuRootViewController: LGSideMenuController {

    class func instantiateFromStoryboard() -> SideMenuRootViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SideMenuRootViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.leftViewController = SideMenuViewController.instantiateFromStoryboard()
        self.leftViewWidth = 275.0
        let root = RootViewController.instantiateFromStoryboard()
        let vc = HomeViewController.instantiateFromStoryboard()
        root.viewControllers = [ vc ]
        self.rootViewController = root
    }

}
