//
//  SideMenuViewController.swift
//  Super
//
//  Created by Protege Global on 21/03/2018.
//  Copyright © 2018 Protege Global. All rights reserved.
//

import UIKit
import LGSideMenuController

class SideMenuViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SideMenuViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SideMenuViewController
    }
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    
    let defaultIndex = UserDefaults.standard
    
    
    var items: [String] = [ "HOME", "ALTERNATIVE MEDICINE","UPLOAD PRESCRIPTION","CHECK OUT", "CATEGORIES", "ORDER HISTORY" , "SETTINGS" , "NEWS LETTERS" , "HELP CENTER" , "TERMS & CONDITIONS","CONTACT US","JOIN US","LOG OUT"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.lightGreen
        
        let nib = UINib(nibName: "SideMenuTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "SideMenuTableViewCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateUser(_:)), name: .NOTIFICATION_USERUPDATE, object: nil)
        
        self.updatePreferrenceData()
        
    }
    
    
    @objc func updateUser(_ notification :Notification) {
        self.updatePreferrenceData()
    }
    
    //MARK:-  Get All Syllabus Listing
    func updatePreferrenceData(){
        
        if Singleton.sharedInstance.CurrentUser != nil {
            
            self.lblName.text = Singleton.sharedInstance.CurrentUser!.first_name
            
            let userDefaults = UserDefaults.standard
            let user = userDefaults.bool(forKey: "AsGuest")
            print(user)
            
            if user {
                self.lblEmail.text = ""
            } else {
                self.lblEmail.text = Singleton.sharedInstance.CurrentUser?.email
            }
            
            guard let imageURL = URL(string:(Singleton.sharedInstance.CurrentUser?.image) ?? "www.google.com") else {return}
            self.imgUser.af_setImage(withURL: imageURL)
        }
    }
    
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 48)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell: SideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell
        
        
        let defaultIndex = UserDefaults.standard
        let row = defaultIndex.integer(forKey: "indexCell")
        
        let rowToSelect:NSIndexPath = NSIndexPath(row: row, section: 0)
        tableView.selectRow(at: rowToSelect as IndexPath, animated: true, scrollPosition: UITableViewScrollPosition.none)
        
        let userDefaults = UserDefaults.standard
        let user = userDefaults.bool(forKey: "AsGuest")
        print(user)
        
        if user {
            if indexPath.row == 12 {
                cell.lblTitle.isHidden = false
                cell.lblTitle.text = "LOG IN"
            }
            else {
                cell.lblTitle.isHidden = false
                cell.lblTitle.text = self.items[indexPath.row]
            }
            
        } else {
            cell.lblTitle.isHidden = false
            cell.lblTitle.text = self.items[indexPath.row]
        }
        
        //        if indexPath.row == 12 {
        //          cell.lblTitle.isHidden = false
        //          cell.lblTitle.text = self.items[indexPath.row]
        //        }
        //        else {
        //            cell.lblTitle.isHidden = false
        //            cell.lblTitle.text = self.items[indexPath.row]
        //        }
        
        if indexPath.row == row {
            
            cell.backgroundColor = UIColor.navColor
        }else{
            
            cell.backgroundColor = UIColor.sideMenu
            
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let cell = tableView.cellForRow(at: indexPath) as! SideMenuTableViewCell
        cell.indexPath = indexPath
        defaultIndex.set(indexPath.row, forKey:"indexCell")
        
        cell.backgroundColor = UIColor.navColor
        
        tableView.reloadData()
        
        switch indexPath.row {
        case 0:
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = HomeViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        case 1:
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = SearchViewController.instantiateFromStoryboard()
            vc.from = "menu"
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        case 2:
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = UploadPrescViewController.instantiateFromStoryboard()
            //            navigationController.setViewControllers([vc], animated: false)
            navigationController.pushViewController(vc, animated: true)
            self.hideLeftViewAnimated(self)
            
        case 3:
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = CartViewController.instantiateFromStoryboard()
            vc.from = "SideMenu"
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        case 4:
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = CategoriesViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        case 5:
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = OrderHistoryViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        case 6:
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = MainSettingsViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        case 7:
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = NewslettersViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        case 8:
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = HelpCenterViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        case 9:
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = TermsAndCondViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        case 10:
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = ContactUsViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        case 11:
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = JoinUsViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        case 12:
            DispatchQueue.main.async {
                
                let userDefaults = UserDefaults.standard
                let user = userDefaults.bool(forKey: "AsGuest")
                print(user)
                
                if user {
                    let alertController = UIAlertController (title: "Alert", message: "Are you sure you want to Login?", preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
                    let doneAction = UIAlertAction(title: "Yes", style: .default) { (_) -> Void in
                        UserDefaults.standard.removeObject(forKey: User_data_userDefault)
                        UserDefaults.standard.synchronize()
                        Singleton.sharedInstance.CurrentUser = nil
                        
                        let nav = RootViewController.instantiateFromStoryboard()
                        AppDelegate.getInstatnce().window?.rootViewController = nav
                        let LoginVC = LoginViewController.instantiateFromStoryboard()
                        nav.pushViewController(LoginVC, animated: true)
                        self.hideLeftViewAnimated(self)
                    }
                    alertController.addAction(cancelAction)
                    alertController.addAction(doneAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    let alertController = UIAlertController (title: "Alert", message: "Are you sure you want to Logout?", preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
                    let doneAction = UIAlertAction(title: "Yes", style: .default) { (_) -> Void in
                        UserDefaults.standard.removeObject(forKey: User_data_userDefault)
                        UserDefaults.standard.removeObject(forKey: "Social_Login")
                        UserDefaults.standard.synchronize()
                        Singleton.sharedInstance.CurrentUser = nil
                        
                        let nav = RootViewController.instantiateFromStoryboard()
                        AppDelegate.getInstatnce().window?.rootViewController = nav
                        let LoginVC = LoginViewController.instantiateFromStoryboard()
                        nav.pushViewController(LoginVC, animated: true)
                        self.hideLeftViewAnimated(self)
                    }
                    alertController.addAction(cancelAction)
                    alertController.addAction(doneAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                
               // let alertController = UIAlertController (title: "Alert", message: "Are you sure you want to Logout?", preferredStyle: .alert)
//                let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
//                let doneAction = UIAlertAction(title: "Yes", style: .default) { (_) -> Void in
//                    UserDefaults.standard.removeObject(forKey: User_data_userDefault)
//                    UserDefaults.standard.synchronize()
//                    Singleton.sharedInstance.CurrentUser = nil
//
//                    let nav = RootViewController.instantiateFromStoryboard()
//                    AppDelegate.getInstatnce().window?.rootViewController = nav
//                    let LoginVC = LoginViewController.instantiateFromStoryboard()
//                    nav.pushViewController(LoginVC, animated: true)
//                    self.hideLeftViewAnimated(self)
//                }
//                alertController.addAction(cancelAction)
//                alertController.addAction(doneAction)
//                self.present(alertController, animated: true, completion: nil)
            }
        default:
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = TermsAndCondViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            return
        }
        
    }
}



