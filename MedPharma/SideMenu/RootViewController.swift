//
//  RootViewController.swift
//  Syllabit
//
//  Created by Protege Global on 21/03/2018.
//  Copyright © 2018 Protege Global. All rights reserved.
//

import UIKit

class RootViewController: UINavigationController {

    class func instantiateFromStoryboard() -> RootViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! RootViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
