//
//  SideMenuTableViewCell.swift
//  Syllabit
//
//  Created by Protege Global on 21/03/2018.
//  Copyright © 2018 Protege Global. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    
    let defaultIndex = UserDefaults.standard

 
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrivacy: UILabel!
    
    var indexPath : IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
          imageView?.image = nil
          self.backgroundColor = UIColor.sideMenu
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
