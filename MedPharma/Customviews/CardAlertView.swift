//
//  CardAlertView.swift
//  MedPharma
//
//  Created by Protege Global on 28/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

class CardAlertView: UIView, UITextFieldDelegate {

    @IBOutlet weak var txtFieldCardNo: UITextField!
    @IBOutlet weak var txtFieldExpMonth: UITextField!
    @IBOutlet weak var txtFieldExpYear: UITextField!
    @IBOutlet weak var txtFieldCvc: UITextField!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var lblValid: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib ()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    @IBAction func didTapOnClose(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    func loadViewFromNib() {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CardAlertView", bundle: bundle)
        let view =  nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7);
        self.addSubview(view);
        lblValid.isHidden = true
        lblWarning.isHidden = true
        txtFieldExpMonth.isEnabled = false
        txtFieldExpYear.isEnabled = false
        txtFieldCvc.isEnabled = false
        txtFieldCardNo.delegate = self
    }
    
    var maxNumberOfCharacters = 16
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // only allow numerical characters
        guard string.characters.flatMap({ Int(String($0)) }).count ==
            string.characters.count else { return false }
        let text = textField.text ?? ""
        if string.characters.count == 0 {
            txtFieldExpMonth.isEnabled = false
            txtFieldExpYear.isEnabled = false
            txtFieldCvc.isEnabled = false
            lblWarning.isHidden = false
            lblValid.isHidden = true
            textField.text = String(text.characters.dropLast()).chunkFormatted()
        }
            
        else {
            let newText = String((text + string).characters
                .filter({ $0 != "-" }).prefix(maxNumberOfCharacters))
            

            txtFieldExpMonth.isEnabled = true
            txtFieldExpYear.isEnabled = true
            txtFieldCvc.isEnabled = true
            lblValid.isHidden = false
            txtFieldExpMonth.isEnabled = true
            lblWarning.isHidden = true
            textField.text = newText.chunkFormatted()
        }
        return false
    }
    
    @IBAction func didTapOnPay(_ sender: Any) {
        if !(txtFieldCardNo.text?.isEmpty)! && !(txtFieldExpMonth.text?.isEmpty)! && !(txtFieldExpYear.text?.isEmpty)! && !(txtFieldCvc.text?.isEmpty)!{
            Alert.showToast(message: "Payment has been made successfully")
            self.removeFromSuperview()
        }else{
            Alert.showAlert(title: "Warning", message: "Please provide all the necessary information")
            self.removeFromSuperview()
        }
    }
    
}

extension Collection {
    
    public func chunk(n: IndexDistance) -> [SubSequence] {
        var res: [SubSequence] = []
        var i = startIndex
        var j: Index
        while i != endIndex {
            j = index(i, offsetBy: n, limitedBy: endIndex) ?? endIndex
            res.append(self[i..<j])
            i = j
        }
        return res
    }
}

extension String {
    func chunkFormatted(withChunkSize chunkSize: Int = 4,
                        withSeparator separator: Character = "-") -> String {
        return characters.filter { $0 != separator }.chunk(n: chunkSize)
            .map{ String($0) }.joined(separator: String(separator))
    }
}




