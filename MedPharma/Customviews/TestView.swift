//
//  TestView.swift
//  MedPharma
//
//  Created by Protege Global on 10/08/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

protocol TestsViewDelegate {
    func deleteTest(id: String, index: Int)
}


class TestView: UIView {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_test_title: UILabel!
    @IBOutlet weak var lbl_test_price: UILabel!
    
    var delegate : TestsViewDelegate?
    var testID : String?
    var index: Int?
    class func instanceFromNib() -> TestView {
        return UINib(nibName: "TestView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! TestView
    }
    
    @IBAction func didTappedOnDelete(_ sender: UIButton) {
        delegate?.deleteTest(id: testID!, index: index!)
        
    }
    
    
    
  
}
