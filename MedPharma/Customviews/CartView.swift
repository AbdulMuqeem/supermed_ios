//
//  CartView.swift
//  MedPharma
//
//  Created by Ayaz Akbar on 8/2/18.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import SwiftyJSON
import NotificationBannerSwift


protocol CartViewDelegate {
    func reloadServerData()
}

protocol AlertPresenting {
    func removeCart()
}


class CartView: UIView {
    
    var delegate: CartViewDelegate?
    
    
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var lblCounter: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    var product_id : String?
    
    @IBAction func didTapOnMinus(_ sender: Any) {
        let myInt2 = (lblCounter.text! as NSString).integerValue
        if var i  =  myInt2 as? Int{
            if i <= 0 {return}
            i = i - 1
            lblCounter.text = "\(i)"
            if i == 0{
                let message = "Do you want to remove the product from cart?"
                Alert.showAlert(title: "Warning", message: message, RemoveActionHandler: { (Bool) in
                    self.updateCartWithQty(counter: self.lblCounter.text!, product_id: self.product_id!, action: "2")
                }) { (Bool) in
                    i = 0
                    self.lblCounter.text = "1"
                    return
                }
            }
            else{
                updateCartWithQty(counter: lblCounter.text!, product_id: product_id!, action: "1")
            }
        }
        print("minus")
    }
    
    @IBAction func didTapOnAdd(_ sender: Any) {
        print("add")
        let myInt2 = (lblCounter.text! as NSString).integerValue
        if var i  =  myInt2 as? Int{
            i = i + 1
            lblCounter.text = "\(i)"
            updateCartWithQty(counter: lblCounter.text!, product_id: product_id!, action: "1")
        }
    }
    
    class func instanceFromNib() -> CartView {
        return UINib(nibName: "CartView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CartView
    }
    
    func updateCartWithQty(counter: String, product_id: String, action: String){
        Alert.showLoader(message: "")
        let param : [String: Any] = ["userid": UserManager.getUserObjectFromUserDefaults()!.id!, "productid": product_id, "qty": lblCounter.text!, "action": action]
    
        print(param)
        
        SuperMedServices.cart_action(param: param, completionHandler: {(success, response, error) in
            Alert.hideLoader()
            if !success {
                print(response)
                 let message = response!["message"].stringValue
                let banner = NotificationBanner(title: "Success", subtitle: "\(message)", style: .danger)
                banner.show()
               self.delegate?.reloadServerData()
                return
            }else{
                
                print("response: \(response!)")
                let message = response!["message"].stringValue
                
                let banner = NotificationBanner(title: "Success", subtitle: "\(message)", style: .success)
                banner.show()
                self.delegate?.reloadServerData()
                
                
            }
        })
    }
}

