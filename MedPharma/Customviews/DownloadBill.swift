//
//  AlertPassword.swift
//  MedPharma
//
//  Created by Protege Global on 29/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import SwiftyJSON
import SwiftValidator

class DownloadBill: UIView {
    @IBOutlet weak var viewForgotPassword: UIView!
    @IBOutlet weak var btnDownloadBill: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib ()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    @IBAction func didTapOnClose(_ sender: UIButton) {
        self.removeFromSuperview()
    }
    
    func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "DownloadBill", bundle: bundle)
        let view =  nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7);
        self.addSubview(view);
    }
}


