//
//  AlertPassword.swift
//  MedPharma
//
//  Created by Protege Global on 29/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import SwiftyJSON
import SwiftValidator

class AlertPassword: UIView, ValidationDelegate {
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var viewForgotPassword: UIView!
    @IBOutlet weak var viewAlert: UIView!
    @IBOutlet weak var txtOldPassword: UITextField!
    
    let validator = Validator()
    @IBOutlet weak var txtReEnterd: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib ()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    
    @IBAction func didTapOnClose(_ sender: UIButton) {
        self.removeFromSuperview()
    }
    
    @IBAction func didTappedOnChangePassword(_ sender: Any) {
        print("did tapped")
    }
    
    
    func validationSuccessful() {
        // submit the form
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
    
    func forgotPassword(param: Dictionary<String,Any>){
        
        UserServices.forgotPassword(param: param, completionHandler: { (success, response, error) in
            if success{
                print("message:\(String(describing: response))")
                let message = response!["message"].stringValue
                self.removeFromSuperview()
                Alert.showAlert(title: "Email", message: "\(message)")
                
            }
            self.removeFromSuperview()
            let message = response!["message"].stringValue
            Alert.showAlert(title: "Email", message: "\(message)")
        })
    }
    
    func changePassword(){
        guard let password = self.txtNewPassword.text, password  != "" else {
            Alert.showAlert(title: "New Password", message: "Please enter new password")
//            let banner = NotificationBanner(title: "New Password", subtitle: "Please enter new password", style: .danger)
//            banner.show()
            return
        }
        guard let passwordRe_entered = self.txtReEnterd.text, passwordRe_entered  != "", password == passwordRe_entered else {
            Alert.showAlert(title: "Re-entered password", message: "Please enter Correct password")
//            let banner = NotificationBanner(title: "Re-entered password", subtitle: "Please enter Correct password", style: .danger)
//            banner.show()
            return
        }
        // userid,old_pass,new_pass,con_pass
        let param = ["userid": UserManager.getUserObjectFromUserDefaults()!.id!, "old_pass": txtOldPassword.text!,"new_pass": password, "con_pass": passwordRe_entered]
        print(param)
        
        UserServices.ChangePassword(param: param, completionHandler: {(success, response, error) in
            if !success{
                print(response)
                return
                
            }else{
                Alert.showToast(message: "Password Changed Successfully")
                self.removeFromSuperview()
                print("response:\(response!)")
            }
        })
    }
    
    func loadViewFromNib() {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "AlertPassword", bundle: bundle)
        let view =  nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7);
        self.addSubview(view);
        //cusotmiseView()
        customizationViews()
    }
    func customizationViews(){
        viewAlert.layer.applySketchShadow(y: 0)
        txtEmail.addPadding(.left(8))
        txtOldPassword.addPadding(.left(8))
        txtReEnterd.addPadding(.left(8))
        txtNewPassword.addPadding(.left(8))
        
       
        
       
//        validator.registerField(txtNewPassword, rules:[PasswordRuleCustom(message: "pleae")])
//        validator.registerField(txtOldPassword, rules: [PasswordRule(message: "Please Enter Old Password")])
//        validator.registerField(txtReEnterd, rules: [PasswordRule(message: "password must be same")])
//
//
        
        
    }
    
    
    
    @IBAction func didTappedOnResetPassword(_ sender: UIButton) {
        
        guard let email = self.txtEmail.text, email != "", AppHelper.isValidEmail(testStr: email) else {
            let banner = NotificationBanner(title: "Email", subtitle: "Please enter valid email", style: .danger)
            banner.show()
            return
        }
        
        let param = ["email": email]
        
        self.forgotPassword(param: param)
    }
    
    
    
    @IBAction func didTapOnCloseView(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func didTapOnChangePassword(_ sender: Any) {
        validator.validate(self)
        changePassword()
    }
    
}


