//
//  HomeCollectionCell.swift
//  MedPharma
//
//  Created by Protege Global on 20/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell {

    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var image: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
}
