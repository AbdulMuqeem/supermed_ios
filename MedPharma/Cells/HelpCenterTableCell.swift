//
//  HelpCenterTableCell.swift
//  MedPharma
//
//  Created by Protege Global on 21/08/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class HelpCenterTableCell: UITableViewCell {
    
    @IBOutlet weak var lblTopics: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
