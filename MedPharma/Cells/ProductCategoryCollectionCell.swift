//
//  ProductCategoryCollectionCell.swift
//  MedPharma
//
//  Created by Protege Global on 09/08/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class ProductCategoryCollectionCell: UICollectionViewCell {
    @IBOutlet weak var lblTitleProduct: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var btnAddToCart: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func didTappedOnAddCart(_ sender: UIButton) {
        
    }
   
}
