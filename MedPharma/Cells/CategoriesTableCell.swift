//
//  CategoriesTableCell.swift
//  MedPharma
//
//  Created by Protege Global on 26/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class CategoriesTableCell: UITableViewCell {

    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
