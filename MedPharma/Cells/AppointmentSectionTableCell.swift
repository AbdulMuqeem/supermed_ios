//
//  AppointmentSectionTableCell.swift
//  MedPharma
//
//  Created by Protege Global on 27/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class AppointmentSectionTableCell: UITableViewCell {

    @IBOutlet weak var lblAppointmentNo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.contentView.addSubview(lblAppointmentNo)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
