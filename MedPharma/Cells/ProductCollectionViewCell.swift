//
//  ProductCollectionViewCell.swift
//  MedPharma
//
//  Created by Protege Global on 25/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

protocol ProductCellDelegate{
    func deleteWishlistItem(product_id: String)
    func addToCartList(product_id: String)
}


class ProductCollectionViewCell: UICollectionViewCell {

    @IBAction func didTappedOnDelete(_ sender: Any) {
        
        print("Product_id:\(productId!)")
        delegate?.deleteWishlistItem(product_id: productId!)
        
    }
    @IBOutlet weak var image_product: UIImageView!
    @IBOutlet weak var lbl_productTitle: UILabel!
    @IBOutlet weak var lbl_productPrice: UILabel!
    @IBOutlet weak var lbl_productDisc: PriceLabel!
    var delegate: ProductCellDelegate?
    var productId : String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func didTappedOnAddToCart(_ sender: Any) {
        print(productId!)
        delegate?.addToCartList(product_id: productId!)
        
        
    }
    
    
    
    
    
}
