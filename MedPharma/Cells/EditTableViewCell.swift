//
//  EditTableViewCell.swift
//  MedPharma
//
//  Created by Protege Global on 25/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class EditTableViewCell: UITableViewCell {

    @IBOutlet weak var btnAddNewDesign: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        customizationView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func didTapOnEdit(_ sender: UIButton){
        print("Tapped on Edit")
        
    }
    
    @IBAction func didTapOnDesign(_ sender: UIButton){
        print("didTapped On Design")
        
    }
    
    
    func customizationView(){
        
//        btnEdit.addDashedBorder(strokeColor: UIColor.navColor, lineWidth: 1.0)
//        btnAddNewDesign.addDashedBorder(strokeColor: UIColor.navColor, lineWidth: 1.0)
    }
    
    
    

}
