//
//  EmergencyTableViewCell.swift
//  MedPharma
//
//  Created by Protege Global on 27/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class EmergencyTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageTitle: UIImageView!

}
