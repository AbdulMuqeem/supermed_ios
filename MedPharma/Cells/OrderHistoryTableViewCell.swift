//
//  OrderHistoryTableViewCell.swift
//  MedPharma
//
//  Created by Protege Global on 28/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class OrderHistoryTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblOrderNo: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblOrderNote: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
