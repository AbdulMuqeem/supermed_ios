//
//  AddressTableCell.swift
//  MedPharma
//
//  Created by Protege Global on 25/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

protocol AddressDelegate {
    func saveAddress(address: String)
    func saveContact(contact: String)
}


class AddressTableCell: UITableViewCell {

    
    var delegate: AddressDelegate?
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblMobileNum: UILabel!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var txtMobile: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtView.delegate = self
        txtMobile.delegate = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    
}

extension AddressTableCell: UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {
       
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
       
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
        }else{
            delegate?.saveAddress(address: txtView.text!)
        }
    }
}
extension AddressTableCell: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        if txtMobile.text == ""{
            
        }else{
            delegate?.saveContact(contact: txtMobile.text!)
        }
    }
    
    
}
