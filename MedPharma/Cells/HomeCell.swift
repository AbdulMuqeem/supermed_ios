//
//  HomeCell.swift
//  MedPharma
//
//  Created by Protege Global on 20/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

protocol AddtoCart {
    func tapToCart()
}

var delegate: AddtoCart?

class HomeCell: UICollectionViewCell {

    @IBOutlet weak var imageProduct: UIImageView!
    
    @IBOutlet weak var lblTitleProduct: UILabel!
    
    @IBOutlet weak var lbl_product_price: UILabel!
    
    @IBOutlet weak var lbl_product_oprice: UILabel!
    
    @IBOutlet weak var add_to_cart: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBAction func didTapOnAddToCart(_ sender: Any) {
        delegate?.tapToCart()
    }
    
}
