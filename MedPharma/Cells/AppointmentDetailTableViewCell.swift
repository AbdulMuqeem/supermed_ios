//
//  AppointmentDetailTableViewCell.swift
//  MedPharma
//
//  Created by Protege Global on 27/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class AppointmentDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var lblLaboratoryName: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
    
}
