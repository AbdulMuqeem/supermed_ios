//
//  User.swift
//
//  Created by Protege Global on 22/03/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

import Foundation
import SwiftyJSON

public final class User: NSObject, NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let email = "Email"
        static let birthdate = "Birthdate"
        static let gender = "Gender"
        static let insertDate = "InsertDate"
        static let isActive = "IsActive"
        static let misc = "Misc"
        static let deviceToken = "DeviceToken"
        static let quiz = "Quiz"
        static let uniCollHighs = "UniCollHighs"
        static let review = "Review"
        static let name = "Name"
        static let test = "Test"
        static let socialmediaid = "socialmediaid"
        static let isSocialMediaUser = "IsSocialMediaUser"
        static let image = "image"
        static let userID = "UserID"
        static let password = "Password"
        static let homework = "Homework"
    }
    
    // MARK: Properties
    public var email: String?
    public var birthdate: String?
    public var gender: String?
    public var insertDate: String?
    public var isActive: String?
    public var misc: String?
    public var deviceToken: String?
    public var quiz: String?
    public var uniCollHighs: String?
    public var review: String?
    public var name: String?
    public var test: String?
    public var socialmediaid: String?
    public var isSocialMediaUser: String?
    public var image: String?
    public var userID: String?
    public var password: String?
    public var homework: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        email = json[SerializationKeys.email].string
        birthdate = json[SerializationKeys.birthdate].string
        gender = json[SerializationKeys.gender].string
        insertDate = json[SerializationKeys.insertDate].string
        isActive = json[SerializationKeys.isActive].string
        misc = json[SerializationKeys.misc].string
        deviceToken = json[SerializationKeys.deviceToken].string
        quiz = json[SerializationKeys.quiz].string
        uniCollHighs = json[SerializationKeys.uniCollHighs].string
        review = json[SerializationKeys.review].string
        name = json[SerializationKeys.name].string
        test = json[SerializationKeys.test].string
        socialmediaid = json[SerializationKeys.socialmediaid].string
        isSocialMediaUser = json[SerializationKeys.isSocialMediaUser].string
        image = json[SerializationKeys.image].string
        userID = json[SerializationKeys.userID].string
        password = json[SerializationKeys.password].string
        homework = json[SerializationKeys.homework].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = birthdate { dictionary[SerializationKeys.birthdate] = value }
        if let value = gender { dictionary[SerializationKeys.gender] = value }
        if let value = insertDate { dictionary[SerializationKeys.insertDate] = value }
        if let value = isActive { dictionary[SerializationKeys.isActive] = value }
        if let value = misc { dictionary[SerializationKeys.misc] = value }
        if let value = deviceToken { dictionary[SerializationKeys.deviceToken] = value }
        if let value = quiz { dictionary[SerializationKeys.quiz] = value }
        if let value = uniCollHighs { dictionary[SerializationKeys.uniCollHighs] = value }
        if let value = review { dictionary[SerializationKeys.review] = value }
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = test { dictionary[SerializationKeys.test] = value }
        if let value = socialmediaid { dictionary[SerializationKeys.socialmediaid] = value }
        if let value = isSocialMediaUser { dictionary[SerializationKeys.isSocialMediaUser] = value }
        if let value = image { dictionary[SerializationKeys.image] = value }
        if let value = userID { dictionary[SerializationKeys.userID] = value }
        if let value = password { dictionary[SerializationKeys.password] = value }
        if let value = homework { dictionary[SerializationKeys.homework] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
        self.birthdate = aDecoder.decodeObject(forKey: SerializationKeys.birthdate) as? String
        self.gender = aDecoder.decodeObject(forKey: SerializationKeys.gender) as? String
        self.insertDate = aDecoder.decodeObject(forKey: SerializationKeys.insertDate) as? String
        self.isActive = aDecoder.decodeObject(forKey: SerializationKeys.isActive) as? String
        self.misc = aDecoder.decodeObject(forKey: SerializationKeys.misc) as? String
        self.deviceToken = aDecoder.decodeObject(forKey: SerializationKeys.deviceToken) as? String
        self.quiz = aDecoder.decodeObject(forKey: SerializationKeys.quiz) as? String
        self.uniCollHighs = aDecoder.decodeObject(forKey: SerializationKeys.uniCollHighs) as? String
        self.review = aDecoder.decodeObject(forKey: SerializationKeys.review) as? String
        self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
        self.test = aDecoder.decodeObject(forKey: SerializationKeys.test) as? String
        self.socialmediaid = aDecoder.decodeObject(forKey: SerializationKeys.socialmediaid) as? String
        self.isSocialMediaUser = aDecoder.decodeObject(forKey: SerializationKeys.isSocialMediaUser) as? String
        self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
        self.userID = aDecoder.decodeObject(forKey: SerializationKeys.userID) as? String
        self.password = aDecoder.decodeObject(forKey: SerializationKeys.password) as? String
        self.homework = aDecoder.decodeObject(forKey: SerializationKeys.homework) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(email, forKey: SerializationKeys.email)
        aCoder.encode(birthdate, forKey: SerializationKeys.birthdate)
        aCoder.encode(gender, forKey: SerializationKeys.gender)
        aCoder.encode(insertDate, forKey: SerializationKeys.insertDate)
        aCoder.encode(isActive, forKey: SerializationKeys.isActive)
        aCoder.encode(misc, forKey: SerializationKeys.misc)
        aCoder.encode(deviceToken, forKey: SerializationKeys.deviceToken)
        aCoder.encode(quiz, forKey: SerializationKeys.quiz)
        aCoder.encode(uniCollHighs, forKey: SerializationKeys.uniCollHighs)
        aCoder.encode(review, forKey: SerializationKeys.review)
        aCoder.encode(name, forKey: SerializationKeys.name)
        aCoder.encode(test, forKey: SerializationKeys.test)
        aCoder.encode(socialmediaid, forKey: SerializationKeys.socialmediaid)
        aCoder.encode(isSocialMediaUser, forKey: SerializationKeys.isSocialMediaUser)
        aCoder.encode(image, forKey: SerializationKeys.image)
        aCoder.encode(userID, forKey: SerializationKeys.userID)
        aCoder.encode(password, forKey: SerializationKeys.password)
        aCoder.encode(homework, forKey: SerializationKeys.homework)
    }
}

