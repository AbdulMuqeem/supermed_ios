//
//  Cart.swift
//
//  Created by mac  on 10/17/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Cart: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let productPrice = "ProductPrice"
    static let subCatId = "SubCatId"
    static let productQty = "ProductQty"
    static let userQty = "UserQty"
    static let subCatName = "SubCatName"
    static let cartQty = "cartQty"
    static let catName = "CatName"
    static let productImage = "ProductImage"
    static let catBriefIntro = "CatBriefIntro"
    static let catId = "CatId"
    static let productDescription = "ProductDescription"
    static let productSku = "ProductSku"
    static let productID = "ProductID"
    static let priceDate = "PriceDate"
    static let userid = "userid"
    static let productName = "ProductName"
    static let productTags = "ProductTags"
    static let subCatBriefIntro = "SubCatBriefIntro"
  }

  // MARK: Properties
  public var productPrice: String?
  public var subCatId: String?
  public var productQty: String?
  public var userQty: String?
  public var subCatName: String?
  public var cartQty: String?
  public var catName: String?
  public var productImage: String?
  public var catBriefIntro: String?
  public var catId: String?
  public var productDescription: String?
  public var productSku: String?
  public var productID: String?
  public var priceDate: String?
  public var userid: String?
  public var productName: String?
  public var productTags: String?
  public var subCatBriefIntro: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    productPrice = json[SerializationKeys.productPrice].string
    subCatId = json[SerializationKeys.subCatId].string
    productQty = json[SerializationKeys.productQty].string
    userQty = json[SerializationKeys.userQty].string
    subCatName = json[SerializationKeys.subCatName].string
    cartQty = json[SerializationKeys.cartQty].string
    catName = json[SerializationKeys.catName].string
    productImage = json[SerializationKeys.productImage].string
    catBriefIntro = json[SerializationKeys.catBriefIntro].string
    catId = json[SerializationKeys.catId].string
    productDescription = json[SerializationKeys.productDescription].string
    productSku = json[SerializationKeys.productSku].string
    productID = json[SerializationKeys.productID].string
    priceDate = json[SerializationKeys.priceDate].string
    userid = json[SerializationKeys.userid].string
    productName = json[SerializationKeys.productName].string
    productTags = json[SerializationKeys.productTags].string
    subCatBriefIntro = json[SerializationKeys.subCatBriefIntro].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = productPrice { dictionary[SerializationKeys.productPrice] = value }
    if let value = subCatId { dictionary[SerializationKeys.subCatId] = value }
    if let value = productQty { dictionary[SerializationKeys.productQty] = value }
    if let value = userQty { dictionary[SerializationKeys.userQty] = value }
    if let value = subCatName { dictionary[SerializationKeys.subCatName] = value }
    if let value = cartQty { dictionary[SerializationKeys.cartQty] = value }
    if let value = catName { dictionary[SerializationKeys.catName] = value }
    if let value = productImage { dictionary[SerializationKeys.productImage] = value }
    if let value = catBriefIntro { dictionary[SerializationKeys.catBriefIntro] = value }
    if let value = catId { dictionary[SerializationKeys.catId] = value }
    if let value = productDescription { dictionary[SerializationKeys.productDescription] = value }
    if let value = productSku { dictionary[SerializationKeys.productSku] = value }
    if let value = productID { dictionary[SerializationKeys.productID] = value }
    if let value = priceDate { dictionary[SerializationKeys.priceDate] = value }
    if let value = userid { dictionary[SerializationKeys.userid] = value }
    if let value = productName { dictionary[SerializationKeys.productName] = value }
    if let value = productTags { dictionary[SerializationKeys.productTags] = value }
    if let value = subCatBriefIntro { dictionary[SerializationKeys.subCatBriefIntro] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.productPrice = aDecoder.decodeObject(forKey: SerializationKeys.productPrice) as? String
    self.subCatId = aDecoder.decodeObject(forKey: SerializationKeys.subCatId) as? String
    self.productQty = aDecoder.decodeObject(forKey: SerializationKeys.productQty) as? String
    self.userQty = aDecoder.decodeObject(forKey: SerializationKeys.userQty) as? String
    self.subCatName = aDecoder.decodeObject(forKey: SerializationKeys.subCatName) as? String
    self.cartQty = aDecoder.decodeObject(forKey: SerializationKeys.cartQty) as? String
    self.catName = aDecoder.decodeObject(forKey: SerializationKeys.catName) as? String
    self.productImage = aDecoder.decodeObject(forKey: SerializationKeys.productImage) as? String
    self.catBriefIntro = aDecoder.decodeObject(forKey: SerializationKeys.catBriefIntro) as? String
    self.catId = aDecoder.decodeObject(forKey: SerializationKeys.catId) as? String
    self.productDescription = aDecoder.decodeObject(forKey: SerializationKeys.productDescription) as? String
    self.productSku = aDecoder.decodeObject(forKey: SerializationKeys.productSku) as? String
    self.productID = aDecoder.decodeObject(forKey: SerializationKeys.productID) as? String
    self.priceDate = aDecoder.decodeObject(forKey: SerializationKeys.priceDate) as? String
    self.userid = aDecoder.decodeObject(forKey: SerializationKeys.userid) as? String
    self.productName = aDecoder.decodeObject(forKey: SerializationKeys.productName) as? String
    self.productTags = aDecoder.decodeObject(forKey: SerializationKeys.productTags) as? String
    self.subCatBriefIntro = aDecoder.decodeObject(forKey: SerializationKeys.subCatBriefIntro) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(productPrice, forKey: SerializationKeys.productPrice)
    aCoder.encode(subCatId, forKey: SerializationKeys.subCatId)
    aCoder.encode(productQty, forKey: SerializationKeys.productQty)
    aCoder.encode(userQty, forKey: SerializationKeys.userQty)
    aCoder.encode(subCatName, forKey: SerializationKeys.subCatName)
    aCoder.encode(cartQty, forKey: SerializationKeys.cartQty)
    aCoder.encode(catName, forKey: SerializationKeys.catName)
    aCoder.encode(productImage, forKey: SerializationKeys.productImage)
    aCoder.encode(catBriefIntro, forKey: SerializationKeys.catBriefIntro)
    aCoder.encode(catId, forKey: SerializationKeys.catId)
    aCoder.encode(productDescription, forKey: SerializationKeys.productDescription)
    aCoder.encode(productSku, forKey: SerializationKeys.productSku)
    aCoder.encode(productID, forKey: SerializationKeys.productID)
    aCoder.encode(priceDate, forKey: SerializationKeys.priceDate)
    aCoder.encode(userid, forKey: SerializationKeys.userid)
    aCoder.encode(productName, forKey: SerializationKeys.productName)
    aCoder.encode(productTags, forKey: SerializationKeys.productTags)
    aCoder.encode(subCatBriefIntro, forKey: SerializationKeys.subCatBriefIntro)
  }

}
