//
//  Search.swift
//
//  Created by mac  on 10/18/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Search: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let productPrice = "ProductPrice"
    static let subCatId = "SubCatId"
    static let productQty = "ProductQty"
    static let subCatName = "SubCatName"
    static let catName = "CatName"
    static let productDescription = "ProductDescription"
    static let isCart = "is_cart"
    static let productImage = "ProductImage"
    static let catId = "CatId"
    static let productSku = "ProductSku"
    static let isWish = "is_wish"
    static let catBriefIntro = "CatBriefIntro"
    static let productID = "ProductID"
    static let priceDate = "PriceDate"
    static let productId = "product_id"
    static let productName = "ProductName"
    static let productTags = "ProductTags"
    static let subCatBriefIntro = "SubCatBriefIntro"
  }

  // MARK: Properties
  public var productPrice: String?
  public var subCatId: String?
  public var productQty: String?
  public var subCatName: String?
  public var catName: String?
  public var productDescription: String?
  public var isCart: String?
  public var productImage: String?
  public var catId: String?
  public var productSku: String?
  public var isWish: String?
  public var catBriefIntro: String?
  public var productID: String?
  public var priceDate: String?
  public var productId: String?
  public var productName: String?
  public var productTags: String?
  public var subCatBriefIntro: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    productPrice = json[SerializationKeys.productPrice].string
    subCatId = json[SerializationKeys.subCatId].string
    productQty = json[SerializationKeys.productQty].string
    subCatName = json[SerializationKeys.subCatName].string
    catName = json[SerializationKeys.catName].string
    productDescription = json[SerializationKeys.productDescription].string
    isCart = json[SerializationKeys.isCart].string
    productImage = json[SerializationKeys.productImage].string
    catId = json[SerializationKeys.catId].string
    productSku = json[SerializationKeys.productSku].string
    isWish = json[SerializationKeys.isWish].string
    catBriefIntro = json[SerializationKeys.catBriefIntro].string
    productID = json[SerializationKeys.productID].string
    priceDate = json[SerializationKeys.priceDate].string
    productId = json[SerializationKeys.productId].string
    productName = json[SerializationKeys.productName].string
    productTags = json[SerializationKeys.productTags].string
    subCatBriefIntro = json[SerializationKeys.subCatBriefIntro].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = productPrice { dictionary[SerializationKeys.productPrice] = value }
    if let value = subCatId { dictionary[SerializationKeys.subCatId] = value }
    if let value = productQty { dictionary[SerializationKeys.productQty] = value }
    if let value = subCatName { dictionary[SerializationKeys.subCatName] = value }
    if let value = catName { dictionary[SerializationKeys.catName] = value }
    if let value = productDescription { dictionary[SerializationKeys.productDescription] = value }
    if let value = isCart { dictionary[SerializationKeys.isCart] = value }
    if let value = productImage { dictionary[SerializationKeys.productImage] = value }
    if let value = catId { dictionary[SerializationKeys.catId] = value }
    if let value = productSku { dictionary[SerializationKeys.productSku] = value }
    if let value = isWish { dictionary[SerializationKeys.isWish] = value }
    if let value = catBriefIntro { dictionary[SerializationKeys.catBriefIntro] = value }
    if let value = productID { dictionary[SerializationKeys.productID] = value }
    if let value = priceDate { dictionary[SerializationKeys.priceDate] = value }
    if let value = productId { dictionary[SerializationKeys.productId] = value }
    if let value = productName { dictionary[SerializationKeys.productName] = value }
    if let value = productTags { dictionary[SerializationKeys.productTags] = value }
    if let value = subCatBriefIntro { dictionary[SerializationKeys.subCatBriefIntro] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.productPrice = aDecoder.decodeObject(forKey: SerializationKeys.productPrice) as? String
    self.subCatId = aDecoder.decodeObject(forKey: SerializationKeys.subCatId) as? String
    self.productQty = aDecoder.decodeObject(forKey: SerializationKeys.productQty) as? String
    self.subCatName = aDecoder.decodeObject(forKey: SerializationKeys.subCatName) as? String
    self.catName = aDecoder.decodeObject(forKey: SerializationKeys.catName) as? String
    self.productDescription = aDecoder.decodeObject(forKey: SerializationKeys.productDescription) as? String
    self.isCart = aDecoder.decodeObject(forKey: SerializationKeys.isCart) as? String
    self.productImage = aDecoder.decodeObject(forKey: SerializationKeys.productImage) as? String
    self.catId = aDecoder.decodeObject(forKey: SerializationKeys.catId) as? String
    self.productSku = aDecoder.decodeObject(forKey: SerializationKeys.productSku) as? String
    self.isWish = aDecoder.decodeObject(forKey: SerializationKeys.isWish) as? String
    self.catBriefIntro = aDecoder.decodeObject(forKey: SerializationKeys.catBriefIntro) as? String
    self.productID = aDecoder.decodeObject(forKey: SerializationKeys.productID) as? String
    self.priceDate = aDecoder.decodeObject(forKey: SerializationKeys.priceDate) as? String
    self.productId = aDecoder.decodeObject(forKey: SerializationKeys.productId) as? String
    self.productName = aDecoder.decodeObject(forKey: SerializationKeys.productName) as? String
    self.productTags = aDecoder.decodeObject(forKey: SerializationKeys.productTags) as? String
    self.subCatBriefIntro = aDecoder.decodeObject(forKey: SerializationKeys.subCatBriefIntro) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(productPrice, forKey: SerializationKeys.productPrice)
    aCoder.encode(subCatId, forKey: SerializationKeys.subCatId)
    aCoder.encode(productQty, forKey: SerializationKeys.productQty)
    aCoder.encode(subCatName, forKey: SerializationKeys.subCatName)
    aCoder.encode(catName, forKey: SerializationKeys.catName)
    aCoder.encode(productDescription, forKey: SerializationKeys.productDescription)
    aCoder.encode(isCart, forKey: SerializationKeys.isCart)
    aCoder.encode(productImage, forKey: SerializationKeys.productImage)
    aCoder.encode(catId, forKey: SerializationKeys.catId)
    aCoder.encode(productSku, forKey: SerializationKeys.productSku)
    aCoder.encode(isWish, forKey: SerializationKeys.isWish)
    aCoder.encode(catBriefIntro, forKey: SerializationKeys.catBriefIntro)
    aCoder.encode(productID, forKey: SerializationKeys.productID)
    aCoder.encode(priceDate, forKey: SerializationKeys.priceDate)
    aCoder.encode(productId, forKey: SerializationKeys.productId)
    aCoder.encode(productName, forKey: SerializationKeys.productName)
    aCoder.encode(productTags, forKey: SerializationKeys.productTags)
    aCoder.encode(subCatBriefIntro, forKey: SerializationKeys.subCatBriefIntro)
  }

}
