//
//  Products.swift
//
//  Created by Protege Global on 24/07/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Products: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let subCatBriefIntro = "SubCatBriefIntro"
    static let subCatId = "SubCatId"
    static let productQty = "ProductQty"
    static let subCatName = "SubCatName"
    static let catName = "CatName"
    static let productImage = "ProductImage"
    static let productOldPrice = "ProductOldPrice"
    static let catId = "CatId"
    static let catBriefIntro = "CatBriefIntro"
    static let productSku = "ProductSku"
    static let productDescription = "ProductDescription"
    static let productID = "ProductID"
    static let image = "image"
    static let productName = "ProductName"
    static let productTags = "ProductTags"
    static let productPrice = "ProductPrice"
    static let is_wish = "is_wish"
    static let is_cart = "is_cart"
    static let userQty = "UserQty"
  }

  // MARK: Properties
  public var subCatBriefIntro: String?
  public var subCatId: String?
  public var productQty: String?
  public var subCatName: String?
  public var catName: String?
  public var productImage: String?
  public var productOldPrice: String?
  public var catId: String?
  public var catBriefIntro: String?
  public var productSku: String?
  public var productDescription: String?
  public var productID: String?
  public var image: String?
  public var productName: String?
  public var productTags: String?
  public var productPrice: String?
  public var is_wish: String?
  public var is_cart: String?
    public var userQty: String?
    
  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    subCatBriefIntro = json[SerializationKeys.subCatBriefIntro].string
    subCatId = json[SerializationKeys.subCatId].string
    productQty = json[SerializationKeys.productQty].string
    subCatName = json[SerializationKeys.subCatName].string
    catName = json[SerializationKeys.catName].string
    productImage = json[SerializationKeys.productImage].string
    productOldPrice = json[SerializationKeys.productOldPrice].string
    catId = json[SerializationKeys.catId].string
    catBriefIntro = json[SerializationKeys.catBriefIntro].string
    productSku = json[SerializationKeys.productSku].string
    productDescription = json[SerializationKeys.productDescription].string
    productID = json[SerializationKeys.productID].string
    image = json[SerializationKeys.image].string
    productName = json[SerializationKeys.productName].string
    productTags = json[SerializationKeys.productTags].string
    productPrice = json[SerializationKeys.productPrice].string
    is_wish = json[SerializationKeys.is_wish].string
     is_cart = json[SerializationKeys.is_cart].string
    userQty = json[SerializationKeys.userQty].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = subCatBriefIntro { dictionary[SerializationKeys.subCatBriefIntro] = value }
    if let value = subCatId { dictionary[SerializationKeys.subCatId] = value }
    if let value = productQty { dictionary[SerializationKeys.productQty] = value }
    if let value = subCatName { dictionary[SerializationKeys.subCatName] = value }
    if let value = catName { dictionary[SerializationKeys.catName] = value }
    if let value = productImage { dictionary[SerializationKeys.productImage] = value }
    if let value = productOldPrice { dictionary[SerializationKeys.productOldPrice] = value }
    if let value = catId { dictionary[SerializationKeys.catId] = value }
    if let value = catBriefIntro { dictionary[SerializationKeys.catBriefIntro] = value }
    if let value = productSku { dictionary[SerializationKeys.productSku] = value }
    if let value = productDescription { dictionary[SerializationKeys.productDescription] = value }
    if let value = productID { dictionary[SerializationKeys.productID] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    if let value = productName { dictionary[SerializationKeys.productName] = value }
    if let value = productTags { dictionary[SerializationKeys.productTags] = value }
    if let value = productPrice { dictionary[SerializationKeys.productPrice] = value }
    if let value = is_wish{dictionary[SerializationKeys.is_wish] = value }
    if let value = is_cart{dictionary[SerializationKeys.is_cart] = value }
    if let value = userQty{dictionary[SerializationKeys.userQty] = value }
    
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.subCatBriefIntro = aDecoder.decodeObject(forKey: SerializationKeys.subCatBriefIntro) as? String
    self.subCatId = aDecoder.decodeObject(forKey: SerializationKeys.subCatId) as? String
    self.productQty = aDecoder.decodeObject(forKey: SerializationKeys.productQty) as? String
    self.subCatName = aDecoder.decodeObject(forKey: SerializationKeys.subCatName) as? String
    self.catName = aDecoder.decodeObject(forKey: SerializationKeys.catName) as? String
    self.productImage = aDecoder.decodeObject(forKey: SerializationKeys.productImage) as? String
    self.productOldPrice = aDecoder.decodeObject(forKey: SerializationKeys.productOldPrice) as? String
    self.catId = aDecoder.decodeObject(forKey: SerializationKeys.catId) as? String
    self.catBriefIntro = aDecoder.decodeObject(forKey: SerializationKeys.catBriefIntro) as? String
    self.productSku = aDecoder.decodeObject(forKey: SerializationKeys.productSku) as? String
    self.productDescription = aDecoder.decodeObject(forKey: SerializationKeys.productDescription) as? String
    self.productID = aDecoder.decodeObject(forKey: SerializationKeys.productID) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
    self.productName = aDecoder.decodeObject(forKey: SerializationKeys.productName) as? String
    self.productTags = aDecoder.decodeObject(forKey: SerializationKeys.productTags) as? String
    self.productPrice = aDecoder.decodeObject(forKey: SerializationKeys.productPrice) as? String
    self.is_wish = aDecoder.decodeObject(forKey: SerializationKeys.is_wish) as? String
    self.is_cart = aDecoder.decodeObject(forKey: SerializationKeys.is_cart) as? String
    self.userQty = aDecoder.decodeObject(forKey: SerializationKeys.userQty) as? String
    
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(subCatBriefIntro, forKey: SerializationKeys.subCatBriefIntro)
    aCoder.encode(subCatId, forKey: SerializationKeys.subCatId)
    aCoder.encode(productQty, forKey: SerializationKeys.productQty)
    aCoder.encode(subCatName, forKey: SerializationKeys.subCatName)
    aCoder.encode(catName, forKey: SerializationKeys.catName)
    aCoder.encode(productImage, forKey: SerializationKeys.productImage)
    aCoder.encode(productOldPrice, forKey: SerializationKeys.productOldPrice)
    aCoder.encode(catId, forKey: SerializationKeys.catId)
    aCoder.encode(catBriefIntro, forKey: SerializationKeys.catBriefIntro)
    aCoder.encode(productSku, forKey: SerializationKeys.productSku)
    aCoder.encode(productDescription, forKey: SerializationKeys.productDescription)
    aCoder.encode(productID, forKey: SerializationKeys.productID)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(productName, forKey: SerializationKeys.productName)
    aCoder.encode(productTags, forKey: SerializationKeys.productTags)
    aCoder.encode(productPrice, forKey: SerializationKeys.productPrice)
    aCoder.encode(is_wish, forKey: SerializationKeys.is_wish)
    aCoder.encode(is_cart, forKey: SerializationKeys.is_cart)
    aCoder.encode(userQty, forKey: SerializationKeys.userQty)
  }

}
