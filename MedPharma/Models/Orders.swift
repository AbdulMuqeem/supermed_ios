//
//  Orders.swift
//
//  Created by mac  on 9/11/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Orders: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let orderNo = "order_no"
    static let paymentId = "payment_id"
    static let userId = "user_id"
    static let orderNote = "order_note"
    static let orderStatus = "orderStatus"
    static let orderTotalPrice = "order_total_price"
  }

  // MARK: Properties
  public var orderNo: String?
  public var paymentId: String?
  public var userId: String?
  public var orderNote: String?
  public var orderStatus: String?
  public var orderTotalPrice: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    orderNo = json[SerializationKeys.orderNo].string
    paymentId = json[SerializationKeys.paymentId].string
    userId = json[SerializationKeys.userId].string
    orderNote = json[SerializationKeys.orderNote].string
    orderStatus = json[SerializationKeys.orderStatus].string
    orderTotalPrice = json[SerializationKeys.orderTotalPrice].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = orderNo { dictionary[SerializationKeys.orderNo] = value }
    if let value = paymentId { dictionary[SerializationKeys.paymentId] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = orderNote { dictionary[SerializationKeys.orderNote] = value }
    if let value = orderStatus { dictionary[SerializationKeys.orderStatus] = value }
    if let value = orderTotalPrice { dictionary[SerializationKeys.orderTotalPrice] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.orderNo = aDecoder.decodeObject(forKey: SerializationKeys.orderNo) as? String
    self.paymentId = aDecoder.decodeObject(forKey: SerializationKeys.paymentId) as? String
    self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? String
    self.orderNote = aDecoder.decodeObject(forKey: SerializationKeys.orderNote) as? String
    self.orderStatus = aDecoder.decodeObject(forKey: SerializationKeys.orderStatus) as? String
    self.orderTotalPrice = aDecoder.decodeObject(forKey: SerializationKeys.orderTotalPrice) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(orderNo, forKey: SerializationKeys.orderNo)
    aCoder.encode(paymentId, forKey: SerializationKeys.paymentId)
    aCoder.encode(userId, forKey: SerializationKeys.userId)
    aCoder.encode(orderNote, forKey: SerializationKeys.orderNote)
    aCoder.encode(orderStatus, forKey: SerializationKeys.orderStatus)
    aCoder.encode(orderTotalPrice, forKey: SerializationKeys.orderTotalPrice)
  }

}
