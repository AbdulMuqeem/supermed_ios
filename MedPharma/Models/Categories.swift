//
//  Categories.swift
//
//  Created by Protege Global on 20/07/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

import Foundation
import SwiftyJSON

public final class Categories: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let catId = "cat_id"
        static let name = "name"
        static let briefIntro = "brief_intro"
    }
    
    // MARK: Properties
    public var catId: String?
    public var name: String?
    public var briefIntro: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        catId = json[SerializationKeys.catId].string
        name = json[SerializationKeys.name].string
        briefIntro = json[SerializationKeys.briefIntro].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = catId { dictionary[SerializationKeys.catId] = value }
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = briefIntro { dictionary[SerializationKeys.briefIntro] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.catId = aDecoder.decodeObject(forKey: SerializationKeys.catId) as? String
        self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
        self.briefIntro = aDecoder.decodeObject(forKey: SerializationKeys.briefIntro) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(catId, forKey: SerializationKeys.catId)
        aCoder.encode(name, forKey: SerializationKeys.name)
        aCoder.encode(briefIntro, forKey: SerializationKeys.briefIntro)
    }
    
}
