//
//  User.swift
//
//  Created by Protege Global on 22/03/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

import Foundation
import SwiftyJSON

public final class Profile: NSObject, NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "id"
        static let patient_id = "patient_id"
        static let image = "image"
        static let first_name = "first_name"
        static let gender = "gender"
        static let last_name = "last_name"
        static let email = "email"
        static let contact = "contact"
        static let dob = "dob"
        static let address = "address"
        static let city = "city"
        static let country = "country"
        static let cartCounter = "cartCounter"
    }
    
    // MARK: Properties
    public var id: String?
    public var patient_id: String?
    public var image: String?
    public var first_name: String?
    public var gender: String?
    public var last_name: String?
    public var email: String?
    public var contact: String?
    public var dob: String?
    public var address: String?
    public var city: String?
    public var country: String?
    public var cartCounter: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        id = json[SerializationKeys.id].string
        patient_id = json[SerializationKeys.patient_id].string
        image = json[SerializationKeys.image].string
        first_name = json[SerializationKeys.first_name].string
        gender = json[SerializationKeys.gender].string
        last_name = json[SerializationKeys.last_name].string
        email = json[SerializationKeys.email].string
        contact = json[SerializationKeys.contact].string
        dob = json[SerializationKeys.dob].string
        address = json[SerializationKeys.address].string
        city = json[SerializationKeys.city].string
        country = json[SerializationKeys.country].string
        cartCounter = json[SerializationKeys.cartCounter].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = patient_id { dictionary[SerializationKeys.patient_id] = value }
        if let value = image { dictionary[SerializationKeys.image] = value }
        if let value = first_name { dictionary[SerializationKeys.first_name] = value }
        if let value = gender { dictionary[SerializationKeys.gender] = value }
        if let value = last_name { dictionary[SerializationKeys.last_name] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = contact { dictionary[SerializationKeys.contact] = value }
        if let value = dob { dictionary[SerializationKeys.dob] = value }
        if let value = address { dictionary[SerializationKeys.address] = value }
        if let value = city { dictionary[SerializationKeys.city] = value }
        if let value = country { dictionary[SerializationKeys.country] = value }
        if let value = cartCounter { dictionary[SerializationKeys.cartCounter] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
        self.patient_id = aDecoder.decodeObject(forKey: SerializationKeys.patient_id) as? String
        self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
        self.first_name = aDecoder.decodeObject(forKey: SerializationKeys.first_name) as? String
        self.gender = aDecoder.decodeObject(forKey: SerializationKeys.gender) as? String
        self.last_name = aDecoder.decodeObject(forKey: SerializationKeys.last_name) as? String
        self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
        self.contact = aDecoder.decodeObject(forKey: SerializationKeys.contact) as? String
        self.dob = aDecoder.decodeObject(forKey: SerializationKeys.dob) as? String
        self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
        self.city = aDecoder.decodeObject(forKey: SerializationKeys.city) as? String
        self.country = aDecoder.decodeObject(forKey: SerializationKeys.country) as? String
        self.cartCounter = aDecoder.decodeObject(forKey: SerializationKeys.cartCounter) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(patient_id, forKey: SerializationKeys.patient_id)
        aCoder.encode(image, forKey: SerializationKeys.image)
        aCoder.encode(first_name, forKey: SerializationKeys.first_name)
        aCoder.encode(gender, forKey: SerializationKeys.gender)
        aCoder.encode(last_name, forKey: SerializationKeys.last_name)
        aCoder.encode(email, forKey: SerializationKeys.email)
        aCoder.encode(contact, forKey: SerializationKeys.contact)
        aCoder.encode(dob, forKey: SerializationKeys.dob)
        aCoder.encode(address, forKey: SerializationKeys.address)
        aCoder.encode(city, forKey: SerializationKeys.city)
        aCoder.encode(country, forKey: SerializationKeys.country)
        aCoder.encode(cartCounter, forKey: SerializationKeys.cartCounter)
    }
}


