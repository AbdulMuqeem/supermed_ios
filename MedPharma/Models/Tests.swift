//
//  Tests.swift
//
//  Created by Protege Global on 06/08/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Tests: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let labPrimaryContact = "labPrimaryContact"
    static let testName = "testName"
    static let labAddress = "labAddress"
    static let testPrice = "testPrice"
    static let testId = "testId"
    static let labSecContact = "labSecContact"
    static let labId = "labId"
    static let labName = "labName"
  }

  // MARK: Properties
  public var labPrimaryContact: String?
  public var testName: String?
  public var labAddress: String?
  public var testPrice: String?
  public var testId: String?
  public var labSecContact: String?
  public var labId: String?
  public var labName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    labPrimaryContact = json[SerializationKeys.labPrimaryContact].string
    testName = json[SerializationKeys.testName].string
    labAddress = json[SerializationKeys.labAddress].string
    testPrice = json[SerializationKeys.testPrice].string
    testId = json[SerializationKeys.testId].string
    labSecContact = json[SerializationKeys.labSecContact].string
    labId = json[SerializationKeys.labId].string
    labName = json[SerializationKeys.labName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = labPrimaryContact { dictionary[SerializationKeys.labPrimaryContact] = value }
    if let value = testName { dictionary[SerializationKeys.testName] = value }
    if let value = labAddress { dictionary[SerializationKeys.labAddress] = value }
    if let value = testPrice { dictionary[SerializationKeys.testPrice] = value }
    if let value = testId { dictionary[SerializationKeys.testId] = value }
    if let value = labSecContact { dictionary[SerializationKeys.labSecContact] = value }
    if let value = labId { dictionary[SerializationKeys.labId] = value }
    if let value = labName { dictionary[SerializationKeys.labName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.labPrimaryContact = aDecoder.decodeObject(forKey: SerializationKeys.labPrimaryContact) as? String
    self.testName = aDecoder.decodeObject(forKey: SerializationKeys.testName) as? String
    self.labAddress = aDecoder.decodeObject(forKey: SerializationKeys.labAddress) as? String
    self.testPrice = aDecoder.decodeObject(forKey: SerializationKeys.testPrice) as? String
    self.testId = aDecoder.decodeObject(forKey: SerializationKeys.testId) as? String
    self.labSecContact = aDecoder.decodeObject(forKey: SerializationKeys.labSecContact) as? String
    self.labId = aDecoder.decodeObject(forKey: SerializationKeys.labId) as? String
    self.labName = aDecoder.decodeObject(forKey: SerializationKeys.labName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(labPrimaryContact, forKey: SerializationKeys.labPrimaryContact)
    aCoder.encode(testName, forKey: SerializationKeys.testName)
    aCoder.encode(labAddress, forKey: SerializationKeys.labAddress)
    aCoder.encode(testPrice, forKey: SerializationKeys.testPrice)
    aCoder.encode(testId, forKey: SerializationKeys.testId)
    aCoder.encode(labSecContact, forKey: SerializationKeys.labSecContact)
    aCoder.encode(labId, forKey: SerializationKeys.labId)
    aCoder.encode(labName, forKey: SerializationKeys.labName)
  }

}
