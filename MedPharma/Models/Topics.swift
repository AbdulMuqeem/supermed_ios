//
//  Topics.swift
//
//  Created by Protege Global on 21/08/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Topics: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "id"
    static let topic = "topic"
    static let question = "question"
    static let answer = "answer"
  }

  // MARK: Properties
  public var id: String?
  public var topic: String?
  public var question: String?
  public var answer: String?
  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    id = json[SerializationKeys.id].string
    topic = json[SerializationKeys.topic].string
    question = json[SerializationKeys.question].string
    answer = json[SerializationKeys.answer].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = topic { dictionary[SerializationKeys.topic] = value }
    if let value = question { dictionary[SerializationKeys.question] = value }
    if let value = answer { dictionary[SerializationKeys.answer] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.topic = aDecoder.decodeObject(forKey: SerializationKeys.topic) as? String
    self.question = aDecoder.decodeObject(forKey: SerializationKeys.question) as? String
    self.answer = aDecoder.decodeObject(forKey: SerializationKeys.answer) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(topic, forKey: SerializationKeys.topic)
    aCoder.encode(question, forKey: SerializationKeys.question)
    aCoder.encode(answer, forKey: SerializationKeys.answer)
  }

}
/*
 {
 "answer" : "Our user-friendly website lists hundreds of products in many different categories. Just browse, choose and add your selected product to the cart. Put your details and check out by selecting a payment method. You will receive a confirmation email and call from our team shortly.",
 "topic" : "Orders",
 "id" : "9",
 "question" : "How to Order?"
 }
 */
