//
//  HomeModel.swift
//  MedPharma
//
//  Created by Protege Global on 27/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import Foundation
struct Home {
    var title : String
    var image : String
    var isSelected : Bool
    
    init(title: String, image: String, isSelected: Bool) {
    self.title = title
    self.image = image
    self.isSelected = isSelected
    }
}


