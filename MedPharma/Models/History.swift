//
//  History.swift
//
//  Created by Protege Global on 08/08/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class History: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let appoinmentStatus = "appoinmentStatus"
    static let patientName = "patient_name"
    static let patientId = "patient_id"
    static let date = "date"
    static let testname = "testname"
    static let labname = "labname"
    static let appointmentId = "appointment_id"
  }

  // MARK: Properties
  public var appoinmentStatus: String?
  public var patientName: String?
  public var patientId: String?
  public var date: String?
  public var testname: String?
  public var labname: String?
  public var appointmentId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    appoinmentStatus = json[SerializationKeys.appoinmentStatus].string
    patientName = json[SerializationKeys.patientName].string
    patientId = json[SerializationKeys.patientId].string
    date = json[SerializationKeys.date].string
    testname = json[SerializationKeys.testname].string
    labname = json[SerializationKeys.labname].string
    appointmentId = json[SerializationKeys.appointmentId].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = appoinmentStatus { dictionary[SerializationKeys.appoinmentStatus] = value }
    if let value = patientName { dictionary[SerializationKeys.patientName] = value }
    if let value = patientId { dictionary[SerializationKeys.patientId] = value }
    if let value = date { dictionary[SerializationKeys.date] = value }
    if let value = testname { dictionary[SerializationKeys.testname] = value }
    if let value = labname { dictionary[SerializationKeys.labname] = value }
    if let value = appointmentId { dictionary[SerializationKeys.appointmentId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.appoinmentStatus = aDecoder.decodeObject(forKey: SerializationKeys.appoinmentStatus) as? String
    self.patientName = aDecoder.decodeObject(forKey: SerializationKeys.patientName) as? String
    self.patientId = aDecoder.decodeObject(forKey: SerializationKeys.patientId) as? String
    self.date = aDecoder.decodeObject(forKey: SerializationKeys.date) as? String
    self.testname = aDecoder.decodeObject(forKey: SerializationKeys.testname) as? String
    self.labname = aDecoder.decodeObject(forKey: SerializationKeys.labname) as? String
    self.appointmentId = aDecoder.decodeObject(forKey: SerializationKeys.appointmentId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(appoinmentStatus, forKey: SerializationKeys.appoinmentStatus)
    aCoder.encode(patientName, forKey: SerializationKeys.patientName)
    aCoder.encode(patientId, forKey: SerializationKeys.patientId)
    aCoder.encode(date, forKey: SerializationKeys.date)
    aCoder.encode(testname, forKey: SerializationKeys.testname)
    aCoder.encode(labname, forKey: SerializationKeys.labname)
    aCoder.encode(appointmentId, forKey: SerializationKeys.appointmentId)
  }

}
