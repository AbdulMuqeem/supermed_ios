//
//  Data.swift
//
//  Created by Abdul Muqeem on 26/06/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ContactUsData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let email = "email"
    static let whatsapp = "whatsapp"
    static let mobile = "mobile"
    static let addressOne = "addressOne"
    static let addressTwo = "addressTwo"
  }

  // MARK: Properties
  public var email: String?
  public var whatsapp: String?
  public var mobile: String?
  public var addressOne: String?
  public var addressTwo: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    email = json[SerializationKeys.email].string
    whatsapp = json[SerializationKeys.whatsapp].string
    mobile = json[SerializationKeys.mobile].string
    addressOne = json[SerializationKeys.addressOne].string
    addressTwo = json[SerializationKeys.addressTwo].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = whatsapp { dictionary[SerializationKeys.whatsapp] = value }
    if let value = mobile { dictionary[SerializationKeys.mobile] = value }
    if let value = addressOne { dictionary[SerializationKeys.addressOne] = value }
    if let value = addressTwo { dictionary[SerializationKeys.addressTwo] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.whatsapp = aDecoder.decodeObject(forKey: SerializationKeys.whatsapp) as? String
    self.mobile = aDecoder.decodeObject(forKey: SerializationKeys.mobile) as? String
    self.addressOne = aDecoder.decodeObject(forKey: SerializationKeys.addressOne) as? String
    self.addressTwo = aDecoder.decodeObject(forKey: SerializationKeys.addressTwo) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(whatsapp, forKey: SerializationKeys.whatsapp)
    aCoder.encode(mobile, forKey: SerializationKeys.mobile)
    aCoder.encode(addressOne, forKey: SerializationKeys.addressOne)
    aCoder.encode(addressTwo, forKey: SerializationKeys.addressTwo)
  }

}
