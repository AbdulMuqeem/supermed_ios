//
//  DoctorsInfo.swift
//
//  Created by Protege Global on 28/08/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class DoctorsInfo: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let daysANDTime = "DaysANDTime"
    static let note = "note"
    static let id = "id"
    static let clinicName = "clinic_name"
    static let visitFee = "visit_fee"
    static let clinicFee = "clinic_fee"
    static let qualification = "qualification"
  }

  // MARK: Properties
  public var daysANDTime: String?
  public var note: String?
  public var id: String?
  public var clinicName: String?
  public var visitFee: String?
  public var clinicFee: String?
  public var qualification: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    daysANDTime = json[SerializationKeys.daysANDTime].string
    note = json[SerializationKeys.note].string
    id = json[SerializationKeys.id].string
    clinicName = json[SerializationKeys.clinicName].string
    visitFee = json[SerializationKeys.visitFee].string
    clinicFee = json[SerializationKeys.clinicFee].string
    qualification = json[SerializationKeys.qualification].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = daysANDTime { dictionary[SerializationKeys.daysANDTime] = value }
    if let value = note { dictionary[SerializationKeys.note] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = clinicName { dictionary[SerializationKeys.clinicName] = value }
    if let value = visitFee { dictionary[SerializationKeys.visitFee] = value }
    if let value = clinicFee { dictionary[SerializationKeys.clinicFee] = value }
    if let value = qualification { dictionary[SerializationKeys.qualification] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.daysANDTime = aDecoder.decodeObject(forKey: SerializationKeys.daysANDTime) as? String
    self.note = aDecoder.decodeObject(forKey: SerializationKeys.note) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.clinicName = aDecoder.decodeObject(forKey: SerializationKeys.clinicName) as? String
    self.visitFee = aDecoder.decodeObject(forKey: SerializationKeys.visitFee) as? String
    self.clinicFee = aDecoder.decodeObject(forKey: SerializationKeys.clinicFee) as? String
    self.qualification = aDecoder.decodeObject(forKey: SerializationKeys.qualification) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(daysANDTime, forKey: SerializationKeys.daysANDTime)
    aCoder.encode(note, forKey: SerializationKeys.note)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(clinicName, forKey: SerializationKeys.clinicName)
    aCoder.encode(visitFee, forKey: SerializationKeys.visitFee)
    aCoder.encode(clinicFee, forKey: SerializationKeys.clinicFee)
    aCoder.encode(qualification, forKey: SerializationKeys.qualification)
  }

}
