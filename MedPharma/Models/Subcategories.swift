//
//  Subcategories.swift
//
//  Created by Protege Global on 24/07/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Subcategories: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let subCatName = "SubCatName"
    static let subCatId = "SubCatId"
    static let parentId = "ParentId"
    static let parentBriefIntro = "ParentBriefIntro"
    static let briefIntro = "BriefIntro"
    static let parentName = "ParentName"
  }

  // MARK: Properties
  public var subCatName: String?
  public var subCatId: String?
  public var parentId: String?
  public var parentBriefIntro: String?
  public var briefIntro: String?
  public var parentName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    subCatName = json[SerializationKeys.subCatName].string
    subCatId = json[SerializationKeys.subCatId].string
    parentId = json[SerializationKeys.parentId].string
    parentBriefIntro = json[SerializationKeys.parentBriefIntro].string
    briefIntro = json[SerializationKeys.briefIntro].string
    parentName = json[SerializationKeys.parentName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = subCatName { dictionary[SerializationKeys.subCatName] = value }
    if let value = subCatId { dictionary[SerializationKeys.subCatId] = value }
    if let value = parentId { dictionary[SerializationKeys.parentId] = value }
    if let value = parentBriefIntro { dictionary[SerializationKeys.parentBriefIntro] = value }
    if let value = briefIntro { dictionary[SerializationKeys.briefIntro] = value }
    if let value = parentName { dictionary[SerializationKeys.parentName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.subCatName = aDecoder.decodeObject(forKey: SerializationKeys.subCatName) as? String
    self.subCatId = aDecoder.decodeObject(forKey: SerializationKeys.subCatId) as? String
    self.parentId = aDecoder.decodeObject(forKey: SerializationKeys.parentId) as? String
    self.parentBriefIntro = aDecoder.decodeObject(forKey: SerializationKeys.parentBriefIntro) as? String
    self.briefIntro = aDecoder.decodeObject(forKey: SerializationKeys.briefIntro) as? String
    self.parentName = aDecoder.decodeObject(forKey: SerializationKeys.parentName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(subCatName, forKey: SerializationKeys.subCatName)
    aCoder.encode(subCatId, forKey: SerializationKeys.subCatId)
    aCoder.encode(parentId, forKey: SerializationKeys.parentId)
    aCoder.encode(parentBriefIntro, forKey: SerializationKeys.parentBriefIntro)
    aCoder.encode(briefIntro, forKey: SerializationKeys.briefIntro)
    aCoder.encode(parentName, forKey: SerializationKeys.parentName)
  }

}
