//
//  LabOrderViewController.swift
//  MedPharma
//
//  Created by Protege Global on 02/07/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class LabOrderViewController: UIViewController {

    @IBOutlet weak var heightTable: NSLayoutConstraint!
    @IBOutlet weak var tableLabAppointment: UITableView!
    
    class func instantiateFromStoryboard() -> LabOrderViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LabOrderViewController
    }
    
    
    @IBOutlet weak var viewRight: UIView!
    @IBOutlet weak var viewLeft: UIView!
    @IBOutlet weak var scrollHeight: NSLayoutConstraint!
    
    let array = ["Test1", "Test2"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Lab Test Appointment"
        self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(didTapOnBack))
        
        let nib = UINib(nibName: "AppointmentSectionTableCell", bundle: nil)
        tableLabAppointment.register(nib, forCellReuseIdentifier: "AppointmentSectionTableCell")
        
        let nib2 = UINib(nibName: "AppointmentDetailTableViewCell", bundle: nil)
        tableLabAppointment.register(nib2, forCellReuseIdentifier: "AppointmentDetailTableViewCell")
        tableLabAppointment.delegate = self
        tableLabAppointment.dataSource = self
        reloadData()
        customizeView()
    }
    
    func reloadData(){
        
        

       heightTable.constant = 300 + (100 * 20)
        
        
        
        
    }
    
    func customizeView(){
         viewLeft.addDashedBorder(strokeColor: UIColor.navColor, lineWidth: 1.0)
         viewRight.addDashedBorder(strokeColor: UIColor.navColor, lineWidth: 1.0)
        
    }
    
    @IBAction func didTappedCoupon(_ sender: Any) {
        
    }
    @IBAction func didTappedPromoCode(_ sender: Any) {
    }
    
    @IBAction func didTappedOnConfirmAppointment(_ sender: Any) {
        let card = CardAlertView(frame: (AppDelegate.getInstatnce().window?.bounds)!)
            AppDelegate.getInstatnce().window?.addSubview(card)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func didTapOnBack() {
        self.navigationController?.popViewController(animated: true)
    }
   
}

extension LabOrderViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 20 //array.count
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let view:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.size.width, height: 10))
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableLabAppointment.dequeueReusableCell(withIdentifier: "AppointmentSectionTableCell") as? AppointmentSectionTableCell
        
        return cell?.contentView
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellDetail = tableLabAppointment.dequeueReusableCell(withIdentifier: "AppointmentDetailTableViewCell") as! AppointmentDetailTableViewCell
        
        return cellDetail
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 100)
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    
}
