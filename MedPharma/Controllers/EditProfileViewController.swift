//
//  EditProfileViewController.swift
//  MedPharma
//
//  Created by Protege Global on 29/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import Photos
import NotificationBannerSwift
import ActionSheetPicker_3_0
import DropDown
import SwiftyJSON
import AlamofireImage


class EditProfileViewController: UIViewController {
    class func instantiateFromStoryboard() -> EditProfileViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! EditProfileViewController
    }
    @IBOutlet weak var btnCity: UIButton!
    
    @IBOutlet weak var btnCountry: UIButton!
    
    var countries = [Countries]()
    var cities = [Cities]()
    var countryArray = [String]()
    var cityArray = [String]()
    
    @IBAction func didTappedOnCountry(_ sender: UIButton) {
        
        let drop = DropDown()
        drop.anchorView = btnCountry
        drop.dataSource = countryArray
        drop.width = btnCountry.frame.size.width
        drop.bottomOffset = CGPoint(x: 0, y: 40)
        drop.direction = .bottom
        drop.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblCountry.text = "\(item)"
            
            let names = self.countries.map({
                if $0.name == item{
                    self.getCitiesFromCountries(id: $0.id!)
                    print($0.id)
                }
            })
        }
        drop.show()
    }
    
    
    @IBAction func didTappedOnCity(_ sender: UIButton) {
        let drop = DropDown()
        drop.anchorView = btnCity
//        let city = ["Karachi","Hyderabad","Lahore","Islamabad"]
        
        drop.dataSource = cityArray
        drop.width = btnCountry.frame.size.width
        drop.bottomOffset = CGPoint(x: 0, y: 40)
        drop.direction = .bottom
        drop.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblCity.text = "\(item)"
            let names = self.countries.map({
                if $0.id == item{
                    print($0.name)
                }
            })
        }
        drop.show()
    }
    
    var selectedImage: UIImage?
    var imagePicker : UIImagePickerController!
    var imageName: String = "no"
    var imageData = Data()
    var imagePath: URL?
    
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    
    @IBAction func didTapOnDOB(_ sender: UIButton) {
        
        ActionSheetDatePicker.show(withTitle: "Select DOB", datePickerMode: .date, selectedDate: Date(), minimumDate: nil, maximumDate: Date(), doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.lblDOB.text = newDate
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.txtEmail.isEnabled = false
        self.title = "Edit Profile"
        self.setPreferenceData()
        self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(didTapOnBack))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.getCountriesFromServer()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
    }
    
    @IBAction func didTapOnSaveChanges(_ sender: Any) {
        self.selectedImage = self.imageProfile.image
        if let data = UIImageJPEGRepresentation(self.selectedImage ?? UIImage(), 0.1) {
            self.imageData = data
        }
        guard self.selectedImage != nil else {
                let banner = NotificationBanner(title: "Profile Image", subtitle: "Please select profile picture", style: .danger)
                banner.show()
                return
            }
        self.updateProfile(data: self.imageData)
    }
    
    @objc func didTapOnBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapOnChangeProfile(_ sender: Any) {
        imagePicker =  UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        let alert = UIAlertController(title: "Choose one of the following:", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        let gallery = UIAlertAction(title: "Camera Roll", style: UIAlertActionStyle.default) { (alert) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let camera = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) { (alert) in
//            imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (alert) in
        }
        cancel.setValue(UIColor.red, forKey: "titleTextColor")
        
        alert.addAction(gallery)
        alert.addAction(camera)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    func updateProfile(data: Data){
        guard let first_name = self.txtFirstName.text,  first_name != "" else {
            let banner = NotificationBanner(title: "First Name", subtitle: "Please enter First Name", style: .danger)
            banner.show()
            return
        }
        guard let last_name = self.txtLastName.text,  last_name != "" else {
            let banner = NotificationBanner(title: "Last Name", subtitle: "Please enter Last Name", style: .danger)
            banner.show()
            return
        }
        
        guard let email = self.txtEmail.text,  email != "" ,AppHelper.isValidEmail(testStr: email) else {
            let banner = NotificationBanner(title: "Email", subtitle: "Please enter valid email", style: .danger)
            banner.show()
            return
        }
        
        guard let mobileNo = self.txtMobileNo.text,  mobileNo != "", AppHelper.isValidPhone(testStr: mobileNo)  else {
            let banner = NotificationBanner(title: "Mobile No", subtitle: "Please enter valid mobile number", style: .danger)
            banner.show()
            return
        }
        
        guard let address = self.txtAddress.text,  address != ""  else {
            let banner = NotificationBanner(title: "Address", subtitle: "Please enter address", style: .danger)
            banner.show()
            return
        }
        
        guard let country = self.lblCountry.text,  country != ""  else {
            let banner = NotificationBanner(title: "Country", subtitle: "Please Select Country", style: .danger)
            banner.show()
            return
        }
        
        guard let city = self.lblCity.text,  city != ""  else {
            let banner = NotificationBanner(title: "City", subtitle: "Please select Country", style: .danger)
            banner.show()
            return
        }
        
        guard let dob = self.lblDOB.text,  dob != ""  else {
            let banner = NotificationBanner(title: "D-O-B", subtitle: "Please Enter Date of birth", style: .danger)
            banner.show()
            return
        }
        
        let param = ["userid": UserManager.getUserObjectFromUserDefaults()!.id!, "first_name": first_name, "last_name": last_name, "contact": mobileNo, "address" : address, "country": country, "city": city, "image": imageData] as [String : Any]
        Alert.showLoader(message: "Loading")
        Request.postProfileImage(url: ServiceApiEndPoints.updateProfile,imageData: imageData,fileName: "file", parameters: param , callback:{(response, error) in
            Alert.hideLoader()
            Alert.showToast(message: "Profile Updated Successfully")
            
            let userResultObj = Profile(object:(response?["profile"])!)
            UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
            
            NotificationCenter.default.post(name: .NOTIFICATION_USERUPDATE, object: nil)
            
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    func getCountriesFromServer() {
        SuperMedServices.GetCountries(param: [:]) { (success, response, error) in
            if !success{
                return
            }else{
                print(response)
                self.countries.removeAll()
                self.countryArray.removeAll()
                let array = response!["countries"].arrayValue
                for country in array {
                    let count = Countries(json: country)
                    self.countries.append(count)
                    self.countryArray.append(count.name!)
                }
            }
        }
    }
    
    func getCitiesFromCountries(id: String){
        let params = ["country_id": id]
        
        SuperMedServices.GetCities(param: params) { (success, response, error) in
            if !success{
                return
            }else{
                print(response)
                self.cities.removeAll()
                self.cityArray.removeAll()
                let array = response!["cities"].arrayValue
                for city in array{
                    let ct = Cities(json: city)
                    self.cities.append(ct)
                    self.cityArray.append(ct.name!)
                }
            }
        }
    }
    
    func setPreferenceData(){
        if Singleton.sharedInstance.CurrentUser != nil {
            self.txtFirstName.text = Singleton.sharedInstance.CurrentUser!.first_name
            self.txtLastName.text = Singleton.sharedInstance.CurrentUser!.last_name
            self.txtEmail.text = Singleton.sharedInstance.CurrentUser!.email
            self.txtMobileNo.text = Singleton.sharedInstance.CurrentUser!.contact
            self.txtAddress.text = Singleton.sharedInstance.CurrentUser!.address
            self.lblCity.text = Singleton.sharedInstance.CurrentUser!.city
            self.lblCountry.text = Singleton.sharedInstance.CurrentUser!.country
            self.lblDOB.text = Singleton.sharedInstance.CurrentUser!.dob
            guard let imageURL = URL(string:(Singleton.sharedInstance.CurrentUser?.image) ?? "www.google.com") else {return}
            self.imageProfile.af_setImage(withURL: imageURL)
            self.selectedImage = self.imageProfile.image
            if let data = UIImageJPEGRepresentation(self.selectedImage ?? UIImage(), 0.1) {
                self.imageData = data
            }
        }
    }
}
//MARK: - Image Picker
extension EditProfileViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let pickedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.imageProfile.image = pickedImage
        self.selectedImage = pickedImage
        if let data = UIImageJPEGRepresentation(pickedImage, 0.1) {
            self.imageData = data
        }
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}


extension Notification.Name {
    static let NOTIFICATION_USERUPDATE = Notification.Name("user_update")
}



