//
//  PaymentWebViewController.swift
//  TAAJ
//
//  Created by Abdul Muqeem on 20/02/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import WebKit

class PaymentWebViewController: UIViewController , WKUIDelegate , WKNavigationDelegate {
    
    class func instantiateFromStoryboard() -> PaymentWebViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! PaymentWebViewController
    }
    
    var webView:WKWebView!
    var pathToFile:String! = ""
    
    var isPaymentResult:Bool! = false
    
    override func loadView() {
        
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "SuperMed Payment"
        self.PlaceLeftButton(image: #imageLiteral(resourceName: "icon_back"), selector: #selector(didTapOnBack))
        
        //For Open link in webView
        let url = URL(string: pathToFile)!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        
    }
    
    @objc func didTapOnBack() {
         self.navigationController?.popToRootViewController(animated: true)
    }
    
//    func successCall() {
//
//        self.view.endEditing(true)
//        self.startLoading(message: "")
//
//        if !AppHelper.isConnectedToInternet() {
//            self.Alert(title: "Error", message: "No Network Connection")
//            self.stopLoading()
//            return
//        }
//
//        let params:[String:Any] = ["":""]
//        print(params)
//
//        SendMoneyServices.PaymentSuccess(param:params , completionHandler: {(status, response, error) in
//
//            if !status {
//                if error != nil {
//                    let error = String(describing: (error as AnyObject).localizedDescription)
//                    print("Error: \(error)")
//                    self.Alert(title: "Error", message: error)
//                    self.stopLoading()
//                    return
//                }
//                let msg = response?["message"].stringValue
//                print("Message: \(String(describing: msg))")
//                self.Alert(title: "Alert", message: msg!)
//                self.stopLoading()
//                return
//            }
//
//            self.stopLoading()
//            print(response!)
//
//            if let status = response!["body"]["status"].string {
//
//                if status == "document_required" {
//
//                    DispatchQueue.main.asyncAfter(deadline: .now()+0.2 ) {
//
//                        let vc = VerifyViewController.instantiateFromStoryboard()
//                        vc.isWebPayment = true
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//
//                }
//
//                else {
//
//                    DispatchQueue.main.asyncAfter(deadline: .now()+0.2 ) {
//
//                        let vc = MyTransfersDetailViewController.instantiateFromStoryboard()
//                        vc.isWebPayment = true
//                        self.navigationController?.pushViewController(vc, animated: true)
//
//                    }
//                }
//
//            }
//
//        })
//
//
//    }
    
    
    //MARK:- WebView Delegate
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        Alert.showLoader(message: "Processing")
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        Alert.hideLoader()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        webView.evaluateJavaScript("document.documentElement.outerHTML") { (result, error) in
            if error == nil {
                
                let htmlString:String = result as! String
                let text = "\(htmlString.htmlToString)"
                
        //        if self.isPaymentResult == true {
                    
                    if text.contains("Success") {
                       // self.successCall()
                    }
                    else {
                        
                        print("Not Exists")
                        
                        if text.contains("Failed") {
                            
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.2 ) {
                                
//                                let vc = MyTransfersDetailViewController.instantiateFromStoryboard()
//                                vc.isPaymentFail = true
//                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            }
                            
                        }
                        
                    }
                    
    //            }
                
            }
            
           // self.isPaymentResult = true
            
        }
        
        
        
    }
    
}

