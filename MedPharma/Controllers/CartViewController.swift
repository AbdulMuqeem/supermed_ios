//
//  CartViewController.swift
//  MedPharma
//
//  Created by Protege Global on 22/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import SwiftyJSON
import NotificationBannerSwift

class CartViewController: UIViewController, CartViewDelegate {
    
    var dict: Dictionary<String, Any> = [String: Any]()
//    var products = [Products]()
    var product = [Products]()
    var from = ""
    var price : Int?
    var discountedPercentage = "0"
    
    class func instantiateFromStoryboard() -> CartViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
    }
    
    @IBOutlet weak var viewPlaceOrder: UIView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var lblDiscountNo: UILabel!
    @IBOutlet weak var lblPriceB: UILabel!
    @IBOutlet weak var lblPriceNo: UILabel!
    @IBOutlet weak var lblDevliveryNo: UILabel!
    @IBOutlet weak var lblCouponDisc: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblTotalPriceNo: UILabel!
    @IBOutlet weak var txtFieldPromo: UITextField!
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var btnApplyCoupon: UIButton!
    
    let base_image = "https://www.supermed.pk/pos/public/uploads/products/"

    @IBOutlet weak var lblWarning: UILabel!{
        
        didSet{
           lblWarning.isHidden = true
        }
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Cart"
        
        customization()
        setController(from: from)
        
//        setProductDataOnScreen()
        
    }
    
    
//    func setProductDataOnScreen(){
////        lblProductTitle.text = products[0].productName
////        lblPriceDiscount.text = products[0].productPrice
////        lblPrice.text = products[0].productOldPrice
////
////        if products[0].is_wish! == "1"{
////            imageWishlist.image = UIImage(named: "icon_wishlistGreen")
////        }else{
////            imageWishlist.image = UIImage(named: "icon_wishlist")
////        }
//    }
    
    func setController(from: String?){
        switch from {
        case "Home":
            getCartProducts()
            self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(didTapOnBack))
        case "SideMenu":
            getCartProducts()
             self.PlaceLeftButton(image: MENU_IMAGE, selector: #selector(MenuAction))
        default:
            self.addviewsInStack(products: self.product)
            self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(didTapOnBack))
        }
        
    }
    
    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    
    
    func customization(){
        btnApplyCoupon.addDashedBorder(strokeColor: UIColor.navColor, lineWidth: 1.0)
        txtFieldPromo.addDashedBorder(strokeColor: UIColor.navColor, lineWidth: 1.0)
        txtFieldPromo.addPadding( .left(16))
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didTappedOnPromoCode(_ sender: Any) {
             print("weldone")
    }
    
    @IBAction func didTappedOnCoupon(_ sender: Any) {
        validateCoupon()
    }
    
    @objc func didTapOnBack() {
        self.navigationController?.popViewController(animated: true)
    }
  
    @IBAction func didTappedOnSubmit(_ sender: UIButton) {
        let controller = PlaceOrderViewController.instantiateFromStoryboard()
        if dict != nil {
             controller.dict = dict
             print(dict)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        return
    }
    
    func getCartProducts(){
        let param = ["userid": UserManager.getUserObjectFromUserDefaults()!.id!]
        SuperMedServices.getCartProducts(param: param) { (success, response, error) in
            if !success{
                print("all Products:\(String(describing: response))")
                if response == nil{
                    return
                }
                
            let counter = response!["cartCounter"].stringValue
                if counter == "0" {
                    
                   
                
                self.lblWarning.isHidden = false
                self.viewPlaceOrder.isHidden = true
                self.scroll.isHidden = true
                    
                return
                }
            }
            
            self.product.removeAll()
            let productsArray = response!["cart"].arrayValue
            if productsArray.count > 0{
                for prod  in  productsArray{
                    let pro = Products(json: prod)
                    self.product.append(pro)
                }
                self.lblWarning.isHidden = true
                self.viewPlaceOrder.isHidden = false
                self.scroll.isHidden = false
                 self.addviewsInStack(products: self.product)
            }
            print(response!)
        }
    }
    
    
    func reloadServerData() {
        getCartProducts()
    }
    
    func addviewsInStack(products : [Products]){
        var total_price = 0
        
        self.lblPriceNo.text = ""
        
        for i in self.stack.arrangedSubviews{
            i.removeFromSuperview()
        }
        
        
        for i in products{
            
            let cart = CartView.instanceFromNib()
                cart.delegate = self
            
            if base_image != i.productImage! {
                cart.imageProduct.setImageFromUrl(urlStr: i.productImage!)
            }
            else {
                cart.imageProduct.setImageFromUrl(urlStr: "https://www.supermed.pk/pos/public/images/tab-missing.png")
            }
            
            cart.lblTitle.text = products[0].productName
            cart.lblPrice.text = products[0].productOldPrice
            cart.lblDiscount.text = products[0].productPrice
            cart.lblTitle.text = i.productName!
            cart.lblPrice.text = "Rs \(i.productPrice!)"
            let discountValue = (i.productPrice! as NSString).floatValue
            let originalPrice = ((i.productOldPrice ?? "0") as NSString).floatValue
            let percentage = findPercentage(discountValue: discountValue, originalValue: originalPrice)

                cart.lblDiscount.text = "Rs \(i.productOldPrice ?? "0")" + " (\(percentage) % off)"
                cart.lblCounter.text = i.userQty

                print(i.productPrice!)
                print(i.userQty!)
                let myInt2 = (i.productPrice! as NSString).integerValue
                let qty = (i.userQty! as NSString).integerValue
                print("product Price:\(myInt2)")
                print("qauntity:", qty)
            
            dict["quantity"] = "\(qty)"
            
            cart.product_id = i.productID!
            self.price = Int(myInt2 * qty)
            
            total_price += self.price!
            stack.addArrangedSubview(cart)
            
            }
        
        
        print(total_price)
        
        lblPriceNo.text = "Rs \(String(describing: total_price) )"
//        lblSubTotal.text = "Rs \(String(describing: self.price!))"
        lblTotalPriceNo.text = "Rs \(String(describing: total_price))"
        
        
        dict["total"] = "\(total_price)"
        dict["delivery"] = 0
        
        dict["coupon"] = self.discountedPercentage
    
//        let dic = ["total": "\(lblTotalPriceNo.text!)", "copoun": txtFieldPromo.text!]
//        self.dict = dic
    }
    
    func ApplyPercentage(value: Float, percentage: Float)-> Int{
         var afterDiscount = percentage/100
             print(afterDiscount)
        
             afterDiscount = value * afterDiscount
             print(afterDiscount)
        
        let  subAmount = self.price! - Int(afterDiscount)
        return Int(subAmount)
    }
    
    func findPercentage(discountValue: Float, originalValue: Float)-> Int{
        //var value = -0.0001
        var original_value:Float = 1.0
        if Int(originalValue) != 0{
            original_value = originalValue
        }
        let percentage  = 1.0 - (discountValue / original_value)
        let v = (percentage * 100.0)
        let p = Int(v)
        return p
    }
    

    
    func validateCoupon(){
        
        guard  let promo = txtFieldPromo.text, promo != "" else {
            let banner = NotificationBanner(title: "Warning", subtitle: "Please Enter Promo Code", style: .warning)
            banner.show()
            return
        }
        
        let param = ["code": promo]
        SuperMedServices.validatePromoCodeORCoupon(param: param){ (success, response, error) in
            
            if !success{
                let banner = NotificationBanner(title: "Alert", subtitle: "Please Enter Valid Promo Code", style: .danger)
                banner.show()
                return
            }else{
                
                print("validate Coupan :\(response!)")
                
                let array = response!["coupon"].arrayValue
                let value = array[0]["discount"].stringValue
//                self.lblDiscountNo.text = "Rs \(value)"
                
                let pecentage = (value as NSString).floatValue
                let discountamount = self.ApplyPercentage(value: Float(self.price!), percentage: pecentage)
                print(discountamount)
//                self.lblSubTotal.text = "Rs \(discountamount)"
                self.lblTotalPriceNo.text = "Rs \(discountamount)"
                self.lblCouponDisc.text = "\(value) %"
                self.discountedPercentage = "\(value)"
                
                let banner = NotificationBanner(title: "Success", subtitle: "Congratulations you got \(value)% discount successfully", style: .success)
                banner.show()
            }
            
        }
     
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "place_order"{
             print("valid")
        }

        if let target = segue.destination as? PlaceOrderViewController {
              print("Excellent")
               target.dict = dict




        }
    }
    
    
}
