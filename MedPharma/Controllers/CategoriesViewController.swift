//
//  CategoriesViewController.swift
//  MedPharma
//
//  Created by Protege Global on 26/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class CategoriesViewController: UIViewController {

    
    var arrayCategories = [Categories]()
    
    @IBOutlet weak var tableCategory: UITableView!
    
    
    class func instantiateFromStoryboard() -> CategoriesViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CategoriesViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.title = "Categories"
        let nib = UINib(nibName: "CategoriesTableCell", bundle: nil)
        
        tableCategory.register(nib, forCellReuseIdentifier: "CategoriesTableCell")
        
        self.PlaceNavigationButtons(selectorForLeft: #selector(MenuAction), leftImage: MENU_IMAGE, selectorForRight:  #selector(goToCart) , rightImage: CART_IMAGE)
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        getCategories()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @objc func goToCart() {
        let cart = CartViewController.instantiateFromStoryboard()
        cart.from = "Home"
        self.navigationController?.pushViewController(cart, animated: false)
    }
    
    func getCategories(){
        
        Alert.showLoader(message: "Loading")
        
        SuperMedServices.GetCategories(param: ["":""], completionHandler:{(sucess, response, error) in
            Alert.hideLoader()
            self.arrayCategories.removeAll()
            if !sucess{
                print("Failure response:\(response)")
                
                return
                
            }else{
                
                let categoriesJSon = response!["categories"].arrayValue
                if categoriesJSon.count > 0{
                    
                    for category in categoriesJSon{
                        
                        let catClass = Categories(json: category)
                            self.arrayCategories.append(catClass)
                        
                    }
                }
                self.tableCategory.reloadData()
                print("Success Rsponse:\(response!)")
            }
        })
    }
}

extension  CategoriesViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableCategory.dequeueReusableCell(withIdentifier: "CategoriesTableCell") as! CategoriesTableCell
        setCellData(index: indexPath, cell: cell)
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCategories.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 56)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = SubCategoryViewController.instantiateFromStoryboard()
            vc.cat_id = arrayCategories[indexPath.row].catId!
            vc.cat_title = arrayCategories[indexPath.row].name!
        
            self.navigationController?.pushViewController(vc, animated: true)
      
    }
    
    
    func setCellData(index: IndexPath, cell: CategoriesTableCell){
         cell.lblTitle.text = arrayCategories[index.row].name
         cell.lblSubTitle.text = arrayCategories[index.row].briefIntro
        
    }
    
}
