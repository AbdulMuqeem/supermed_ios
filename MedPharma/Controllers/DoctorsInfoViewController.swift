//
//  DoctorsInfoViewController.swift
//  MedPharma
//
//  Created by Protege Global on 27/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON
class DoctorsInfoViewController: UIViewController {

    
    class func instantiateFromStoryboard() -> DoctorsInfoViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DoctorsInfoViewController
    }
    
    @IBOutlet weak var btnSpec: UIButton!
    @IBOutlet weak var btnDoctor: UIButton!
    @IBOutlet weak var lblDoctor: UILabel!
    @IBOutlet weak var lblSpec: UILabel!
    @IBOutlet weak var lblVisitFees: UILabel!
    
    @IBOutlet weak var lblWeekends: UILabel!
    @IBOutlet weak var lblAvailability: UILabel!
    @IBOutlet weak var lblFees: UILabel!
    @IBOutlet weak var lblDaysAvailability: UILabel!
    @IBOutlet weak var lblDegree: UILabel!
    @IBOutlet weak var lblHospital: UILabel!
    let dropSpec = DropDown()
       var arraySpec = [Specialization]()
       var listSpec = [String]()
       var arrayDoctors = [Doctors]()
       var listDoctors = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(didTapOnBack))

        
        self.title = "Doctor's Info"
        getSpecializationsDoctors()
        
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didTapOnBack() {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func didTappedOnDetail(_ sender: Any) {
    }
    
    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @objc func NotificationAction() {
        
        
    }
    @IBAction func didTappedOnDoctor(_ sender: Any) {
        let drop = DropDown()
        drop.anchorView = btnDoctor
        if listDoctors.count <= 0 {
            return
        }
        
        drop.dataSource = listDoctors
        drop.width = btnDoctor.frame.size.width
        drop.bottomOffset = CGPoint(x: 2, y: 48)
        drop.direction = .bottom
        drop.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected Doctor: \(item) at index:  \(index)")
            self.lblDoctor.text = "\(item)"
         
            _ = self.arrayDoctors.map({
                
                let fullName = $0.firstName! + " " + $0.lastName!
                if fullName == item{
                      self.getDoctorInfoByID(doc_id: $0.id!)
                }
            })
        }
        drop.show()
    }
    
    @IBAction func didTappedOnSpec(_ sender: Any) {
       
        dropSpec.anchorView = btnSpec
        dropSpec.dataSource = listSpec
        dropSpec.width = btnSpec.frame.size.width
        dropSpec.bottomOffset = CGPoint(x: 2, y: 50)
        dropSpec.direction = .bottom
        dropSpec.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblSpec.text = "\(item)"
            _ = self.arraySpec.map({
                if $0.name == item{
                    //$0.id!
                    self.getDoctorsList(param:["spec_id": $0.id!] )
                }
            })
        }
        dropSpec.show()
    }
    
    
    func getSpecializationsDoctors(){
        
        Alert.showLoader(message: "")
        
        let param = ["":""]
        
        SuperMedServices.getSpecialization(param: param, completionHandler:{(success, response, error) in
            
            Alert.hideLoader()
            
            if !success{
                //print("response:\(response)")
                return
            }else{
                
                let spec = response!["specialization"].arrayValue
                if spec.count > 0{
                    self.listSpec.removeAll()
                    for s in spec{
                        let specialiation = Specialization(json: s)
                            self.arraySpec.append(specialiation)
                            self.listSpec.append(specialiation.name!)
                    }
                    if self.arraySpec.count > 1{
                        let spec = self.arraySpec[1]
                        self.lblSpec.text = spec.name
                        guard let id = spec.id else {return}
                        self.getDoctorsListFirstTime(param:["spec_id": id] )
                    }
                }
                print("response:\(response!)")
            }
        })
    }
    
    func getDoctorInfoByID(doc_id: String){
        
        Alert.showLoader(message: "")
        
        let param = ["doc_id": doc_id]
        
        SuperMedServices.doctorInfoById(param: param, completionHandler:{(success, response, error) in
            Alert.hideLoader()
            if !success{
                //print("response:\(response)")
                return
            }else{
                
                let info = response!["doctorsInfo"]
                let doctInfo = DoctorsInfo(json: info)
                print("response:\(info)")
                DispatchQueue.main.async {
                    self.setInfoOfDoctorOnView(doctorInfo: doctInfo)
                }
              }
        })
    }
    
    func setInfoOfDoctorOnView(doctorInfo: DoctorsInfo){
        UIView.animate(withDuration: 0.5, animations: {
            self.lblFees.text = "Clinic Fees: Rs \(doctorInfo.clinicFee ?? "0")"
            self.lblVisitFees.text = "Visit Fee: Rs \(doctorInfo.visitFee ?? "0")"
            self.lblDegree.text = doctorInfo.qualification!
            self.lblDaysAvailability.text = doctorInfo.daysANDTime!
            self.lblHospital.text = doctorInfo.clinicName!
        })
    }

    func getDoctorsList(param: Dictionary<String, Any>){
        Alert.showLoader(message: "")
        print(param)
        SuperMedServices.getDoctorsListBySpecID(param:param, completionHandler:{
           (success, response, error) in
        Alert.hideLoader()
            if !success{
                //print("response:\(response)")
                return
            }else{
                let spec = response!["doctors"].arrayValue
                if spec.count > 0{
                    self.listDoctors.removeAll()
                    self.arrayDoctors.removeAll()
                    for s in spec{
                        let doc = Doctors(json: s)
                          self.arrayDoctors.append(doc)
                        let fullName = doc.firstName! + " " + doc.lastName!
                          self.listDoctors.append(fullName)
                    }
                }
                print("response:\(response!)")
            }
        })
    }
    
    func getDoctorsListFirstTime(param: Dictionary<String, Any>){
        Alert.showLoader(message: "")
        print(param)
        SuperMedServices.getDoctorsListBySpecID(param:param, completionHandler:{
            (success, response, error) in
            Alert.hideLoader()
            if !success{
                //print("response:\(response)")
                return
            }else{
                let spec = response!["doctors"].arrayValue
                if spec.count > 0{
                    self.listDoctors.removeAll()
                    self.arrayDoctors.removeAll()
                    for s in spec{
                        let doc = Doctors(json: s)
                        self.arrayDoctors.append(doc)
                        let fullName = doc.firstName! + " " + doc.lastName!
                        self.listDoctors.append(fullName)
                    }
                    let doc = Doctors(json: spec.first!)
                    let doc_name = (doc.firstName ?? "") + " " + (doc.lastName ?? "")
                    self.lblDoctor.text = doc_name
                    guard let doc_id = doc.id else {return}
                    self.getDoctorInfoByID(doc_id: doc_id)
                }
                print("response:\(response!)")
            }
        })
    }
}

