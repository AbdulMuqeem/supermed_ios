//
//  FaceBookVC.swift
//  MedPharma
//
//  Created by mac  on 10/16/18.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class FaceBookVC: UIViewController , UIWebViewDelegate {
    
    class func instantiateFromStoryboard() -> FaceBookVC {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! FaceBookVC
    }
    
    @IBOutlet weak var webView: UIWebView!
    var type:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.PlaceLeftButton(image: #imageLiteral(resourceName: "icon_back"), selector: #selector(didTapOnBack))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        
        if self.type == "facebook" {
            self.title = "Facebook"
            self.fbURL()
        }
        else if self.type == "linkedin" {
            self.title = "Linkedin"
            self.linkedinURL()
        }
        else if self.type == "twitter" {
            self.title = "Twitter"
            self.twitterURL()
        }
        else if self.type == "instagram" {
            self.title = "Instagram"
            self.instagramURL()
        }

    }
    
    @objc func didTapOnBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func fbURL(){
        Alert.showLoader(message: "Loading")
        webView.loadRequest(URLRequest(url: NSURL(string: "https://www.facebook.com/Supermed.pk/")! as URL))
        Alert.hideLoader()
    }

    func linkedinURL(){
        Alert.showLoader(message: "Loading")
        webView.loadRequest(URLRequest(url: NSURL(string: "https://www.linkedin.com/company/supermedpk/")! as URL))
        Alert.hideLoader()
    }
    
    func twitterURL(){
        Alert.showLoader(message: "Loading")
        webView.loadRequest(URLRequest(url: NSURL(string: "https://twitter.com/Supermedpk")! as URL))
        Alert.hideLoader()
    }
    
    func instagramURL(){
        Alert.showLoader(message: "Loading")
        webView.loadRequest(URLRequest(url: NSURL(string: "https://www.instagram.com/Supermed.pk/?hl=en")! as URL))
        Alert.hideLoader()
    }
    
}
