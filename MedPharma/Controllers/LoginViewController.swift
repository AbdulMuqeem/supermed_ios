//
//  LoginViewController.swift
//  MedPharma
//
//  Created by Protege Global on 12/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import SwiftyJSON
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

class LoginViewController: UIViewController, GIDSignInUIDelegate {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    class func instantiateFromStoryboard() -> LoginViewController {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LoginViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //SocialManager.GoogleSetDelegate(delegate: self, uiDelegate: self)
        self.navigationController?.HideNavigationBar()
        
    }

    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
         self.navigationController?.HideNavigationBar()
    }
    
    @IBAction func didTapOnCreatAnAccount(_ sender: Any) {
        
        let controller = RegisterViewController.instantiateFromStoryboard()
                       self.navigationController?.pushViewController(controller, animated: true)
       
    }
    @IBAction func didTapOnGuestUser(_ sender: Any) {
//        let controller = SideMenuRootViewController.instantiateFromStoryboard()
//        controller.leftViewPresentationStyle = .slideAbove
//        AppDelegate.getInstatnce().window?.rootViewController = controller
        
        guestUser(param: ["":""])
        
    }
       
    func guestUser(param:Dictionary<String,Any>){
        
        Alert.showLoader(message: "Logging")
        
        SuperMedServices.GetGuestUser(param: param, completionHandler: {(success, response, error) in
            Alert.hideLoader()
            if !success{
                return
            }else{
                
                // Guest
               
                let user = UserDefaults.standard
                user.set(true, forKey: "AsGuest")
                
                let userResultObj = Profile(object:(response?["profile"])!)
                let res = response!["profile"].dictionaryValue
                let cart = res["cartCounter"]!.stringValue
                UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
                Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
                NotificationCenter.default.post(name: .cart, object: nil)
                let userDefaults = UserDefaults.standard
                userDefaults.set(cart, forKey: "cart")
                
                let controller = SideMenuRootViewController.instantiateFromStoryboard()
                controller.leftViewPresentationStyle = .slideAbove
                AppDelegate.getInstatnce().window?.rootViewController = controller
            }
        })
        
    }
    
    @IBAction func didTapOnForgotPassword(_ sender: Any) {
        let forgotPassword = AlertPassword(frame: (AppDelegate.getInstatnce().window?.bounds)!)
            forgotPassword.viewAlert.isHidden = true
            forgotPassword.btnForgotPassword.isHidden = true
            AppDelegate.getInstatnce().window?.addSubview(forgotPassword)
    }
    
    @IBAction func didTapOnSignIn(_ sender: Any) {
        
        
        guard let email = self.txtEmail.text, email != "", AppHelper.isValidEmail(testStr: email) else {
            let banner = NotificationBanner(title: "Email", subtitle: "Please enter valid email", style: .danger)
            banner.show()
            return
        }
        
        guard let password = self.txtPassword.text, password != "" else {
            let banner = NotificationBanner(title: "Email", subtitle: "Please enter passwords", style: .danger)
            banner.show()
            return
        }
        
        
        let param = ["email":email, "password": password]
        self.loginUser(param: param)
        
    }
    
    func loginUser(param:Dictionary<String,Any>){
        
        Alert.showLoader(message: "Logging")
        
        UserServices.Login(param: param, completionHandler: {(success, response, error) in
            Alert.hideLoader()
            if !success{
                let banner = NotificationBanner(title: "Warning", subtitle: "Username and Password doesn't match", style: .danger)
                banner.show()
                return
            }
            else{
                // User
                let user = UserDefaults.standard
                user.set(false, forKey: "AsGuest")
                
                let res = response!["profile"].dictionaryValue
                let cart = res["cartCounter"]!.stringValue
                print("cart:\(cart)")
                
                NotificationCenter.default.post(name: .cart, object: nil)
                let userDefaults = UserDefaults.standard
                    userDefaults.set(cart, forKey: "cart")
                
                self.changeRootViewController(response: response)

//                let userResultObj = Profile(object:(response?["profile"])!)
//                UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
//                Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
//
//                let controller = SideMenuRootViewController.instantiateFromStoryboard()
//                controller.leftViewPresentationStyle = .slideAbove
//                AppDelegate.getInstatnce().window?.rootViewController = controller
                
                /*
                 let userResultObj = Profile(object:(response?["profile"])!)
                 UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
                 Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
                 
                 let controller = SideMenuRootViewController.instantiateFromStoryboard()
                 controller.leftViewPresentationStyle = .slideAbove
                 AppDelegate.getInstatnce().window?.rootViewController = controller
                 */
            }
        })
    }
    
    @IBAction func didTapOnGoogleSignIn(_ sender: Any) {
        
        SocialManager.GoogleLogin()
        
//        let controller = SideMenuRootViewController.instantiateFromStoryboard()
//        controller.leftViewPresentationStyle = .slideAbove
//        AppDelegate.getInstatnce().window?.rootViewController = controller
    }
    
    @IBAction func didTapOnFacebookSignIn(_ sender: Any) {
        
        self.view.endEditing(true)
        Alert.showLoader(message: "")
        
        SocialManager.FacebookLogin(from: self, permisions: ["public_profile", "email"]) { (result, error) in
        
            // public_profile
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            Alert.showLoader(message: "")
            let parameters: [String: Any] = ["socialmediaid": result!.token.userID!]
            print(parameters)
            
            UserServices.LoginWSocialMedia(param: parameters) { (status, response, error) in
                if !status{
                    if error != nil {
                        print("Error: \(String(describing: error?.localizedDescription))")
                        Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                        Alert.hideLoader()
                        return
                    }
                    
                    SocialManager.FacebookGetUser(graphPath: "me", parameters: ["fields": "id, name, email"], callback: { (connection, result, error) in
                        if error != nil {
                            print("Error: \(String(describing: error?.localizedDescription))")
                            Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                            Alert.hideLoader()
                            return
                        }
//                        guard let userInfo = result as? [String: Any] else { return } //handle the error
//
//                        var data = [String: Any]()
//                        var name = ""
//
//                        if let firstName = (userInfo["first_name"]) as? String {
//                            print(firstName)
//                            name = firstName
//                            data["name"] = name
//                        }
//
//                        if let lastName = (userInfo["last_name"]) as? String {
//                            print(lastName)
//                            name = name + lastName
//                            data["name"] = name
//                        }
//
//                        if let id = (userInfo["id"]) as? String {
//                            print(id)
//                            data["id"] = id
//                        }
//
//                        if let email = (userInfo["email"]) as? String {
//                            print(email)
//                            data["email"] = email
//                        }
//
//                        let vc = RegisterViewController.instantiateFromStoryboard()
//                        vc.isSocialLogin = true
//                        vc.data = data
//                        self.navigationController?.pushViewController(vc, animated: true)
                    })
//                    return
                }
//                let userResultObj = Profile(object:(response?["profile"])!)
//                UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
//                Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
//
//                let controller = SideMenuRootViewController.instantiateFromStoryboard()
//                controller.leftViewPresentationStyle = .slideAbove
//                AppDelegate.getInstatnce().window?.rootViewController = controller
//                Alert.showAlert(title: "Alert", message: "Logged in successfully")
                
                UserDefaults.standard.set(true, forKey: "Social_Login")
                self.changeRootViewController(response: response)
            }
        }
    }
}

extension LoginViewController: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
//            let fullName = user.profile.givenName
//            let lastName = user.profile.familyName
//            let email = user.profile.email
            
            print("User ID: \(userId!)")
            print("ID Token: \(idToken!)")
            
            let parameters: [String: Any] = ["socialmediaid": idToken!]
            print(parameters)
            Alert.showLoader(message: "Loading")
            UserServices.LoginWSocialMedia(param: parameters) { (status, response, error) in
                Alert.hideLoader()
                if !status {
                    if error != nil {
                        print("Error: \(String(describing: error?.localizedDescription))")
                        Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                        return
                    }
//                    var data = [String: Any]()
//                    data["name"] = fullName
//                    data["id"] = idToken
//                    data["email"] = email
//                    data["lastName"] = lastName
//
//                    let vc = RegisterViewController.instantiateFromStoryboard()
//                    vc.isSocialLogin = true
//                    vc.data = data
//                    self.navigationController?.pushViewController(vc, animated: true)
                    return
                }
                
                UserDefaults.standard.set(true, forKey: "Social_Login")
                self.changeRootViewController(response: response)
                
//                let userResultObj = Profile(object:(response?["profile"])!)
//                UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
//                Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
//
//                let controller = SideMenuRootViewController.instantiateFromStoryboard()
//                controller.leftViewPresentationStyle = .slideAbove
//                AppDelegate.getInstatnce().window?.rootViewController = controller
//                Alert.showAlert(title: "Alert", message: "Logged in successfully")
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("Error: \(error.localizedDescription)")
    }
}

extension LoginViewController{
    
    private func changeRootViewController(response :JSON?){
        Alert.hideLoader()
        let userResultObj = Profile(object:(response?["profile"])!)
        UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
        Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
        
        let controller = SideMenuRootViewController.instantiateFromStoryboard()
        controller.leftViewPresentationStyle = .slideAbove
        AppDelegate.getInstatnce().window?.rootViewController = controller
    }
    
}
