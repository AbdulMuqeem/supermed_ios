//
//  TermsAndCondViewController.swift
//  MedPharma
//
//  Created by Protege Global on 12/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class TermsAndCondViewController: UIViewController {

    @IBOutlet weak var tvTerms: UITextView!
    
    class func instantiateFromStoryboard() -> TermsAndCondViewController {
        Alert.showLoader(message: "Loading")
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        Alert.hideLoader()
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! TermsAndCondViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Terms & Conditions"
        self.PlaceLeftButton(image: MENU_IMAGE, selector: #selector(MenuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        
        self.getTerms()
        
    }
    
    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    
    func getTerms(){
        
        Alert.showLoader(message: "Loading")
        NotificationServices.TermsAndConditions(param: ["":""]) { (success, response, error) in
        Alert.hideLoader()
            
            if !success {
                return
                
            }
            else {
                let resp = response!["contactUs"]
                let contactUs = ContactUs(json: resp)
                self.tvTerms.text = (contactUs.pageContent ?? "").htmlToString
            }
        }
    }
}
