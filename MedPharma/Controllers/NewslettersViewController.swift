//
//  NewslettersViewController.swift
//  MedPharma
//
//  Created by Protege Global on 12/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class NewslettersViewController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSubscribe: UIButton!
    
    
    class func instantiateFromStoryboard() -> NewslettersViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! NewslettersViewController
    }
    
    @IBAction func didTappedOnSubscribe(_ sender: Any) {
            subscribeLetter()
    }
    
    @IBAction func didTapOnTextField(_ sender: Any) {
        self.btnSubscribe.isEnabled = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnSubscribe.isEnabled = false
        self.PlaceLeftButton(image: MENU_IMAGE, selector: #selector(MenuAction))
//        self.PlaceNavigationButtons(selectorForLeft: #selector(MenuAction), leftImage: MENU_IMAGE, selectorForRight:  #selector(NotificationAction) , rightImage: NOTIFICATION_IMAGE)
        
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
            self.title = "News Letter Subscription"
        
        txtEmail.addPadding(.left(16))
    }

    
    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    
//    @objc func NotificationAction() {
//
//        //        let vc = AcceptfriendViewController.instantiateFromStoryboard()
//        //        self.navigationController?.pushViewController(vc, animated: true)
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
//    func textFieldDidBeginEditing() {
//
//        if (txtEmail.text?.isEmpty)! {
//        btnSubscribe.isEnabled = false
//    } else {
//        btnSubscribe.isEnabled = true
//    }
//}
    
    func subscribeLetter(){
        Alert.showLoader(message: "Loading")
        
        guard let email = txtEmail.text, email != "", AppHelper.isValidEmail(testStr: email) else {
            
            let banner = NotificationBanner(title: "Email", subtitle: "Please Enter Valid Email", style: .danger)
            banner.show()
            Alert.hideLoader()
            return
        }
        
        let param = ["email": email]
        Alert.showLoader(message: "Loading")
        SuperMedServices.subscribeNewsLetter(param: param, completionHandler:{(success, response, error) in
            Alert.hideLoader()
            if !success{
                
                let banner = NotificationBanner(title: "Email", subtitle: "\(response!["message"])", style: .danger)
                banner.show()
                
                return
            }else{
                
                let banner = NotificationBanner(title: "Success", subtitle: "\(response!["message"])", style: .success)
                banner.show()
            }
        })
        
    }
    
   

}
