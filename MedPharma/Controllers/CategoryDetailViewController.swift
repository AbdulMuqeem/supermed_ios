//
//  CategoryDetailViewController.swift
//  MedPharma
//
//  Created by Protege Global on 26/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import SwiftyJSON
import NotificationBannerSwift

class CategoryDetailViewController: UIViewController, ProductCellDelegate {

    @IBOutlet weak var lblWarning: UILabel!{
        didSet{
             lblWarning.isHidden = true
        }
    }
    @IBOutlet weak var collectionCategory: UICollectionView!
    class func instantiateFromStoryboard() -> CategoryDetailViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CategoryDetailViewController
    }
    
    var arrayProducts = [Products]()
    var subCat_id:String?
    var subTitle: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = subTitle
        self.PlaceNavigationButtons(selectorForLeft: #selector(didTapOnBack), leftImage: BACK_IMAGE, selectorForRight:  #selector(goToCart) , rightImage: CART_IMAGE)
        
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        if subTitle == "Wishlist"{
                    let nib = UINib(nibName: "ProductCollectionViewCell", bundle: nil)
                    collectionCategory.register(nib, forCellWithReuseIdentifier: "ProductCollectionViewCell")
                    getWishlistFromServer()
                    
        }else{
                    let nib = UINib(nibName: "ProductCategoryCollectionCell", bundle: nil)
                    collectionCategory.register(nib, forCellWithReuseIdentifier: "ProductCategoryCollectionCell")
                    getCategoriesDetail()
           
        }
    }
    
    @objc func goToCart() {
        let cart = CartViewController.instantiateFromStoryboard()
        cart.from = "Home"
        self.navigationController?.pushViewController(cart, animated: false)
    }

    @objc func didTapOnBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func NotificationAction() {
        
        //        let vc = AcceptfriendViewController.instantiateFromStoryboard()
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func getWishlistFromServer(){
        
        Alert.showLoader(message: "Loading")
        let param = ["userid": UserManager.getUserObjectFromUserDefaults()!.id!]
        
        SuperMedServices.getWishlist(param: param, completionHandler: {(success, response, error) in
            
            Alert.hideLoader()
            if !success{
                self.arrayProducts.removeAll()
                
                self.lblWarning.text = "No product found in wishlist"
                self.lblWarning.isHidden = false
                self.collectionCategory.reloadData()
                return
            }else{
                self.arrayProducts.removeAll()
                let wishlist = response!["wishlist"].arrayValue
                
                self.lblWarning.isHidden = true
                
                if wishlist.count > 0{
                    
                    
                    for category in wishlist{
                        
                        let catClass = Products(json: category)
                        self.arrayProducts.append(catClass)
                        
                    }
                }
                self.collectionCategory.reloadData()
            }
        })
    }
    
    
    
    func getCategoriesDetail(){
        let param = ["sub_cat_id": subCat_id!]
        print(param)
        
        Alert.showLoader(message: "Loading")
        
        SuperMedServices.GetProductSubCategoriesByID(param: param, completionHandler:{ (success, response, error) in
            
            Alert.hideLoader()
            if !success{
                print("response: \(String(describing: response))")
                self.lblWarning.text = "No product found in this category"
                self.lblWarning.isHidden = false
                return
            }
            self.arrayProducts.removeAll()
            let products = response!["products"].arrayValue
            
            self.lblWarning.isHidden = true
            
            if products.count > 0{
                for product in products{
                    let pro = Products(json: product)
                        self.arrayProducts.append(pro)
                }
                
                
            }
            self.collectionCategory.reloadData()
            print("response:\(response!)")
            
        })
        
        
    }
    
    
    func addToCartList(product_id: String) {
          print(product_id)
        
        let param = ["productid": product_id, "userid": UserManager.getUserObjectFromUserDefaults()!.id!, "qty": "1"]
        
        
        SuperMedServices.moveWishlistItem(param:param, completionHandler: {(success, response, error) in
            if !success{
                return
            }
            
            print("response: \(response!)")
            
            self.getWishlistFromServer()
            
        })
        
        
    }
    
    
    
    func deleteWishlistItem(product_id: String){
        
        
        let param = ["productid": product_id,"userid": UserManager.getUserObjectFromUserDefaults()!.id!]
        
          SuperMedServices.deleteWishlistItem(param:param, completionHandler:{(success, response,error) in
            if !success{
                 return
            }
            print("response: \(response!)")
            self.getWishlistFromServer()
            
        })
        
        
    }
    
    
   
}

extension CategoryDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if subTitle == "Wishlist"{
            let cell = collectionCategory.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as? ProductCollectionViewCell
                cell?.delegate = self
                cell?.productId = arrayProducts[indexPath.row].productID!
                setDataProductionCell(index: indexPath, cell: cell!)
          
           return cell!
            
            }else{
           
            let cell = collectionCategory.dequeueReusableCell(withReuseIdentifier: "ProductCategoryCollectionCell", for: indexPath) as? ProductCategoryCollectionCell
            setDataOnCell(index: indexPath, cell: cell!)
            return cell!
        }
 
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width:  SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size:175), height: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 300))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let productQty = Int(arrayProducts[indexPath.row].productQty ?? "0") else {return}
        if productQty == 0{return}
        let product_detail = ProductDetailViewController.instantiateFromStoryboard()
        product_detail.product = [arrayProducts[indexPath.row]]
        self.navigationController?.pushViewController(product_detail, animated: true)
    }
    
    func setDataOnCell(index: IndexPath, cell:ProductCategoryCollectionCell)
    {
        
        let base_image = "https://www.supermed.pk/pos/public/uploads/products/"
        
        let price = arrayProducts[index.row].productPrice!
        let textRange = NSMakeRange(0, price.count)
        let attributedText = NSMutableAttributedString(string: price)
        attributedText.addAttribute(NSAttributedStringKey.strikethroughStyle,
                                    value: NSUnderlineStyle.styleSingle.rawValue,
                                    range: textRange)
        
       let attributes = [NSAttributedStringKey.foregroundColor : UIColor.red]
        attributedText.addAttributes(attributes, range: textRange)
        
        if base_image == arrayProducts[index.row].productImage {
            cell.imageProduct.setImageFromUrl(urlStr: "https://www.supermed.pk/pos/public/images/tab-missing.png")
        } else {
            cell.imageProduct.setImageFromUrl(urlStr: arrayProducts[index.row].productImage ?? "https://www.supermed.pk/pos/public/images/tab-missing.png")
        }
        
        cell.lblTitleProduct.text = arrayProducts[index.row].productName
        cell.lblPrice.text = "Rs \(arrayProducts[index.row].productPrice ?? "0")"
       // cell.lbl_productDisc.attributedText = attributedText //+ "(30%)"
        //cell.imageProduct.setImageFromUrl(urlStr: arrayProducts[index.row].productImage!)
        cell.btnAddToCart.tag = index.row
        cell.btnAddToCart.addTarget(self, action: #selector(self.onBtnAddToCart(_:)), for: .touchUpInside)
        guard let productQty = Int(arrayProducts[index.row].productQty ?? "0") else {return}
        if productQty == 0{
            cell.btnAddToCart.isUserInteractionEnabled = false
            cell.btnAddToCart.setTitle("OUT OF STOCK", for: .normal)
        }
        else{
            cell.btnAddToCart.isUserInteractionEnabled = true
            cell.btnAddToCart.setTitle("ADD TO CART", for: .normal)
        }
    }
    
    
    func setDataProductionCell(index: IndexPath, cell: ProductCollectionViewCell){
         cell.lbl_productTitle.text = arrayProducts[index.row].productName!
         cell.lbl_productPrice.text = "Rs \(arrayProducts[index.row].productPrice!)"
//         cell.lbl_productDisc.text = "Rs \(arrayProducts[index.row].productOldPrice!)"
         cell.image_product.setImageFromUrl(urlStr: arrayProducts[index.row].productImage!)
    }
}
//Action add to cart
extension CategoryDetailViewController{
    @objc func onBtnAddToCart(_ sender: UIButton){
        let product = self.arrayProducts[sender.tag]
        self.addCartItem(product: product)
    }
    func addCartItem(product: Products){
        let param : [String: Any] = ["userid": UserManager.getUserObjectFromUserDefaults()!.id!, "productid": product.productID ?? "0", "qty": "1", "action": "0"]
        Alert.showLoader(message: "Loading")
        SuperMedServices.cart_action(param: param, completionHandler: {(success, response, error) in
            Alert.hideLoader()
            if !success {
                //print(response)
                Alert.showAlert(title: "Alert", message: (error?.localizedDescription) ?? "Server error")
                return
            }else{
                Alert.showAlert(title: "Alert", message: "Product Updated Successfully")
                print("response: \(response!)")
            }
        })
    }
}
