//
//  JoinUsViewController.swift
//  MedPharma
//
//  Created by Protege Global on 12/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class JoinUsViewController: UIViewController {

    class func instantiateFromStoryboard() -> JoinUsViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! JoinUsViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Join Us"
        self.PlaceLeftButton(image: MENU_IMAGE, selector: #selector(MenuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        
    }
    
    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @IBAction func didTapOnFb(_ sender: Any) {
        let vc = FaceBookVC.instantiateFromStoryboard()
        vc.type = "facebook"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapOnLinkedIn(_ sender: Any) {
        let vc = FaceBookVC.instantiateFromStoryboard()
        vc.type = "linkedin"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapOntwitter(_ sender: Any) {
        let vc = FaceBookVC.instantiateFromStoryboard()
        vc.type = "twitter"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapOnInstagram(_ sender: Any) {
        let vc = FaceBookVC.instantiateFromStoryboard()
        vc.type = "instagram"
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
