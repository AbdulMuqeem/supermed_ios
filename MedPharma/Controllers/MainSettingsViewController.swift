//
//  MainSettingsViewController.swift
//  MedPharma
//
//  Created by Protege Global on 26/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import SCLAlertView


class MainSettingsViewController: UIViewController {
    
    @IBOutlet weak var collectionCategory: UICollectionView!
    class func instantiateFromStoryboard() -> MainSettingsViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MainSettingsViewController
    }
    
    let array = ["Edit Profile", "Change Password"]
    
    @IBOutlet weak var tableSettings: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Settings"
        self.PlaceLeftButton(image: MENU_IMAGE, selector: #selector(MenuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        
        let nib = UINib(nibName: "SettingTableViewCell", bundle: nil)
        tableSettings.register(nib, forCellReuseIdentifier: "SettingTableViewCell")
        
    }
    
    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    
}

extension MainSettingsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableSettings.dequeueReusableCell(withIdentifier: "SettingTableViewCell") as! SettingTableViewCell
        cell.lblTitle.text = array[indexPath.row]
        cell.selectionStyle = .none
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 56)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            
             let userDefaults = UserDefaults.standard
             let social = userDefaults.bool(forKey: "Social_Login")
            
             if social {
                Alert.showAlert(title: "Alert", message: "You have logged in as Social Login, You can't update")
                return
                }
            
        
            let user = userDefaults.bool(forKey: "AsGuest")
            print(user)
            
            if user {
                Alert.showAlert(title: "Alert", message: "You have logged in as Guest Member, in order to access this feature. you need to Login OR Register on App")
                return
            }
            
            if indexPath.row == 0{
                let editProfile = EditProfileViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(editProfile, animated: true)
            }
            
            let cell = self.tableSettings.cellForRow(at: indexPath) as! SettingTableViewCell
            cell.viewBackground.backgroundColor = UIColor.sideMenu
            cell.setSelected(true, animated: true)
            
            
            if indexPath.row == 1{
                let alertView = AlertPassword(frame:(AppDelegate.getInstatnce().window?.bounds)!)
                alertView.viewForgotPassword.isHidden = true
                alertView.btnChangePassword.isHidden = false
                self.view.addSubview(alertView)
            }
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableSettings.cellForRow(at: indexPath) as! SettingTableViewCell
        cell.viewBackground.backgroundColor = UIColor.white
        
    }
    
    func customizeAlertView(){
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
            kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
            kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
            showCloseButton: false
        )
        
        // Initialize SCLAlertView using custom Appearance
        let alert = SCLAlertView(appearance: appearance)
        
        // Creat the subview
        let subview = UIView(frame: CGRect(x: 0,y:0,width:216,height:70))
        let x = (subview.frame.width - 180) / 2
        
        // Add textfield 1
        let textfield1 = UITextField(frame: CGRect(x: x, y: 10, width: 180, height: 25))
        textfield1.layer.borderColor = UIColor.green.cgColor
        textfield1.layer.borderWidth = 1.5
        textfield1.layer.cornerRadius = 5
        textfield1.placeholder = "Username"
        textfield1.textAlignment = NSTextAlignment.center
        subview.addSubview(textfield1)
        
        // Add textfield 2
        let textfield2 = UITextField(frame: CGRect(x:x,y:textfield1.frame.maxY + 10,width:180,height:25))
        textfield2.isSecureTextEntry = true
        textfield2.layer.borderColor = UIColor.blue.cgColor
        textfield2.layer.borderWidth = 1.5
        textfield2.layer.cornerRadius = 5
        textfield1.layer.borderColor = UIColor.blue.cgColor
        textfield2.placeholder = "Password"
        textfield2.textAlignment = NSTextAlignment.center
        subview.addSubview(textfield2)
        
        // Add the subview to the alert's UI property
        alert.customSubview = subview
        alert.addButton("Login") {
            print("Logged in")
        }
        
        // Add Button with Duration Status and custom Colors
        alert.addButton("Duration Button", backgroundColor: UIColor.brown, textColor: UIColor.yellow) {
            print("Duration Button tapped")
        }
        
        alert.showInfo("Login", subTitle: "", closeButtonTitle: "login")
    }
    
    
}


