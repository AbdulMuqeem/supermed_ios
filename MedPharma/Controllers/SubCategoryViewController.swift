//
//  SubCategoryViewController.swift
//  MedPharma
//
//  Created by Protege Global on 24/07/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class SubCategoryViewController: UIViewController {

    @IBOutlet weak var tableSubCategory: UITableView!
     var arrayCategories = [Subcategories]()
     var cat_id : String?
     var cat_title: String?
    
    class func instantiateFromStoryboard() -> SubCategoryViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SubCategoryViewController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.title = cat_title!
        let nib = UINib(nibName: "CategoriesTableCell", bundle: nil)
        
        tableSubCategory.register(nib, forCellReuseIdentifier: "CategoriesTableCell")
        self.PlaceNavigationButtons(selectorForLeft: #selector(didTapOnBack), leftImage: BACK_IMAGE, selectorForRight:  #selector(NotificationAction) , rightImage: CART_IMAGE)
        
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        tableSubCategory.delegate = self
        tableSubCategory.dataSource = self
        getSubCategories()
        
    }
    @objc func didTapOnBack() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func NotificationAction() {
        
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getSubCategories(){
        
        Alert.showLoader(message: "Loading")
        let param = ["cat_id":cat_id!]
        print(param)
        
        SuperMedServices.GetCategoriesByID(param: param, completionHandler:{(sucess, response, error) in
            Alert.hideLoader()
            self.arrayCategories.removeAll()
            if !sucess{
                print("Failure response:\(response)")
                return
            }else{
                
                let categoriesJSon = response!["subcategories"].arrayValue
                if categoriesJSon.count > 0{
                    
                    for category in categoriesJSon{
                        
                        let catClass = Subcategories(json: category)
                        self.arrayCategories.append(catClass)
                        
                    }
                }
                self.tableSubCategory.reloadData()
                print("Success Rsponse:\(response!)")
            }
            
            
        })
        
        
    }
    
}

extension  SubCategoryViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableSubCategory.dequeueReusableCell(withIdentifier: "CategoriesTableCell") as! CategoriesTableCell
        setCellData(index: indexPath, cell: cell)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCategories.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 56)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = CategoryDetailViewController.instantiateFromStoryboard()
              vc.subCat_id = arrayCategories[indexPath.row].subCatId!
              vc.subTitle = arrayCategories[indexPath.row].subCatName!
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    func setCellData(index: IndexPath, cell: CategoriesTableCell){
        cell.lblTitle.text = arrayCategories[index.row].subCatName
        cell.lblSubTitle.text = arrayCategories[index.row].briefIntro
        
        
    }
    
}
