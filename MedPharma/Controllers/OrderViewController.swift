//
//  OrderViewController.swift
//  MedPharma
//
//  Created by Protege Global on 12/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class OrderViewController: UIViewController {

    class func instantiateFromStoryboard() -> OrderViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! OrderViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.PlaceNavigationButtons(selectorForLeft: #selector(MenuAction), leftImage: MENU_IMAGE, selectorForRight:  #selector(NotificationAction) , rightImage: NOTIFICATION_IMAGE)
        
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        // Do any additional setup after loading the view.
    }
    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @objc func NotificationAction() {
        
        //        let vc = AcceptfriendViewController.instantiateFromStoryboard()
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
