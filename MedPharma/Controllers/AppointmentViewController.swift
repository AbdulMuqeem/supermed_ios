//
//  AppointmentViewController.swift
//  MedPharma
//
//  Created by Protege Global on 27/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import SwiftyJSON

class AppointmentViewController: UIViewController {
    class func instantiateFromStoryboard() -> AppointmentViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AppointmentViewController
    }
     let array = ["Ambulance Helpline","Police Helpline","Fire Bridge Helpline","Hospital Helpline"]
     var arrayAppointment = [History]()
    
    
    
    
    @IBOutlet weak var tableAppointment: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Appointment History"
        self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(didTapOnBack))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        
        let nib = UINib(nibName: "AppointmentSectionTableCell", bundle: nil)
        tableAppointment.register(nib, forCellReuseIdentifier: "AppointmentSectionTableCell")
        
        let nib2 = UINib(nibName: "AppointmentDetailTableViewCell", bundle: nil)
        tableAppointment.register(nib2, forCellReuseIdentifier: "AppointmentDetailTableViewCell")
        tableAppointment.delegate = self
        tableAppointment.dataSource = self
        tableAppointment.estimatedRowHeight = 100
        tableAppointment.rowHeight = UITableViewAutomaticDimension
        
        getAppointmentHistory()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func didTapOnBack() {
        self.navigationController?.popViewController(animated: true)
    }

}
extension AppointmentViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 //arrayAppointment.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayAppointment.count
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let view:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.size.width, height: 10))
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableAppointment.dequeueReusableCell(withIdentifier: "AppointmentSectionTableCell") as? AppointmentSectionTableCell
           cell?.lblAppointmentNo.text = "Appointment No: " + arrayAppointment[section].appointmentId!
        return cell?.contentView
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
         let cellDetail = tableAppointment.dequeueReusableCell(withIdentifier: "AppointmentDetailTableViewCell") as! AppointmentDetailTableViewCell
        
        cellDetail.lblDate.text = arrayAppointment[indexPath.section].date!
        cellDetail.lblTitle.text = arrayAppointment[indexPath.section].testname!
        cellDetail.lblID.text = "Patient ID: " + arrayAppointment[indexPath.section].patientId!
        cellDetail.lblName.text = "Patient Name: " + arrayAppointment[indexPath.section].patientName!
        cellDetail.lblLaboratoryName.text = "Labortory:" + arrayAppointment[indexPath.section].labname!
        return cellDetail
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    
    
 
    // MARK:- Service For Appointment history
    
    func getAppointmentHistory(){
        let param = ["patient_id": UserManager.getUserObjectFromUserDefaults()!.patient_id!]
             print(param)
        SuperMedServices.appointmentHistory(param: param){ (success, response, error) in
            if !success{
                return
            }else{
                let history = response!["history"].arrayValue
                if history.count > 0{
                    for item in history{
                        let his = History(json: item)
                        self.arrayAppointment.append(his)
                    }
                    print(self.arrayAppointment)
                }
                else{
                    Alert.showAlert(title: "Alert", message: "No History Found.")
                }
                self.tableAppointment.reloadData()
                print("response Appoinment history:\(response!) ")
            }
        }
    }
}





