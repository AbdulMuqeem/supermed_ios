//
//  LabTestViewController.swift
//  MedPharma
//
//  Created by Protege Global on 27/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import DropDown
import AAPickerView
import ActionSheetPicker_3_0
import SwiftyJSON
import NotificationBannerSwift
import SnapKit

class LabTestViewController: UIViewController, TestsViewDelegate {

    @IBOutlet weak var scroll_view: UIScrollView!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var viewAddTest: UIView!
    
    @IBOutlet weak var stackHeight: NSLayoutConstraint!
    
    @IBOutlet weak var stackViewTest: UIStackView!
    @IBOutlet weak var viewLab: UIView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPatientName: UILabel!
    @IBOutlet weak var lblPatientId: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblLab: UILabel!
    @IBOutlet weak var btnLab: UIButton!
    @IBOutlet weak var btnTest: UIButton!
    @IBOutlet weak var lblTest: UILabel!
    @IBOutlet weak var txtMriId: UITextField!
    @IBOutlet weak var txtDetail: UITextField!
    @IBOutlet weak var imageCash: UIImageView!
    @IBOutlet weak var imageCredit: UIImageView!
    @IBOutlet weak var btnCash: UIButton!
    @IBOutlet weak var btnCredit: UIButton!
    
    var lab_id: String?
    var test_id: String?
    var test_price: String?
    var dic = [String: Any]()
    var arrayTest = [Any]()
    var arrayTestList = [Any]()
    var receipt = ""
    
    @IBOutlet weak var lblPrice: UILabel!
    
    
    var arrayLabortories = [Labs]()
    var listLabNames = [String]()
    var arrayOfTests = [Tests]()
    var listTests = [String]()
    var dropListTests = DropDown()
    
    var payType = "cash"
    
    
    class func instantiateFromStoryboard() -> LabTestViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LabTestViewController
    }
    
    func checkData(type:String) {
        
        guard let labID = self.lab_id, labID != ""  else {
            let banner = NotificationBanner(title: "Laboratory", subtitle: "Laboratory id is missing", style: .danger)
            banner.show()
            return
        }
        
        
        guard let testID = self.test_id, testID != "" else {
            let banner = NotificationBanner(title: "Lab Test", subtitle: "Laboratory Test id is missing", style: .danger)
            banner.show()
            return
        }
        
        guard let price_test = self.test_price, price_test != "" else {
            let banner = NotificationBanner(title: "Price", subtitle: "Price is missing", style: .danger)
            banner.show()
            return
        }
        
        guard let date = self.lblDate.text, date != "" else {
            let banner = NotificationBanner(title: "Date", subtitle: "Please select Date", style: .danger)
            banner.show()
            return
        }
        
        if date == "Select Appointment Date" {
            let banner = NotificationBanner(title: "Date", subtitle: "Please select Date", style: .danger)
            banner.show()
            return
        }
        
        guard let comment = self.txtDetail.text, comment != "" else {
            let banner = NotificationBanner(title: "History", subtitle: "Please enter history detail of pateint", style: .danger)
            banner.show()
            return
        }
        
        
        let dict = ["lab_id":labID ,"test_id" : testID, "price": price_test , "date": date, "time": "10:10", "problem": comment , "paytype":self.payType]
        
        arrayTest.append(dict)
        
        let dictionary  = ["lab_name": lblLab.text!, "test_name": lblTest.text!, "price": price_test]
        arrayTestList.append(dictionary)
        addTestInStack(dic: arrayTestList)
        
        let param = ["patient_id": UserManager.getUserObjectFromUserDefaults()!.patient_id!, "patient_name": UserManager.getUserObjectFromUserDefaults()!.first_name!,"gender": "male", "patient_address": lblAddress.text!, "pay_type": "cash","tests": arrayTest,"mri_id":self.txtMriId.text ?? ""] as [String : Any]
        
        self.dic = param
        
        if type == "Test" {
            Alert.showToast(message: "Test Added")
        }
        else if type == "Submit" {
            print(self.dic)
            self.bookAppointment(param: self.dic)
        }
        
    }
    
    @IBAction func didTappedOnAddTest(_ sender: Any) {
        
        let user = UserDefaults.standard.bool(forKey: "AsGuest")
        print(user)
        
        if user {
            Alert.showAlert(title: "Alert", message: "You have logged in as Guest Member, in order to access this feature. you need to Login OR Register on App")
            return
        }
        
        self.checkData(type: "Test")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Lab Test"
        self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(didTapOnBack))
      
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        customizeView()
        getLabortriesListFirstTime()
    }

    
    func customizeView(){
        
        let userDefaults = UserDefaults.standard
        let user = userDefaults.bool(forKey: "AsGuest")
        print(user)
        
        if user {
            
            lblPatientId.text = "Patient ID"
            lblPatientName.text = "Guest User"
            lblAddress.text = "Patient Address"
            
        }
        else {
           
            lblPatientId.text = UserManager.getUserObjectFromUserDefaults()?.patient_id!
            lblPatientName.text = (UserManager.getUserObjectFromUserDefaults()?.first_name!)! + " " +  (UserManager.getUserObjectFromUserDefaults()?.last_name)!
            lblAddress.text = UserManager.getUserObjectFromUserDefaults()?.address
            
        }

    }
    
    @IBAction func didTapOnSubmit(_ sender: Any) {
       
        let user = UserDefaults.standard.bool(forKey: "AsGuest")
        print(user)
        
        if user {
            Alert.showAlert(title: "Alert", message: "You have logged in as Guest Member, in order to access this feature. you need to Login OR Register on App")
            return
        }
        
        self.checkData(type: "Submit")
    }

    
    @objc func didTapOnBack() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    @IBAction func didTappedOnTest(_ sender: Any) {
        dropListTests.show()
    }
    
    @IBAction func didTappedOnCalendar(_ sender: Any) {
        
        ActionSheetDatePicker.show(withTitle: "Select DOB", datePickerMode: .date, selectedDate: Date(), minimumDate: nil, maximumDate: Date(), doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.lblDate.text = newDate
            
        
            
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender as! UIView)
        
    }
    @objc func NotificationAction() {
        
      
    }

    @IBAction func didTappedOnLab(_ sender: Any) {
        
        getLabortriesList()
        
        
    }
    
    func getLabortriesList(){
        
        Alert.showLoader(message: "")
        
        SuperMedServices.getLABTest(param: ["":""], completionHandler:{(success, response, error ) in
            Alert.hideLoader()
            if !success{
                return
            }
            self.arrayLabortories.removeAll()
            self.listLabNames.removeAll()
            let labs = response!["labs"].arrayValue
            if labs.count > 0{
                for lab in labs{
                    let l = Labs(json: lab)
                    self.arrayLabortories.append(l)
                    self.listLabNames.append(l.labName!)
                }
                let drop = DropDown()
                drop.anchorView = self.btnLab
                drop.dataSource = self.listLabNames
                drop.width = self.viewLab.frame.size.width
                drop.bottomOffset = CGPoint(x: 0, y: 55)
                drop.direction = .bottom
                drop.selectionAction = { [unowned self] (index: Int, item: String) in
                    print("Selected item: \(item) at index: \(index)")
                    self.lblLab.text = "\(item)"
                    
                    _ = self.arrayLabortories.map({
                        if $0.labName == item{
                            self.getLabListByLabortoryId(lab_id: $0.labId!)
                            self.lab_id = $0.labId!
                        }
                    })
                }
                drop.show()
            }
            //print("list:\(response)")
        })
    }
    
    func getLabortriesListFirstTime(){
        
        Alert.showLoader(message: "")
        
        SuperMedServices.getLABTest(param: ["":""], completionHandler:{(success, response, error ) in
            Alert.hideLoader()
            if !success{
                return
            }
            self.arrayLabortories.removeAll()
            self.listLabNames.removeAll()
            let labs = response!["labs"].arrayValue
            if labs.count > 0{
                for lab in labs{
                    let l = Labs(json: lab)
                    self.arrayLabortories.append(l)
                    self.listLabNames.append(l.labName!)
                }
                if self.listLabNames.count > 0{
                    self.lblLab.text = self.listLabNames.first ?? "-"
                    guard let lab = response!["labs"].arrayValue.first else {return}
                    let labId = Labs(json: lab).labId ?? "0"
                    self.lab_id = labId
                    self.getLabListByLabortoryIdFirstTime(lab_id: labId)
                }
            }
            //print("list:\(response)")
        })
    }
    func deleteTest(id: String, index: Int) {
        if let newSelectedView = stackViewTest.arrangedSubviews[index] as? TestView {
               stackViewTest.removeArrangedSubview(newSelectedView)
        }
        scroll_view.contentSize = CGSize(width: self.view.frame.size.width - 16, height: scroll_view.contentSize.height - 100)
        stackHeight.constant -= 100
        arrayTestList.remove(at: index)
        stackViewTest.reloadInputViews()
        //101stackViewTest.removeAllArrangedSubviews()
        for i in stackViewTest.subviews{
            i.removeFromSuperview()
        }
        for (index,item) in self.arrayTestList.enumerated(){
            
            let view = TestView.instanceFromNib()
            view.index = index
            view.delegate = self
            view.testID = "hahahahah"
            
            if let i = item as? Dictionary<String, Any>{
                view.lbl_title.text = i["lab_name"] as? String
                view.lbl_test_title.text = i["price"] as? String
                view.lbl_test_price.text = i["test_name"] as? String
                
            }
            
            stackViewTest.insertArrangedSubview(view, at:index)
            
        }
    }
    func addTestInStack(dic:Array<Any>){
      
       stackViewTest.removeAllArrangedSubviews()
       stackHeight.constant = 0
    
        for (index,item) in dic.enumerated(){
            
            let view = TestView.instanceFromNib()
                 view.index = index
                 view.delegate = self
                 view.testID = ""
            
            if let i = item as? Dictionary<String, Any>{
                 view.lbl_title.text = i["lab_name"] as? String
                 view.lbl_test_title.text = i["test_name"] as? String
                 view.lbl_test_price.text = "Price Rs: " + (i["price"] as? String ?? "0")
                
            }
            
                 stackViewTest.insertArrangedSubview(view, at:index)
        
        }
        stackViewTest.backgroundColor = UIColor.purple
        stackHeight.constant += CGFloat(100 * dic.count)
        scroll_view.contentSize = CGSize(width: view.frame.size.width - 16, height: mainStackView.frame.size.height + stackHeight.constant + 100)
       
    }
    
    @IBOutlet weak var heightStack: NSLayoutConstraint!
    
    func getLabListByLabortoryId(lab_id: String){
        
        let param = ["lab_id": lab_id]
        
        SuperMedServices.LabTestByLab_ID(param: param, completionHandler:{(success, response, error) in
            if !success{
                print("response :\(String(describing: response))")
                return
            }
            
            self.arrayOfTests.removeAll()
            self.listTests.removeAll()
            print("lab Test :\(response!)")
            let arrayOfLabs = response!["tests"].arrayValue
            if arrayOfLabs.count > 0{
                for test in arrayOfLabs{
                    let t = Tests(json: test)
                    self.arrayOfTests.append(t)
                    self.listTests.append(t.testName!)
                }
                self.dropListTests.anchorView = self.btnTest
                self.dropListTests.dataSource = self.listTests
                self.dropListTests.width = self.viewLab.frame.width
                self.dropListTests.bottomOffset = CGPoint(x: 0, y: 55)
                self.dropListTests.direction = .bottom
                self.dropListTests.selectionAction = { [unowned self] (index: Int, item: String) in
                    print("Selected item: \(item) at index: \(index)")
                    self.lblTest.text = "\(item)"
                   
                    _ = self.arrayOfTests.map({
                        if $0.testName == item{
                           // self.getLabListByLabortoryId(lab_id: $0.testId!)
                              self.lblPrice.text = $0.testPrice!
                              self.test_price = $0.testPrice!
                              self.test_id = $0.testId!
                        }
                    })
                }
            }
        })
        
    }
    func getLabListByLabortoryIdFirstTime(lab_id: String){
        
        Alert.showLoader(message: "")
        
        let param = ["lab_id": lab_id]
        
        SuperMedServices.LabTestByLab_ID(param: param, completionHandler:{(success, response, error) in
            Alert.hideLoader()
            
            if !success{
                print("response :\(String(describing: response))")
                return
            }
            
            self.arrayOfTests.removeAll()
            self.listTests.removeAll()
            print("lab Test :\(response!)")
            let arrayOfLabs = response!["tests"].arrayValue
            if arrayOfLabs.count > 0{
                for test in arrayOfLabs{
                    let t = Tests(json: test)
                    self.arrayOfTests.append(t)
                    self.listTests.append(t.testName!)
                }
                if self.listTests.count > 0{
                    self.lblTest.text = self.listTests.first ?? "-"
                    guard let test = response!["tests"].arrayValue.first else {return}
                    let Test = Tests(json: test)
                    self.lblPrice.text = Test.testPrice ?? "0"
                    self.test_price = Test.testPrice ?? "0"
                    self.test_id = Test.testId ?? "0"
                }
            }
        })
    }
    @IBAction func didTapOnCard(_ sender: Any) {
        
//        let alertView = CardAlertView(frame:(AppDelegate.getInstatnce().window?.bounds)!)
//        AppDelegate.getInstatnce().window?.addSubview(alertView)
        imageCash.image = UIImage(named: "icon_radioUn")
        imageCredit.image = UIImage(named: "icon_radioSelected")
        
        self.payType = "keenu"
    }
    
    @IBAction func didTapOnCash(_ sender: UIButton) {
        
        imageCash.image = UIImage(named: "icon_radioSelected")
        imageCredit.image = UIImage(named: "icon_radioUn")
        
        self.payType = "cash"
    }
    
    // MARK:- Book Appointment
    
    func bookAppointment(param : [String: Any]){
        
        Alert.showLoader(message: "Booking Lab Test")
        print(param)
        let jsonData = try! JSONSerialization.data(withJSONObject: param)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
              // print(jsonString)
        
        SuperMedServices.bookAppointment(param: param){(success, response, error) in
            Alert.hideLoader()
        
            if !success{
                return
            }else{
                print("Response: \(response!)")
                let msg = response!["message"].stringValue
                
                if self.payType == "keenu" {
                    
                    let order_id = response!["data"]["appointment_no"].stringValue
                  //  let price = response!["data"]["total_price"].intValue
                    
                    let vc = PaymentWebViewController.instantiateFromStoryboard()
                    vc.pathToFile = "https://www.supermed.pk/api/keenuPaymentProcess/\(order_id)/Appointment"
                    print("Web Url : \(vc.pathToFile)")
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } else {
                    
                    let recipet = response!["receipt"].stringValue
                    self.receipt = recipet
                    self.showDownloadBillPopupWith()
                    
                    let banner = NotificationBanner(title: "Success", subtitle: "\(msg)", style: .success)
                    banner.show()
                    
                }
                

                
//                let urlStr = receipt
//
//                // Converting string to URL Object
//                let url = URL(string: urlStr)
//
//                // Get the PDF Data form the Url in a Data Object
//                let pdfData = try? Data.init(contentsOf: url!)
//
//                // Get the Document Directory path of the Application
//                let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
//
//                // Split the url into a string Array by separator "/" to get the pdf name
//                let pdfNameFromUrlArr = urlStr.components(separatedBy: "/")
//
//                // Appending the Document Directory path with the pdf name
//                let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrlArr[
//                    pdfNameFromUrlArr.count - 1])
//                // Writing the PDF file data to the Document Directory Path
//                do {
//                    _ = try pdfData?.write(to: actualPath, options: .atomic)
//                       print("pdf downloaded")
//                }catch{
//
//                    print("Pdf can't be saved")
//                }

            }
            
        }
    }
}
//MARK:- Download bill pop-up
extension LabTestViewController{
    private func showDownloadBillPopupWith(){
        let downloadBill = DownloadBill(frame: (AppDelegate.getInstatnce().window?.bounds)!)
        downloadBill.btnDownloadBill.addTarget(self, action: #selector(self.onBtnDownloadBill), for: .touchUpInside)
        downloadBill.btnOpenBill.addTarget(self, action: #selector(self.onBtnOpenBill), for: .touchUpInside)
        downloadBill.btnClose.addTarget(self, action: #selector(self.onBtnClose), for: .touchUpInside)
        AppDelegate.getInstatnce().window?.addSubview(downloadBill)
    }
    @objc func onBtnClose(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func onBtnOpenBill(){
        self.openBillInBrowser()
    }
    @objc func onBtnDownloadBill(){
        self.startDownloading()
    }
    private func startDownloading(){
        self.navigationController?.popViewController(animated: true)
        let urlStr = self.receipt
        
        // Converting string to URL Object
        let url = URL(string: urlStr)
        
        // Get the PDF Data form the Url in a Data Object
        let pdfData = try? Data.init(contentsOf: url!)
        
        // Get the Document Directory path of the Application
        let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        
        // Split the url into a string Array by separator "/" to get the pdf name
        let pdfNameFromUrlArr = urlStr.components(separatedBy: "/")
        
        // Appending the Document Directory path with the pdf name
        let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrlArr[
            pdfNameFromUrlArr.count - 1])
        // Writing the PDF file data to the Document Directory Path
        do {
            _ = try pdfData?.write(to: actualPath, options: .atomic)
            print("pdf downloaded")
            Alert.showAlert(title: "Alert", message: "Bill downloaded.")
        }catch{
            
            print("Pdf can't be saved")
        }
    }
    private func openBillInBrowser(){
        let urlStr = self.receipt
        // Converting string to URL Object
        guard let url = URL(string: urlStr) else {return}
        if UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url)
        }
    }
}
