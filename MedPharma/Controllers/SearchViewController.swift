//
//  SearchViewController.swift
//  MedPharma
//
//  Created by Protege Global on 25/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import SwiftyJSON
import NotificationBannerSwift

class SearchViewController: UIViewController {
    class func instantiateFromStoryboard() -> SearchViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SearchViewController
    }
    
    @IBOutlet weak var collectionSearchView: UICollectionView!
    @IBOutlet weak var txtFieldSearch: UITextField!
    @IBOutlet weak var viewTop: UIView!
    var arrayProducts = [Products]()
    var from : String?
    
    let base_image = "https://www.supermed.pk/pos/public/uploads/products/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionSearchView.register(UINib(nibName: "ProductCategoryCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ProductCategoryCollectionCell") // as? ProductCategoryCollectionCell
        
        if from == "menu"{
            self.title = "Alternative Medicine"
            self.PlaceNavigationButtons(selectorForLeft: #selector(MenuAction), leftImage: MENU_IMAGE, selectorForRight:  #selector(didTapOnCart) , rightImage: CART_IMAGE)
        }else{
            self.title = "SEARCH FOR PRODUCT"
            self.PlaceNavigationButtons(selectorForLeft: #selector(didTapOnBack), leftImage: BACK_IMAGE, selectorForRight: #selector(didTapOnCart), rightImage: CART_IMAGE)
        }
        self.txtFieldSearch.addTarget(self, action: #selector(self.onSearchTextField(_:)), for: UIControlEvents.editingChanged)
        self.searchProductWith("")
    }
}

//MARK:- Helper Methods
extension SearchViewController{
    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    @objc func didTapOnCart(){
        let cart = CartViewController.instantiateFromStoryboard()
        cart.from = "Home"
        self.navigationController?.pushViewController(cart, animated: false)
    }
    @objc func didTapOnBack() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func onSearchTextField(_ sender: UITextField) {
        let searchText = sender.text ?? ""
        self.searchProductWith(searchText)
    }
    func searchProductWith(_ title:String){
        if from == "menu"{
            self.searchAlternativeProduct(name: title)
        }else{
            self.searchProductsByName(name: title)
        }
    }
}

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:  SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size:175), height: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 280))
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product_detail = ProductDetailViewController.instantiateFromStoryboard()
        product_detail.product = [self.arrayProducts[indexPath.row]]
        self.navigationController?.pushViewController(product_detail, animated: true)
    }
}

extension SearchViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayProducts.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionSearchView.dequeueReusableCell(withReuseIdentifier: "ProductCategoryCollectionCell", for: indexPath) as! ProductCategoryCollectionCell
        setCellData(index: indexPath, cell: cell)
        return cell
    }
    
    func setCellData(index: IndexPath, cell: ProductCategoryCollectionCell){
        let product = arrayProducts[index.row]
        
        if base_image == product.productImage {
            cell.imageProduct.setImageFromUrl(urlStr: "https://www.supermed.pk/pos/public/images/tab-missing.png")
        } else {
            cell.imageProduct.setImageFromUrl(urlStr: product.productImage ?? "https://www.supermed.pk/pos/public/images/tab-missing.png")
        }
        
       // cell.imageProduct.setImageFromUrl(urlStr: product.productImage ?? "www.google.com")
        cell.lblTitleProduct.text = product.productName ?? "-"
        cell.lblPrice.text = "Rs:" + "\(product.productPrice ?? "0")"
        cell.btnAddToCart.tag = index.row
        cell.btnAddToCart.addTarget(self, action: #selector(onBtnAddToCart(_:)), for: .touchUpInside)
        guard let productQty = Int(product.productQty ?? "0") else {return}
        if productQty == 0{
            cell.btnAddToCart.isUserInteractionEnabled = false
            cell.btnAddToCart.setTitle("OUT OF STOCK", for: .normal)  // *change
        }
        else{
            cell.btnAddToCart.isUserInteractionEnabled = true
            cell.btnAddToCart.setTitle("ADD TO CART", for: .normal)
        }
    }
    @objc func onBtnAddToCart(_ sender: UIButton){
        let product = arrayProducts[sender.tag]
        self.addCartItem(product: product)
    }
}

//MARK:- Services
extension SearchViewController{
    func searchAlternativeProduct(name: String){
        let user_id = UserManager.getUserObjectFromUserDefaults()!.id ?? "0"
        let param :[String:Any] = ["search": name, "user_id":user_id]
        Alert.showLoader(message: "Loading")
        SuperMedServices.alternativeProductSearch(param: param, completionHandler:{(success, response,error) in
            Alert.hideLoader()
            if !success {
                let banner = NotificationBanner(title: "Search", subtitle: "\(response!["message"].stringValue)", style: .danger)
                banner.show()
                return
            }
            self.arrayProducts.removeAll()
            let pro = response!["search"].arrayValue
            if pro.count > 0{
                for product in pro{
                    let prod = Products(json: product)
                    self.arrayProducts.append(prod)
                }
                self.collectionSearchView.reloadData()
            }
        })
    }
    
    func searchProductsByName(name: String){
        let user_id = UserManager.getUserObjectFromUserDefaults()!.id ?? "0"
        let param:[String:Any] = ["search":name, "user_id":user_id]
        Alert.showLoader(message: "Loading")
        SuperMedServices.searchProducts(param: param, completionHandler: { (success,response, error) in
            Alert.hideLoader()
            if !success {
                let banner = NotificationBanner(title: "Search", subtitle: "\(response!["message"].stringValue)", style: .danger)
                banner.show()
                return
            }
            self.arrayProducts.removeAll()
            let pro = response!["search"].arrayValue
            if pro.count > 0{
                for product in pro{
                    let prod = Products(json: product)
                    self.arrayProducts.append(prod)
                }
                self.collectionSearchView.reloadData()
            }
        })
    }
    func addCartItem(product: Products){
        let user_id = UserManager.getUserObjectFromUserDefaults()!.id ?? "0"
        let param : [String: Any] = ["userid":user_id, "productid": product.productID ?? "0", "qty": "1", "action": "0"]
        Alert.showLoader(message: "Loading")
        SuperMedServices.cart_action(param: param, completionHandler: {(success, response, error) in
            Alert.hideLoader()
            if !success {return}
            else{
                Alert.showAlert(title: "Alert", message: "Product Added to Cart Successfully")
                print("response: \(response!)")
            }
        })
    }
}

