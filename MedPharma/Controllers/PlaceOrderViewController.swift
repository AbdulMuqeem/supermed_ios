//
//  PlaceOrderViewController.swift
//  MedPharma
//
//  Created by Protege Global on 25/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import DropDown
import NotificationBannerSwift
import SwiftyJSON
import PDFKit

class PlaceOrderViewController: UIViewController {
    
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var btnCity: UIButton!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextView!
    @IBOutlet weak var txtAdd: UITextField!
    
    var countries = [Countries]()
    var arrayOfCountries = [String]()
    var cities = [Cities]()
    var arrayOfCities = [String]()
    var dictionary = [String: String]()
    
    var webView : UIWebView?
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var scrollForGuest: UIScrollView!
    @IBOutlet weak var viewForGuest: UIView!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtViewAddress: UITextView!
    @IBOutlet weak var imageCash: UIImageView!
    @IBOutlet weak var imageCredit: UIImageView!
    @IBOutlet weak var btnCash: UIButton!
    @IBOutlet weak var btnCredit: UIButton!
    @IBOutlet weak var scrollMainView: UIScrollView!
    @IBOutlet weak var lblCouponDiscount: UILabel!
    
    @IBOutlet weak var guest_imageCash: UIImageView!
    @IBOutlet weak var guest_imageCredit: UIImageView!
    
    var contact = ""
    var address = ""
    var dict = [String: Any]()
    
    @IBOutlet weak var viewPrices: UIView!
    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var viewCity: UIView!
    @IBOutlet weak var lblTotalPayble: UILabel!
    @IBOutlet weak var lblDelivery: UILabel!
    @IBOutlet weak var lblItem: UILabel!
    @IBOutlet weak var lblUItem: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var viewPakistan: UIView!
    @IBOutlet weak var viewEmail: UIView!
    
    // User PriceViews
    
    @IBOutlet weak var lblUPrice: UILabel!
    @IBOutlet weak var lblUDelivery: UILabel!
    @IBOutlet weak var lblUTotalPayble: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    
    @IBOutlet weak var lblPayWithKeenu: UILabel!
    
    var receipt = ""
    
    var payType = "cash"
    
    @IBAction func didTappedOnCountry(_ sender: Any) {
        let drop = DropDown()
        drop.anchorView = btnCountry
        drop.dataSource = arrayOfCountries
        drop.width = btnCountry.frame.size.width
        drop.bottomOffset = CGPoint(x: 0, y: 45)
        drop.direction = .bottom
        drop.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblCountry.text = "\(item)"
            
            let names = self.countries.map({
                if $0.name == item{
                    self.getCitiesFromCountries(id: $0.id!)
                    print($0.id)
                }
            })
        }
        drop.show()
        
        
    }
    @IBAction func didTappedOnCity(_ sender: Any) {
        let drop = DropDown()
        drop.anchorView = btnCity
//        let city = ["Karachi","Hyderabad","Lahor","Islamabad"]
        
        drop.dataSource = arrayOfCities
        drop.width = btnCountry.frame.size.width
        drop.bottomOffset = CGPoint(x: 0, y: 45)
        drop.direction = .bottom
        drop.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblCity.text = "\(item)"
            
            let names = self.countries.map({
                if $0.id == item{
                    print($0.name)
                }
            })
        }
        drop.show()
    }
    class func instantiateFromStoryboard() -> PlaceOrderViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! PlaceOrderViewController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblName.text = Singleton.sharedInstance.CurrentUser!.first_name
        self.lblPayWithKeenu.text = "Pay with keenu"
        
        setDataOnView()
        getCountriesFromServer()
        self.title = "Place An Order"
        self.PlaceLeftButton(image: #imageLiteral(resourceName: "icon_back"), selector: #selector(didTapOnBack))
        txtMobile.delegate = self
        txtViewAddress.delegate = self
        
        print("DICTIONARY:\(dict)")
        self.setDataOnView()
        
        let user = UserDefaults.standard
        let guest = user.bool(forKey: "AsGuest")
        print(user)
        
        if guest{
//            customiseView()
            txtAdd.addPadding(.left(8))
            txtEmail.addPadding(.left(8))
            txtMobile.addPadding(.left(8))
            txtLastName.addPadding(.left(8))
            txtFirstName.addPadding(.left(8))
            txtMobile.addPadding(.left(8))
            txtMobileNo.addPadding(.left(8))
            
             scrollForGuest.isHidden = false
             scrollMainView.isHidden = true
        
        }else{
             setDataOnUView()
             scrollMainView.isHidden = false
             scrollForGuest.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setDataOnView(){
    txtViewAddress.text = UserManager.getUserObjectFromUserDefaults()?.address!
    txtMobile.text = UserManager.getUserObjectFromUserDefaults()?.contact!
        self.lblCouponDiscount.text = "\(self.dict["coupon"] as? String ?? "0") %"
    
//        self.scrollForGuest.isHidden = false
//        self.scrollMainView.bringSubview(toFront: self.viewPrices)
        
//        self.lblItem.text = dict["item"] as? String
        //self.viewCard.isHidden = true
        self.lblTotalPayble.text = dict["total"] as? String
        self.totalPrice.text = dict["total"] as? String
 }
    
    func setDataOnUView(){
        self.lblUPrice.text = "Rs \(dict["total"] as? String ?? "0")"
        let disPercent = 1.0 - (Double("\(self.dict["coupon"] as? String ?? "0")") ?? 0.0) / 100.0
        let total = (Double("\(dict["total"] as? String ?? "0")") ?? 0.0) * disPercent
        self.lblUTotalPayble.text = "Rs \(total)"
    }
    
//    func countriesJSON(){
//
//        if let path = Bundle.main.path(forResource: "countries", ofType: "json") {
//            do {
//                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .dataReadingMapped)
//                let jsonObj = try JSON(data: data)
//
//                for country in jsonObj{
//                    let dict = country.1
//                    print(dict["name"])
//                    countries.append(dict["name"].stringValue)
//                }
//
//
//            } catch let error {
//                print("parse error: \(error.localizedDescription)")
//            }
//        } else {
//            print("Invalid filename/path.")
//        }
//
//    }
    
    
    
    func customiseView(){
        scrollForGuest.snp.makeConstraints{(make)-> Void in
            make.left.equalTo(self.view)
            make.top.equalTo(self.view)
            make.bottom.equalTo(self.view).offset(-60)
            make.right.equalTo(self.view)
        }
        
        scrollForGuest.addSubview(viewForGuest)
        viewForGuest.snp.makeConstraints{(make)-> Void in
            make.left.equalTo(scrollForGuest).offset(8)
            make.top.equalTo(scrollForGuest)
            make.bottom.equalTo(scrollForGuest)
            make.right.equalTo(scrollForGuest).offset(8)
            make.width.equalTo(scrollForGuest).offset(-16)
        }
        viewForGuest.addSubview(stackView)
       
        stackView.snp.makeConstraints{(make)-> Void in
            make.top.equalTo(viewForGuest)
            make.bottom.equalTo(viewForGuest)
            make.left.equalTo(viewForGuest)
            make.right.equalTo(viewForGuest)
        }
        
        stackView.addArrangedSubview(viewName)
        stackView.addArrangedSubview(viewEmail)
        stackView.addArrangedSubview(viewMobile)
        stackView.addArrangedSubview(viewPakistan)
        stackView.addArrangedSubview(viewCity)
        stackView.addArrangedSubview(viewAddress)
        stackView.addArrangedSubview(viewCard)
        stackView.addArrangedSubview(viewPrices)
        
        stackView.distribution = .fillProportionally
        
    }
    
    func validateTextFields(){
        
        guard let fname = self.txtFirstName.text, fname != "First Name", fname != "" else {
            let banner = NotificationBanner(title: "Name", subtitle: "Please enter first name", style: .danger)
            banner.show()
            return
        }
        
        guard let lname = self.txtLastName.text, lname != "Last Name", lname != "" else {
            let banner = NotificationBanner(title: "Name", subtitle: "Please enter last name", style: .danger)
            banner.show()
            return
        }
        
        
        guard let email = self.txtEmail.text, email != "", AppHelper.isValidEmail(testStr: email) else {
            let banner = NotificationBanner(title: "Email", subtitle: "Please enter valid email", style: .danger)
            banner.show()
            return
        }
        
        guard let phone_number = self.txtMobileNo.text, phone_number != "", AppHelper.isValidPhone(testStr: phone_number) else {
            let banner = NotificationBanner(title: "Phone Number", subtitle: "Please enter valid phone number", style: .danger)
            banner.show()
            return
        }
        
        
     
        guard let country = self.lblCountry.text, country != "country", country != "" else {
            let banner = NotificationBanner(title: "country", subtitle: "Please select country", style: .danger)
            banner.show()
            return
        }
        
        guard let city = self.lblCity.text, city != "city", city != "" else {
            let banner = NotificationBanner(title: "city", subtitle: "Please select city", style: .danger)
            banner.show()
            return
        }
        
        guard let address = self.txtAdd.text, address != "address", address != "" else {
            let banner = NotificationBanner(title: "Address", subtitle: "Please enter address", style: .danger)
            banner.show()
            return
        }
        
  
        
        
        //first_name,last_name,contact,dob,address,country,city,email,password
        
        let param = ["first_name": fname, "last_name": lname, "contact": phone_number, "address": address, "country": country, "city": city, "email": email]
        
       // signUpService(param: param as! Dictionary<String, String>)
        
      //  checkoutProduct()
        
        
    }
    
    @objc func didTapOnBack() {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func didTapOnCard(_ sender: Any) {
        
//        let alertView = CardAlertView(frame:(AppDelegate.getInstatnce().window?.bounds)!)
//        AppDelegate.getInstatnce().window?.addSubview(alertView)
        imageCash.image = UIImage(named: "icon_radioUn")
        imageCredit.image = UIImage(named: "icon_radioSelected")
        
        guest_imageCash.image = UIImage(named: "icon_radioUn")
        guest_imageCredit.image = UIImage(named: "icon_radioSelected")
        
        self.payType = "keenu"
    }
    
    @IBAction func didTapOnCash(_ sender: UIButton) {
        
        imageCash.image = UIImage(named: "icon_radioSelected")
        imageCredit.image = UIImage(named: "icon_radioUn")
        
        guest_imageCash.image = UIImage(named: "icon_radioSelected")
        guest_imageCredit.image = UIImage(named: "icon_radioUn")
        
        self.payType = "cash"

    }
    
    @IBAction func didTapOnCheckout(_ sender: Any) {
            checkoutProduct()
        }
}

extension PlaceOrderViewController: UITextViewDelegate, UITextFieldDelegate{
    
        func textViewDidChange(_ textView: UITextView) {
            
        }
        func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
            
            return true
        }
        func textViewDidEndEditing(_ textView: UITextView) {
            if txtViewAddress.text == "" {
            }else{
               saveAddress(address: txtViewAddress.text!)
            }
            
        }
    
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            return true
        }
        func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
            if txtMobile.text == "" {
                
            }else{
              saveContact(contact: txtMobile.text!)
            }
        }
        
    
    func setData(){
        
    }
    
    func checkoutProduct(){

        var param:[String:Any] = [:]
        
        let user = UserDefaults.standard
        let guest = user.bool(forKey: "AsGuest")
        print(user)
        
        if guest {

            guard let fname = self.txtFirstName.text, fname != "First Name", fname != "" else {
                let banner = NotificationBanner(title: "Name", subtitle: "Please enter first name", style: .danger)
                banner.show()
                return
            }
            
            guard let lname = self.txtLastName.text, lname != "Last Name", lname != "" else {
                let banner = NotificationBanner(title: "Name", subtitle: "Please enter last name", style: .danger)
                banner.show()
                return
            }
            
            
            guard let email = self.txtEmail.text, email != "", AppHelper.isValidEmail(testStr: email) else {
                let banner = NotificationBanner(title: "Email", subtitle: "Please enter valid email", style: .danger)
                banner.show()
                return
            }
            
            guard let phone_number = self.txtMobileNo.text, phone_number != "", AppHelper.isValidPhone(testStr: phone_number) else {
                let banner = NotificationBanner(title: "Phone Number", subtitle: "Please enter valid phone number", style: .danger)
                banner.show()
                return
            }
            
            guard let country = self.lblCountry.text, country != "country", country != "" else {
                let banner = NotificationBanner(title: "country", subtitle: "Please select country", style: .danger)
                banner.show()
                return
            }
            
            guard let city = self.lblCity.text, city != "city", city != "" else {
                let banner = NotificationBanner(title: "city", subtitle: "Please select city", style: .danger)
                banner.show()
                return
            }
            
            guard let address = self.txtAdd.text, address != "address", address != "" else {
                let banner = NotificationBanner(title: "Address", subtitle: "Please enter address", style: .danger)
                banner.show()
                return
            }
            
            self.address = self.txtAdd.text!
            if self.address.isEmptyOrWhitespace() {
                Alert.showAlert(title: "Alert", message: "kindly provide complete address")
            }
            
            self.contact = self.txtMobileNo.text!
            if self.address.isEmptyOrWhitespace() {
                Alert.showAlert(title: "Alert", message: "kindly provide Mobile Number")
                return
            }

            param = ["userid": UserManager.getUserObjectFromUserDefaults()!.id!,"contact": phone_number, "address": address , "ordernote": "", "paytype": self.payType , "coupanCode": "" ,"first_name" :fname , "last_name":lname , "country":country , "city":city , "email":email]
            
        }
        else {
            
            self.address = self.txtViewAddress.text!
            if self.address.isEmptyOrWhitespace() {
                Alert.showAlert(title: "Alert", message: "kindly provide complete address")
            }
            
            self.contact = self.txtMobile.text!
            if self.address.isEmptyOrWhitespace() {
                Alert.showAlert(title: "Alert", message: "kindly provide Mobile Number")
                return
            }
            
            let userId = UserManager.getUserObjectFromUserDefaults()!.id!
            let fname = UserManager.getUserObjectFromUserDefaults()!.first_name!
            let lname = UserManager.getUserObjectFromUserDefaults()!.last_name!
            let country = UserManager.getUserObjectFromUserDefaults()!.country!
            let city = UserManager.getUserObjectFromUserDefaults()!.city!
            let email = UserManager.getUserObjectFromUserDefaults()!.email!
            
            param = ["userid": userId ,"contact": self.contact , "address": self.address , "ordernote": "", "paytype": self.payType , "coupanCode": "" ,"first_name" :fname , "last_name":lname , "country":country , "city":city , "email":email]
            
        }
        
        Alert.showLoader(message: "CheckOut Product")
        print(param)
        
        SuperMedServices.checkout(param: param, completionHandler: {(success, response, error) in
           Alert.hideLoader()
            
            if !success{
                return
            }else{
                
                print("response:\(response!)")
                
                if self.payType == "keenu" {
                    
                    let order_id = response!["data"]["order_no"].stringValue
                    //let price = response!["data"]["order_total_price"].intValue
                    
                    let vc = PaymentWebViewController.instantiateFromStoryboard()
                    vc.pathToFile = "https://www.supermed.pk/api/keenuPaymentProcess/\(order_id)/Ordered"
                    print("Web Url : \(vc.pathToFile)")
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } else {
                    
                    let recipet = response!["receipt"].stringValue
                    self.receipt = recipet
                    self.showDownloadBillPopupWith()
                    
                }
                
//                // Url in String format
//
//                let url = URL(string: recipet)
//                Alert.showLoader(message: "Pdf Downloading")
//
//                let urlRequest = URLRequest(url: url!)
//
//                let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
//
//                    var documentsURL: URL? = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
//                    documentsURL = documentsURL?.appendingPathComponent("localFile.pdf")
//                    try! data?.write(to: documentsURL!, options: .atomic)
//                }
//
//                task.resume()
//                  Alert.hideLoader()
//
//
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
//                self.loadPDF(url: url!)
//            }

            }
        })
        
//        let user = UserDefaults.standard
//        let guest = user.bool(forKey: "AsGuest")
//        print(user)
//
//        if guest{
//            self.validateTextFields()
//            Alert.showToast(message: "Unable to CheckOut")
//        }
    
    }

    func loadPDF(url : URL){
        
         webView = UIWebView(frame:(AppDelegate.getInstatnce().window?.bounds)!)
        DispatchQueue.main.async {
            let re = NSURLRequest(url: url)
            self.webView?.loadRequest(re as URLRequest)
            AppDelegate.getInstatnce().window?.addSubview(self.webView!)
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate // This is not required
        webView?.addGestureRecognizer(tap)
    }
    
     @objc func handleTap(sender: UITapGestureRecognizer?) {
        webView?.removeFromSuperview()
    }
    
    func saveAddress(address: String) {
        self.address = address
    }
    
    func saveContact(contact: String) {
        self.contact = contact
    }
    
    func getCountriesFromServer(){
        SuperMedServices.GetCountries(param: [:]) { (success, response, error) in
            if !success{
                return
            }else{
                print(response)
                self.countries.removeAll()
                self.arrayOfCountries.removeAll()
                let array = response!["countries"].arrayValue
                for country in array{
                    let count = Countries(json: country)
                    self.countries.append(count)
                    self.arrayOfCountries.append(count.name!)
                }
            }
        }
    }
    
    func getCitiesFromCountries(id: String){
        let params = ["country_id": id]
        
        SuperMedServices.GetCities(param: params) { (success, response, error) in
            if !success{
                return
            }else{
                print(response)
                self.cities.removeAll()
                self.arrayOfCities.removeAll()
                let array = response!["cities"].arrayValue
                for city in array{
                    let ct = Cities(json: city)
                    self.cities.append(ct)
                    self.arrayOfCities.append(ct.name!)
                }
            }
        }
    }
}
//MARK:- Download bill pop-up
extension PlaceOrderViewController{
    private func showDownloadBillPopupWith(){
        let downloadBill = DownloadBill(frame: (AppDelegate.getInstatnce().window?.bounds)!)
        downloadBill.btnDownloadBill.addTarget(self, action: #selector(self.onBtnDownloadBill), for: .touchUpInside)
        downloadBill.btnOpenBill.addTarget(self, action: #selector(self.onBtnOpenBill), for: .touchUpInside)
        downloadBill.btnClose.addTarget(self, action: #selector(self.onBtnClose), for: .touchUpInside)
        AppDelegate.getInstatnce().window?.addSubview(downloadBill)
    }
    @objc func onBtnClose(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    @objc func onBtnDownloadBill(){
        self.startDownloading()
    }
    @objc func onBtnOpenBill(){
        self.openBillInBrowser()
    }
    private func startDownloading(){
        Alert.showLoader(message: "Pdf Downloading")
        self.navigationController?.popToRootViewController(animated: true)
        let urlStr = self.receipt
        
        // Converting string to URL Object
        let url = URL(string: urlStr)
        
        // Get the PDF Data form the Url in a Data Object
        let pdfData = try? Data.init(contentsOf: url!)
        
        // Get the Document Directory path of the Application
        let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        
        // Split the url into a string Array by separator "/" to get the pdf name
        let pdfNameFromUrlArr = urlStr.components(separatedBy: "/")
        
        // Appending the Document Directory path with the pdf name
        let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrlArr[
            pdfNameFromUrlArr.count - 1])
        // Writing the PDF file data to the Document Directory Path
        do {
            _ = try pdfData?.write(to: actualPath, options: .atomic)
            print("pdf downloaded")
            Alert.showAlert(title: "Alert", message: "Bill downloaded.")
            Alert.hideLoader()
        }catch{
            Alert.hideLoader()
            print("Pdf can't be saved")
        }
    }
    private func openBillInBrowser(){
        let urlStr = self.receipt
        // Converting string to URL Object
        guard let url = URL(string: urlStr) else {return}
        if UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url)
        }
    }
}

