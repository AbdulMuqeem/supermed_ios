//
//  LinkedInVC.swift
//  MedPharma
//
//  Created by mac  on 10/16/18.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class LinkedInVC: UIViewController {
    
    class func instantiateFromStoryboard() -> LinkedInVC {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LinkedInVC
    }
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Linked In"
        
        self.PlaceLeftButton(image: #imageLiteral(resourceName: "icon_back"), selector: #selector(didTapOnBack))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        
        linkInURL()
        
    }
    
    @objc func didTapOnBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func linkInURL(){
        // webView
        Alert.showLoader(message: "Loading")
        webView.loadRequest(URLRequest(url: NSURL(string: "https://www.linkedin.com")! as URL))
        Alert.hideLoader()
    }
    
    

}
