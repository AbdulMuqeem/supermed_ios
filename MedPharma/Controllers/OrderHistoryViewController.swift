//
//  OrderHistoryViewController.swift
//  MedPharma
//
//  Created by Protege Global on 12/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class OrderHistoryViewController: UIViewController {

    @IBOutlet weak var tableOrderHistory: UITableView!
    
    var arrayOfOrder = [Orders]()
    
    class func instantiateFromStoryboard() -> OrderHistoryViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! OrderHistoryViewController
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.PlaceNavigationButtons(selectorForLeft: #selector(MenuAction), leftImage: MENU_IMAGE, selectorForRight:  #selector(NotificationAction) , rightImage: NOTIFICATION_IMAGE)
        self.PlaceLeftButton(image: MENU_IMAGE, selector: #selector(MenuAction))
        
        self.title = "Order History"
        
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        // Do any additional setup after loading the view.
        
        let nib = UINib(nibName: "AppointmentSectionTableCell", bundle: nil)
        tableOrderHistory.register(nib, forCellReuseIdentifier: "AppointmentSectionTableCell")
        
        let nib2 = UINib(nibName: "OrderHistoryTableViewCell", bundle: nil)
        tableOrderHistory.register(nib2, forCellReuseIdentifier: "OrderHistoryTableViewCell")
        tableOrderHistory.delegate = self
        tableOrderHistory.dataSource = self
        self.showOrderHistory()
        
    }
    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @objc func NotificationAction() {
        
        //        let vc = AcceptfriendViewController.instantiateFromStoryboard()
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  

}
extension OrderHistoryViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfOrder.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayOfOrder.count
    }
    
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//
//        let view:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.size.width, height: 10))
//        view.backgroundColor = .clear
//        return view
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 44)
//    }
//
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let cell = tableOrderHistory.dequeueReusableCell(withIdentifier: "AppointmentSectionTableCell") as? AppointmentSectionTableCell
//        cell?.lblAppointmentNo.text = "Order ID: " + arrayOfOrder[section].orderNo!
//        return cell
//    }
//
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellDetail = tableOrderHistory.dequeueReusableCell(withIdentifier: "OrderHistoryTableViewCell") as! OrderHistoryTableViewCell
        cellDetail.lblOrderNo.text = "Order No: " + arrayOfOrder[indexPath.row].orderNo!
        cellDetail.lblTotalPrice.text = arrayOfOrder[indexPath.row].orderTotalPrice
        cellDetail.lblOrderStatus.text = "Order Status: \(arrayOfOrder[indexPath.row].orderStatus ?? "")"
        if (arrayOfOrder[indexPath.row].orderNote ?? "").isEmpty{
            cellDetail.lblOrderNo.text = "Order Note: No Note"
        }
        else{
            cellDetail.lblOrderNo.text = "Order Note: \(arrayOfOrder[indexPath.row].orderNote ?? "")"
        }
        cellDetail.selectionStyle = .none
        return cellDetail
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension//SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 70)
    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 10.0
//    }
    
    
    
    func showOrderHistory(){
        Alert.showLoader(message: "Loading")
        let param : [String: Any] = ["userid": UserManager.getUserObjectFromUserDefaults()!.id!]
        print(param)
        SuperMedServices.OrderHistory(param: param) { (success, response, error) in
            Alert.hideLoader()
            if !success{
                return
            }
            print(response)
            let orderHistory = response!["orders"].arrayValue
            for item in orderHistory{
                let or = Orders(json: item)
                self.arrayOfOrder.append(or)
            }
            self.tableOrderHistory.reloadData()
            
    }
}
    
}
