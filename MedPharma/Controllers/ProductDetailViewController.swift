//
//  ProductDetailViewController.swift
//  MedPharma
//
//  Created by Protege Global on 21/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import SnapKit

class ProductDetailViewController: UIViewController {
    
    @IBOutlet weak var viewWishlist: UIView!
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var imageMinus: UIImageView!
    @IBOutlet weak var imageAdd: UIImageView!
    @IBOutlet weak var lblQuantiy: UILabel!
    @IBOutlet weak var viewAdder: UIView!
    @IBOutlet weak var btnWishlist: UIButton!
    @IBOutlet weak var lblWishlist: UILabel!
    @IBOutlet weak var imageWishlist: UIImageView!
    @IBOutlet weak var viewSeprator: UIView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var lblCounter: UILabel!
    @IBOutlet weak var lblProductTitle: UILabel!
    @IBOutlet weak var lblPriceDiscount: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblAvailableQuantiy: UILabel!
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var lblAddToCart: UILabel!
    
    var products = [Products]()
    var product = [Products]()
    var images = [String]()
    var isInWishList = false
    let imagesCount = 3
    
    class func instantiateFromStoryboard() -> ProductDetailViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ProductDetailViewController
    }
    @IBOutlet weak var pageControl: FSPageControl!{
        didSet {
            self.pageControl.numberOfPages = imagesCount
            self.pageControl.contentHorizontalAlignment = .center
            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            self.pageControl.backgroundColor = UIColor.clear
        }
    }
    @IBOutlet weak var pagerView: FSPagerView!{      didSet {
        self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.pagerView.itemSize = .zero
        }
    }
    
    @IBAction func didTappedOnWishlist(_ sender: Any) {
        print("weldone")
        
//        let userDefault = UserDefaults.standard
//        userDefault.set(true, forKey: "wishlist")
//        imageWishlist.image = UIImage(named: "icon_wishlistGreen")
        
        if self.isInWishList{
            //delete from whishlist
            self.deleteFromWhishlist()
            
        }
        else{
            //add in whishlist
            addToWishlistByService()
        }
    }
    
    @IBOutlet weak var view_counter: UIView!
    let base_image = "https://www.supermed.pk/pos/public/uploads/products/"

    override func viewDidLoad() {
        super.viewDidLoad()

        customizeView()
        
        self.title = "Product Descripton"
       // self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(didTapOnBack))
        self.PlaceNavigationButtons(selectorForLeft: #selector(didTapOnBack), leftImage: BACK_IMAGE, selectorForRight: #selector(didTapOnCart), rightImage: CART_IMAGE)
        pagerView.delegate = self
        pagerView.dataSource = self
      
        setProductDataOnScreen()
        print(product[0])
        for _ in 0..<imagesCount{
            
            images.append(product[0].productImage!)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func setProductDataOnScreen(){
         lblProductTitle.text = product[0].productName
         lblPriceDiscount.text = ""//"Rs\(product[0].productOldPrice ?? "0")"
         lblPrice.text = "Rs\(product[0].productPrice!)"
        lblAvailableQuantiy.text = "Quantity Left: " + "\(product[0].productQty ?? "0")"
        if product[0].is_wish! == "1"{
            imageWishlist.image = UIImage(named: "icon_wishlistGreen")
            self.isInWishList = true
        }else{
            imageWishlist.image = UIImage(named: "icon_wishlist")
            self.isInWishList = false
        }
        guard let productQty = Int("\(product[0].productQty ?? "0")") else {return}
        if productQty == 0{
            self.btnAddToCart.isUserInteractionEnabled = false
            self.lblAddToCart.text = "OUT OF STOCK"
        }
        else{
            self.btnAddToCart.isUserInteractionEnabled = true
            self.lblAddToCart.text = "ADD TO CART"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didTapOnCart(){
        let cart = CartViewController.instantiateFromStoryboard()
        cart.from = "Home"
        self.navigationController?.pushViewController(cart, animated: false)
//      print("Cart")
    
    }
    
    @IBAction func didTappedOnAddToCart(_ sender: Any) {
        print("TappedOnAddToCart")
        addCartItem()
    }
    
    func addCartItem(){
        let param : [String: Any] = ["userid": UserManager.getUserObjectFromUserDefaults()!.id!, "productid": product[0].productID!, "qty": lblCounter.text!, "action": "0"]
        Alert.showLoader(message: "Loading")
        SuperMedServices.cart_action(param: param, completionHandler: {(success, response, error) in
        Alert.hideLoader()
            
            if !success {
                //print(response)
                return
            }else{
                
                print("response: \(response!)")
                self.goToCheckoutController()
            }
        })
    }
    
    func goToCheckoutController(){
        getCartProducts()
    }
    
    
    func getCartProducts(){
        let param = ["userid": UserManager.getUserObjectFromUserDefaults()!.id!]
        SuperMedServices.getCartProducts(param: param) { (success, response, error) in
            if !success{
                //print("all Products:\(response)")
                return
            }
            self.products.removeAll()
            let productsArray = response!["cart"].arrayValue
            if productsArray.count > 0 {
                for prod  in  productsArray {
                    let pro = Products(json: prod)
                    self.products.append(pro)
                }
              
                let controller = CartViewController.instantiateFromStoryboard()
                    controller.product = self.products
                self.navigationController?.pushViewController(controller, animated: true)
                
            }
            print(response!)
        }
    }
    
    
    @IBAction func didTapOnAdd(_ sender: Any) {
        
        var myInt2 = (lblCounter.text as! NSString).integerValue
        if var i  =  myInt2 as? Int{
            
            i = i + 1
            lblCounter.text = "\(i)"
        }
    }
    
    @IBAction func didTapOnMinus(_ sender: UIButton) {
        let myInt2 = (lblCounter.text as! NSString).integerValue
        if var i  =  myInt2 as? Int{
            if i <= 0{
                return
            }
            
            i = i - 1
            lblCounter.text = "\(i)"
        }
    }
    
    func addToWishlistByService(){
        
        let param = ["userid": UserManager.getUserObjectFromUserDefaults()!.id!, "productid":product[0].productID!]
        
        print(param)
        Alert.showLoader(message: "Adding")
        SuperMedServices.addWishlist(param: param, completionHandler:{(success, response, error) in
            Alert.hideLoader()
            if !success {return}
            //print("response:\(response!)")
            let wishlistCounter = response!["wishlistCounter"]
            
            let userDefault = UserDefaults.standard
                userDefault.set("\(wishlistCounter)", forKey: "wishCounter")
            
            userDefault.set(true, forKey: "wishlist")
            self.imageWishlist.image = UIImage(named: "icon_wishlistGreen")
            self.isInWishList = true
        })
    }
    
    func deleteFromWhishlist(){
        let param = ["userid": UserManager.getUserObjectFromUserDefaults()!.id!, "productid":product[0].productID!]
        SuperMedServices.deleteWishlistItem(param: param) { (success, response, error) in
           if !success {return}
               print("response:\(response!)")
                let userDefault = UserDefaults.standard
                userDefault.set(false, forKey: "wishlist")
                self.imageWishlist.image = UIImage(named: "icon_wishlist")
                self.isInWishList = false
        }
    }
    
    @objc func didTapOnBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func customizeView(){
        
        
        scroll.addSubview(pagerView)
        scroll.translatesAutoresizingMaskIntoConstraints = false
        scroll.showsHorizontalScrollIndicator = false
        scroll.alwaysBounceVertical = false
        scroll.alwaysBounceHorizontal = false
        
        
        scroll.snp.makeConstraints{(make)-> Void in
            
            make.top.equalTo(self.view)
            make.bottom.equalTo(self.view).offset(-60)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
            
            
        }
        
        
        
        
       
        
        // PagerView
        pagerView.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(scroll.snp.width)
            make.height.equalTo(185)
            make.top.equalTo(self.scroll.snp.top)
            make.centerX.equalTo(scroll.snp.centerX)
        }
        // PageController
        
        
        
        scroll.addSubview(pageControl)
      

        
        

        pageControl.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(120)
            make.height.equalTo(24)
            make.bottom.equalTo(pagerView).offset(-16)
            make.centerX.equalTo(pagerView)
           
        }
        
        scroll.addSubview(viewWishlist)
        
        viewWishlist.snp.makeConstraints{(make)-> Void in
            make.left.equalTo(pagerView)
            make.right.equalTo(pagerView)
            make.top.equalTo(pagerView.snp.bottom).offset(16)
            make.height.equalTo(100)
            
            
        }
        
        
        viewWishlist.addSubview(lblProductTitle)
        
        
        lblProductTitle.snp.makeConstraints{(make) -> Void in
            make.left.equalTo(viewWishlist.snp.left).offset(16)
            make.top.equalTo(viewWishlist.snp.top).offset(0)
            make.right.equalTo(btnWishlist.snp.left)
        }
        
        viewWishlist.addSubview(imageWishlist)
        
        imageWishlist.snp.makeConstraints{(make) -> Void in
            make.right.equalTo(viewWishlist.snp.right).offset(-18)
            make.top.equalTo(viewWishlist.snp.top).offset(18)
            
        }
        
        viewWishlist.addSubview(lblWishlist)
        
        lblWishlist.snp.makeConstraints{(make) -> Void in
            make.centerX.equalTo(imageWishlist.snp.centerX)
            make.top.equalTo(imageWishlist.snp.bottom).offset(9)
            
        }
        
        viewWishlist.addSubview(btnWishlist)
        
        btnWishlist.snp.makeConstraints{(make)-> Void in
           make.width.equalTo(48)
           make.height.equalTo(48)
           make.top.equalTo(viewWishlist.snp.top).offset(9)
           make.right.equalTo(viewWishlist.snp.right).offset(-9)
            
            
            
        }
        
        
        viewWishlist.addSubview(viewSeprator)
        viewSeprator.backgroundColor = UIColor.lightGray
        
        
        viewSeprator.snp.makeConstraints{(make)-> Void in
            make.height.equalTo(1)
            make.width.equalTo(viewWishlist)
            make.bottom.equalTo(viewWishlist.snp.bottom).offset(1)
            
        }
        
        viewWishlist.addSubview(lblPrice)
       // viewWishlist.backgroundColor = UIColor.gray
        
        lblPrice.snp.makeConstraints{(make)-> Void in
            make.top.equalTo(lblProductTitle.snp.bottom).offset(0)
            make.left.equalTo(lblProductTitle.snp.left)
            make.bottom.equalTo(viewWishlist.snp.bottom).offset(-30)
        }
        viewWishlist.addSubview(lblAvailableQuantiy)
        lblAvailableQuantiy.snp.makeConstraints{(make)-> Void in
            make.top.equalTo(lblPrice.snp.bottom).offset(0)
            make.left.equalTo(lblPrice.snp.left)
            make.bottom.equalTo(viewWishlist.snp.bottom).offset(-10)
        }
        
        viewWishlist.addSubview(lblPriceDiscount)
        lblPriceDiscount.snp.makeConstraints{(make)-> Void in
            make.left.equalTo(lblPrice.snp.right).offset(8)
            make.top.equalTo(lblPrice.snp.top)
            make.bottom.equalTo(lblPrice.snp.bottom)
     }
        
//        viewWishlist.addSubview(lblCutPrice)
//        lblCutPrice.snp.makeConstraints { (make) in
//            make.left.equalTo(lblPrice.snp.right).offset(8)
//            make.top.equalTo(lblPrice.snp.top)
//            make.bottom.equalTo(lblPrice.snp.bottom)
//        }
        
        
        
        scroll.addSubview(viewAdder)

        let lblDescr = UILabel()
        lblDescr.translatesAutoresizingMaskIntoConstraints = false
        scroll.addSubview(lblDescr)
        lblDescr.numberOfLines = 0
        
        lblDescr.snp.makeConstraints{(make) -> Void in
          make.top.equalTo(viewWishlist.snp.bottom).offset(16)
          make.left.equalTo(lblProductTitle)
          make.right.equalTo(pagerView).offset(-16)
          make.bottom.equalTo(viewAdder.snp.top).offset(-8)
         
           
        }
        
        
        viewAdder.backgroundColor = UIColor.white
        
        viewAdder.snp.makeConstraints{(make)-> Void in
                make.width.equalToSuperview()
                make.height.equalTo(64)
                make.bottom.equalTo(scroll)
        }
        
       
        let viewSeprator2 = UIView()
        
        viewAdder.addSubview(viewSeprator2)
        viewSeprator2.backgroundColor = UIColor.lightGray
        
        
        
        view_counter.addSubview(lblCounter)
        view_counter.addSubview(btnAdd)
        view_counter.addSubview(btnMinus)
        view_counter.addSubview(imageAdd)
        view_counter.addSubview(imageMinus)
        viewAdder.addSubview(lblQuantiy)
        viewAdder.addSubview(view_counter)
        viewAdder.translatesAutoresizingMaskIntoConstraints = false
        lblQuantiy.translatesAutoresizingMaskIntoConstraints = false
        
        lblQuantiy.snp.makeConstraints{(make)-> Void in
            make.left.equalTo(viewAdder).offset(16)
            make.centerY.equalToSuperview()
        }
        
        viewSeprator2.snp.makeConstraints{(make)-> Void in
            make.height.equalTo(1)
            make.width.equalTo(viewAdder)
            make.top.equalTo(8)
            make.centerX.equalTo(viewAdder)
            
            
        }
        
        
        view_counter.backgroundColor = UIColor.clear
        view_counter.snp.makeConstraints{(make)-> Void in
            
            make.right.equalTo(8)
            make.width.equalTo(200)
            make.centerY.equalToSuperview()
            make.height.equalTo(48)
            
        }
        
        imageMinus.snp.makeConstraints{(make) -> Void in
            make.right.equalTo(-32)
            make.centerY.equalToSuperview()
            
            
        }
        
        lblCounter.snp.makeConstraints{(make)-> Void in
            make.right.equalTo(imageMinus.snp.left).offset(-16)
            make.centerY.equalToSuperview()
            
            
        }
        
        imageAdd.snp.makeConstraints{(make)-> Void in
            make.right.equalTo(lblCounter.snp.left).offset(-16)
            make.centerY.equalToSuperview()
            
        }
        
        btnAdd.backgroundColor = UIColor.clear
        btnMinus.backgroundColor = UIColor.clear
        
        btnAdd.snp.makeConstraints{(make)-> Void in
            make.width.equalTo(34)
            make.height.equalTo(34)
            make.centerY.equalToSuperview()
            make.left.equalTo(imageMinus)
        }
        
        btnMinus.snp.makeConstraints{(make)-> Void in
            make.width.equalTo(34)
            make.height.equalTo(34)
            make.centerY.equalToSuperview()
            make.right.equalTo(imageAdd)
        }
        
        lblDescr.text = product[0].productDescription
        
    }
    
}

extension ProductDetailViewController: FSPagerViewDelegate, FSPagerViewDataSource{
    
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.images.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        if base_image == self.images[index] {
            cell.imageView?.setImageFromUrl(urlStr: "https://www.supermed.pk/pos/public/images/tab-missing.png")
        } else {
            cell.imageView?.setImageFromUrl(urlStr: self.images[index])
        }
       // cell.imageView?.setImageFromUrl(urlStr: self.images[index])
        cell.imageView?.contentMode = .scaleAspectFit
        cell.imageView?.clipsToBounds = true
        cell.textLabel?.isHidden = true
        //cell.textLabel?.text = index.description+index.description
        return cell
    }
    
    // MARK:- FSPagerView Delegate
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        self.pageControl.currentPage = index
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        guard self.pageControl.currentPage != pagerView.currentIndex else {
            return
        }
        self.pageControl.currentPage = pagerView.currentIndex // Or Use KVO with property "currentIndex"
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
//        switch sender.tag {
//        case 1:
//            let newScale = 0.5+CGFloat(sender.value)*0.5 // [0.5 - 1.0]
//            self.pagerView.itemSize = self.pagerView.frame.size.applying(CGAffineTransform(scaleX: newScale, y: newScale))
//        case 2:
//            self.pagerView.interitemSpacing = CGFloat(sender.value) * 20 // [0 - 20]
//        case 3:
//            self.numberOfItems = Int(roundf(sender.value))
//            self.pageControl.numberOfPages = self.images.count
//            self.pagerView.reloadData()
//        default:
//            break
//        }
    }
}

