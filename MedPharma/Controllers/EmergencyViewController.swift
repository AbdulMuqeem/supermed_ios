//
//  EmergencyViewController.swift
//  MedPharma
//
//  Created by Protege Global on 27/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import SwiftyJSON

class EmergencyViewController: UIViewController {

    @IBOutlet weak var TableEmergency: UITableView!
    
    var arrayEmergency = [Emergency]()

    class func instantiateFromStoryboard() -> EmergencyViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! EmergencyViewController
    }
    
//    let array = ["Ambulance Helpline","Police Helpline","Fire Bridge Helpline","Hospital Helpline"]
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        self.title = "Emergency Contact"
        self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(didTapOnBack))
        let nib = UINib(nibName: "EmergencyTableViewCell", bundle: nil)
        TableEmergency.register(nib, forCellReuseIdentifier: "EmergencyTableViewCell")
        TableEmergency.delegate = self
        TableEmergency.dataSource = self
        getEmergencyNumbers()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didTapOnBack() {
        self.navigationController?.popViewController(animated: true)
    }
   
    
    func getEmergencyNumbers(){
        
        SuperMedServices.GetEmergency(param: ["":""], completionHandler: {(success, response, error) in
          
            if !success{
                return
            }
            
            print("response\(response!)")
            let emergency = response!["emergency"].arrayValue
            if emergency.count > 0{
                for item in emergency{
                    let e = Emergency(json: item)
                     self.arrayEmergency.append(e)
                }
              self.TableEmergency.reloadData()
            }
            
        })
        
    }
    

}
extension EmergencyViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayEmergency.count
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = TableEmergency.dequeueReusableCell(withIdentifier: "EmergencyTableViewCell") as! EmergencyTableViewCell
        let name = arrayEmergency[indexPath.row].name ?? ""
        let cell_number = arrayEmergency[indexPath.row].contact ?? ""
        cell.lblTitle.text = name + "-" + cell_number
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 80)
     }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
  
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 16
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell_number = arrayEmergency[indexPath.row].contact ?? ""
        
        DispatchQueue.main.async {
            if let url = URL(string: "tel://\(cell_number)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        
//        if let url = URL(string: "tel:/\(cell_number)") {
//            if #available(iOS 10, *) {
//                UIApplication.shared.open(url, options: [:],
//                                          completionHandler: {
//                                            (success) in
//                                            print("Open \(url): \(success)")
//                                            if !success{
//                                                Alert.showAlert(title: "Error", message: "Calling service is unavailable")
//                                            }
//                })
//            } else {
//                let success = UIApplication.shared.openURL(url)
//                print("Open \(url): \(success)")
//            }
//        }
    }
}
