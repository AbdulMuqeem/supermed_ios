//
//  ContactUsViewController.swift
//  MedPharma
//
//  Created by Protege Global on 12/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import SwiftyJSON

class ContactUsViewController: UIViewController {

    class func instantiateFromStoryboard() -> ContactUsViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ContactUsViewController
    }
    
    @IBOutlet weak var tvContactUs: UITextView!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblSecondaryAddress: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblWhatsApp: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Contact Us"
        self.PlaceLeftButton(image: MENU_IMAGE, selector: #selector(MenuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.contactUs()
        
    }
    
    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    
    func contactUs(){
        
        Alert.showLoader(message:"Loading")
        
        SuperMedServices.contactUS(param: ["":""]){(success,response, error) in
        Alert.hideLoader()
            
            if !success {
                return
            }
            else{
                
                let resp = response!["contactUs"]
                let contactUS = ContactUs(json: resp)
                self.lblDetail.text = (contactUS.pageContent ?? "").htmlToString
                
                let dataResp = response!["data"]
                let contactUSData = ContactUsData(json: dataResp)
                
                if let address = contactUSData.addressOne {
                    self.lblAddress.text = address
                }
                
                if let secondaryAddress = contactUSData.addressTwo {
                    self.lblSecondaryAddress.text = secondaryAddress
                }
                
                if let mobile = contactUSData.mobile {
                    self.lblMobile.text = mobile
                }
                
                if let whatsApp = contactUSData.whatsapp {
                    self.lblWhatsApp.text = whatsApp
                }
                
                if let email = contactUSData.email {
                    self.lblEmail.text = email
                }
                
            }
        }
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
