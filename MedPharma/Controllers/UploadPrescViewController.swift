//
//  UploadPrescViewController.swift
//  MedPharma
//
//  Created by Protege Global on 12/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import NotificationBannerSwift
class UploadPrescViewController: UIViewController {

    
    let imagePicker = UIImagePickerController()
    @IBOutlet weak var imagePlaceHolder: UIImageView!
    class func instantiateFromStoryboard() -> UploadPrescViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! UploadPrescViewController
    }
    
    var selectedIamge : UIImage?
    
    @IBAction func didTapOnSubmit(_ sender: Any) {
        guard let image = selectedIamge, image != nil else {
            
            let banner = NotificationBanner(title: "Image ", subtitle: "Please Select prescription Image", style: .danger)
            banner.show()
            return
        }
        
        
        uploadPrescription(image: selectedIamge!)
        
    }
    
    @IBAction func didTapOnTakeSnap(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true)
        }
        else{
            let banner = NotificationBanner(title: "Camera", subtitle: "Camera is not available.", style: .danger)
            banner.show()
            return
        }
    }
    
    @IBAction func didTapOnBrowseImage(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    
    }
    
    @IBAction func didTapOnWhatsApp(_ sender: UIButton) {
        self.shareOnwhatsapp()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Upload Prescription"
        self.PlaceNavigationButtons(selectorForLeft: #selector(MenuAction), leftImage: #imageLiteral(resourceName: "icon_back"), selectorForRight: nil,rightImage: NOTIFICATION_IMAGE)
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        imagePicker.delegate = self
        
    }

    @objc func MenuAction() {
        let vc = HomeViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func NotificationAction() {
        
        //        let vc = AcceptfriendViewController.instantiateFromStoryboard()
        //        self.navigationController?.pushViewController(vc, animated: true)
    }


}
// MARK: - UIImagePickerControllerDelegate Methods

extension UploadPrescViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imagePlaceHolder.contentMode = .scaleAspectFit
            imagePlaceHolder.image = pickedImage
            self.selectedIamge = pickedImage
        }
        
       
        
        if let editedImage = info["UIImagePickerControllerEditedImage"]   as? UIImage {
             self.selectedIamge = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
             self.selectedIamge = originalImage
            
            
        }
        
        
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        
        
        dismiss(animated: true, completion: nil)
    }
    
    
    
    func shareOnwhatsapp(){
 
        let urlWhats = "whatsapp://send?phone=+923102911911" //&text=Message to share"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    UIApplication.shared.openURL(whatsappURL)
                } else {
                    let simAlert = UIAlertController(title: "No Response", message: "Insert Sim first", preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alert) in
                        self.dismiss(animated: true, completion: nil)
                    }
                    simAlert.addAction(okAction)
                    self.present(simAlert, animated: true, completion: nil)
                }
            }
        }
    
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension UploadPrescViewController{
    
    func uploadPrescription(image: UIImage){
        let param = ["image": "image", "userid": UserManager.getUserObjectFromUserDefaults()!.id!] as [String: Any]
        
        print(param)
        
        Alert.showLoader(message: "uploading prescription")
        
        SuperMedServices.uploadPrescription(param: param, image: image, completionHandler:{(success, response, error) in
            if !success{
                return
            }else{
                Alert.showAlert(title: "Alert", message: "Prescription Uploaded!")
                print(response!)
                
            }
        })
        
        
        
        
        
        
    }
    
    
}
