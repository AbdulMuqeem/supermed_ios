//
//  RegisterViewController.swift
//  MedPharma
//
//  Created by Protege Global on 21/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON
import ActionSheetPicker_3_0
import AAPickerView
import NotificationBannerSwift
import SwiftyJSON

class RegisterViewController: UIViewController , UIScrollViewDelegate, UITextFieldDelegate{
    
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var btnCity: UIButton!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextView!
    @IBOutlet weak var txtConfirmPassword: UITextFieldPadding!
    @IBOutlet weak var txtPassword: UITextFieldPadding!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var btnMale: KGRadioButton!
    @IBOutlet weak var btnFemale: KGRadioButton!
    @IBOutlet weak var lblSelGender: UILabel!
    
    
    var countries = [Countries]() // Array of Countries
    var countryArray = [String]()
    var cities = [Cities]()       // Array of cities
    var cityArray = [String]()
    
//    let gender = ["Male", "Female"]
    
    var isSocialLogin: Bool = false
    var data: [String: Any] = [String: Any]()
    
    class func instantiateFromStoryboard() -> RegisterViewController {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! RegisterViewController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.lblCountry.text = lblCountry.text
        scrollView.delegate = self
        self.getCountriesFromServer()
        self.title = "Registration"
        
        lblCountry.text = "Pakistan"
        lblCity.text = "Karachi"
        
        txtMobileNo.delegate = self
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(didTapOnBack))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
      //  scrollView.contentSize = CGSize(width: 300, height: 810)
        
        if self.isSocialLogin{
            Alert.showAlert(title: "Alert", message: "Please provide complete information to continue sign up.")
            self.txtFirstName.text = self.data["name"] as? String
            self.txtLastName.text = self.data["lastName"] as? String
            self.txtEmail.text = self.data["email"] as? String
        }
    }
    
    @objc func didTapOnBack() {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let s = NSString(string: textField.text ?? "").replacingCharacters(in: range, with: string)
        guard !s.isEmpty else { return true }
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .none
        return numberFormatter.number(from: s)?.intValue != nil
    }
    
    
//    @IBAction func didTapOnGender(_ sender: Any) {
//        let dropDown = DropDown()
//        dropDown.anchorView = btnGender
//        dropDown.dataSource = gender
//        dropDown.width = btnGender.frame.size.width
//        dropDown.bottomOffset = CGPoint(x: 0, y: 45)
//        dropDown.direction = .bottom
//        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
//            print("Selected item: \(item) at index: \(index)")
//            self.lblGender.text = item
//        }
//        
//    }
    
    @IBAction func didTappedOnCalendar(_ sender: UIButton) {
        
        print("weldone")
        ActionSheetDatePicker.show(withTitle: "Select DOB", datePickerMode: .date, selectedDate: Date(), minimumDate: nil, maximumDate: Date(), doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.lblDOB.text = newDate
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        
    }
    
    override func viewDidLayoutSubviews() {
        
        
        
        scrollView.translatesAutoresizingMaskIntoConstraints = true
        scrollView.contentSize = CGSize(width: 305, height: 810)
         print(scrollView.frame.size.height)
    }
    
    @IBAction func didTappedOnCity(_ sender: Any) {
        let drop = DropDown()
        drop.anchorView = btnCity
        
        drop.dataSource = cityArray
        drop.width = btnCountry.frame.size.width
        drop.bottomOffset = CGPoint(x: 0, y: 45)
        drop.direction = .bottom
        drop.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblCity.text = "\(item)"
            
            let names = self.countries.map({
                if $0.id == item{
                    print($0.name)
                }
            })
        }
        drop.show()
        
    }
    
    @IBAction func didTappedOnCountry(_ sender: Any) {
            let drop = DropDown()
                drop.anchorView = btnCountry
                drop.dataSource = countryArray
                drop.width = btnCountry.frame.size.width
                drop.bottomOffset = CGPoint(x: 0, y: 45)
                 drop.direction = .bottom
        drop.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblCountry.text = "\(item)"
            
            let _ = self.countries.map({
                if $0.name == item{
                    self.getCitiesFromCountries(id: $0.id!)
                    print($0.id)
                }
            })
        }
        drop.show()
       
       

    }
    
    @IBAction func didTappedOnSignUp(_ sender: Any) {
        
        
        guard let fname = self.txtFirstName.text, fname != "First Name", fname != "" else {
            let banner = NotificationBanner(title: "Name", subtitle: "Please enter first name", style: .danger)
            banner.show()
            return
        }
        
        guard let lname = self.txtLastName.text, lname != "Last Name", lname != "" else {
            let banner = NotificationBanner(title: "Name", subtitle: "Please enter last name", style: .danger)
            banner.show()
            return
        }
        
        
        guard let email = self.txtEmail.text, email != "", AppHelper.isValidEmail(testStr: email) else {
            let banner = NotificationBanner(title: "Email", subtitle: "Please enter valid email", style: .danger)
            banner.show()
            return
        }
        
        guard let phone_number = self.txtMobileNo.text, phone_number != "", AppHelper.isValidPhone(testStr: phone_number) else {
            let banner = NotificationBanner(title: "Phone Number", subtitle: "Please enter valid phone number", style: .danger)
            banner.show()
            return
        }
        
//        guard let gender = self.lblGender.text
//            else{
//                let banner = NotificationBanner(title: "Gender", subtitle: "Please enter Your Gender", style: .danger)
//                banner.show()
//                return
//        }
        
        guard let dob = self.lblDOB.text, dob != "Date Of Birth", dob != "" else {
            let banner = NotificationBanner(title: "Date Of Birth", subtitle: "Please enter date of birth", style: .danger)
            banner.show()
            return
        }
        
        guard let country = self.lblCountry.text, country != "country", country != "" else {
            let banner = NotificationBanner(title: "country", subtitle: "Please select country", style: .danger)
            banner.show()
            return
        }
        
        guard let city = self.lblCity.text, city != "city", city != "" else {
            let banner = NotificationBanner(title: "city", subtitle: "Please select city", style: .danger)
            banner.show()
            return
        }
        
        guard let address = self.txtAddress.text, address != "Address", address != "" else {
            let banner = NotificationBanner(title: "Address", subtitle: "Please enter address", style: .danger)
            banner.show()
            return
        }
        
        guard let password = self.txtPassword.text, password != "Address", password != "" else {
            let banner = NotificationBanner(title: "Password", subtitle: "Please enter password", style: .danger)
            banner.show()
            return
        }
        
        guard let confirm_password = self.txtConfirmPassword.text, confirm_password == password, confirm_password != "" else {
            let banner = NotificationBanner(title: "Password", subtitle: "Please enter confirm valid password", style: .danger)
            banner.show()
            return
        }
        
        
        //first_name,last_name,contact,dob,address,country,city,email,password
        
        let param = ["first_name": fname, "last_name": lname,"contact": phone_number, "dob": lblDOB.text, "address": address, "country": country, "city": city, "email": email, "password": password]
        
        signUpService(param: param as! Dictionary<String, String>)
        
    }
    
    func signUpService(param: Dictionary<String, String>){
              Alert.showLoader(message: "Registering")
              UserServices.SignUp(param: param, completionHandler:{ (success, response, error) in
              Alert.hideLoader()
            if !success{
                 print("response:\(response!)")
                let message = response!["message"].stringValue
                let banner = NotificationBanner(title: "Sign Up", subtitle: "\(message)", style: .danger)
                banner.show()
                return
            }
            print("response:\(response!["message"])")
            let message = response!["message"].stringValue
            let banner = NotificationBanner(title: "Sign Up", subtitle: "\(message)", style: .success)
            banner.show()
                let vc = LoginViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
        })
    }
    
    func getCountriesFromServer(){
        SuperMedServices.GetCountries(param: [:]) { (success, response, error) in
            if !success{
                return
            }else{
                print(response)
                self.countries.removeAll()
                self.countryArray.removeAll()
                let array = response!["countries"].arrayValue
                for country in array{
                    let count = Countries(json: country)
                    self.countries.append(count)
                    self.countryArray.append(count.name!)
                }
            }
        }
    }
    
    func getCitiesFromCountries(id: String){
        let params = ["country_id": id]
        
        SuperMedServices.GetCities(param: params) { (success, response, error) in
            if !success{
                return
            }else{
                print(response)
                self.cities.removeAll()
                self.cityArray.removeAll()
                let array = response!["cities"].arrayValue
                for city in array{
                    let ct = Cities(json: city)
                    self.cities.append(ct)
                    self.cityArray.append(ct.name!)
                }
            }
        }
    }
    
    @IBAction func didTappedOnAlreadyAccount(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMale(_ sender: KGRadioButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            btnMale.isSelected = true
            btnFemale.isSelected = false
            lblSelGender.text = "Male"
            print(lblSelGender.text!)
        } else {
            lblSelGender.text = "Female"
        }
        
    }
    
    @IBAction func btnFemale(_ sender: KGRadioButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            btnFemale.isSelected = true
            btnMale.isSelected = false
            lblSelGender.text = "Female"
            print(lblSelGender.text!)
        } else {
            lblSelGender.text = "Male"
        }
    }
    
}
