//
//  AlternatMedViewController.swift
//  MedPharma
//
//  Created by Protege Global on 12/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class AlternatMedViewController: UIViewController {

    class func instantiateFromStoryboard() -> AlternatMedViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AlternatMedViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.PlaceNavigationButtons(selectorForLeft: #selector(MenuAction), leftImage: MENU_IMAGE, selectorForRight:  #selector(goToCart) , rightImage: NOTIFICATION_IMAGE)
        
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
    }
    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @objc func goToCart() {
//        let cart = CartViewController.instantiateFromStoryboard()
//        cart.from = "Home"
//        self.navigationController?.pushViewController(cart, animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
}
