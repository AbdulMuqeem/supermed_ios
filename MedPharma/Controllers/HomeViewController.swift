//
//  HomeViewController.swift
//  MedPharma
//
//  Created by Protege Global on 12/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import SwiftyJSON
//import SnapKit
class HomeViewController: UIViewController {
    
    @IBOutlet weak var viewTopCategory: UIView!
    
    var arrayBanners = [Banners]()
    var arrayProduct = [Products]()
    
    @IBOutlet weak var lblTopMenu: UILabel!
    @IBOutlet weak var lblTopSelling: UILabel!
    
    @IBAction func btnApplyCoupon(_ sender: Any) {
    }
    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    let images = ["icon_lab","icon_docInfo","icon_emergency","icon_history"]
    let home1 = Home(title: "LAB TEST", image:"icon_lab",isSelected: true)
    let home2 = Home(title: "DOCTOR INFORMATION", image:"icon_docInfo",isSelected: false)
    let home3 = Home(title: "EMERGENCY CONTACTS", image:"icon_emergency",isSelected: false)
    let home4 = Home(title: "APPOINTMENT HISTORY", image:"icon_history",isSelected: false)
    var arrayHome = [Home]()
    
    static var cartCounter : String?
    
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    @IBOutlet weak var scrollHomeView: UIScrollView!
    @IBOutlet weak var collectionViewCategories: UICollectionView!
    
    fileprivate let imageNames = ["1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg"]
    fileprivate var numberOfItems : Int?
    
    @IBOutlet weak var collectionCircleView: UICollectionView!
    @IBOutlet weak var pagerView: FSPagerView!
        {      didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.itemSize = .zero
        }
        
    }
    
    
    @IBOutlet weak var pageControl: FSPageControl!{
        didSet {
            self.pageControl.numberOfPages = self.arrayBanners.count
            self.pageControl.contentHorizontalAlignment = .center
            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            self.pageControl.backgroundColor = UIColor.clear
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Home"

        self.PlaceNavigationButtons(selectorForLeft: #selector(MenuAction), leftImage: MENU_IMAGE, selectorForRight:  #selector(NotificationAction) , rightImage: NOTIFICATION_IMAGE)
        
        let userDefaults = UserDefaults.standard
        let cart = userDefaults.value(forKey: "cart")
       // print(cart!)
        
        
        self.placeRightButtons(selectForCart:#selector(goToCart), selectorForFavorite: #selector(goToFavorite), selectorForSearch: #selector(goToSearch), cartValue: (cart as? String ?? "0"))
        arrayHome = [home1, home2, home3, home4]
        
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.pagerView.automaticSlidingInterval = 3.0 - self.pagerView.automaticSlidingInterval
        
        
        collectionCircleView.register(UINib(nibName: "HomeCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionCell")
        
        collectionViewCategories.register(UINib(nibName: "HomeCell", bundle: nil), forCellWithReuseIdentifier: "HomeCell")
        collectionCircleView.delegate = self
        collectionCircleView.dataSource = self
        collectionCircleView.backgroundColor = UIColor.clear
        collectionViewCategories.delegate = self
        collectionViewCategories.dataSource = self
        collectionViewCategories.backgroundColor = UIColor.clear
        
        self.getBanners()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setCounter(notification:)), name: .cart, object: nil)
        
        
    }
    
    
    
    @objc func setCounter(notification: NSNotification){
        
        if GISTUtility.isAppAlreadyLaunchedOnce(){
            let cart = UserManager.getUserObjectFromUserDefaults()?.cartCounter!
            self.placeRightButtons(selectForCart: #selector(goToCart), selectorForFavorite: #selector(goToFavorite), selectorForSearch: #selector(goToSearch), cartValue: cart!)
        }else{
            self.placeRightButtons(selectForCart:#selector(goToCart), selectorForFavorite: #selector(goToFavorite), selectorForSearch: #selector(goToSearch), cartValue: "0")
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userDefaults = UserDefaults.standard
        let is_wishlist = userDefaults.bool(forKey: "wishlist")
        if is_wishlist{
            getProductsFromServer()
            userDefaults.set(false, forKey: "wishlist")
            
        }
        getProductsFromServer()
        
    }

    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @objc func goToSearch(){
        
        let search = SearchViewController.instantiateFromStoryboard()
        search.from = "Home"
        self.navigationController?.pushViewController(search, animated: false)
        
    }
    
    @objc func goToCart(){
        
        let user = UserDefaults.standard.bool(forKey: "AsGuest")
        print(user)
        
        if user {
            Alert.showAlert(title: "Alert", message: "You have logged in as Guest Member, in order to access this feature. you need to Login OR Register on App")
            return
        }
        
        let cart = CartViewController.instantiateFromStoryboard()
        cart.from = "Home"
        self.navigationController?.pushViewController(cart, animated: false)
        
    }
    @objc func goToFavorite(){
        
        let user = UserDefaults.standard.bool(forKey: "AsGuest")
        print(user)
        
        if user {
            Alert.showAlert(title: "Alert", message: "You have logged in as Guest Member, in order to access this feature. you need to Login OR Register on App")
            return
        }
        
        let favorite = CategoryDetailViewController.instantiateFromStoryboard()
        favorite.subTitle = "Wishlist"
        self.navigationController?.pushViewController(favorite, animated: false)
        
    }
    
    @objc func NotificationAction() {
        Alert.showAlert(title: "Alert", message: "Implementation Required")
    }

    func customizationDesign(){
        
        scrollHomeView.snp.makeConstraints{(make)-> Void in
            make.left.equalTo(self.view)
            make.top.equalTo(self.view)
            make.bottom.equalTo(self.view)
            make.right.equalTo(self.view)
        }
        
        scrollHomeView.addSubview(pagerView)
        scrollHomeView.addSubview(pageControl)
        scrollHomeView.addSubview(viewTopCategory)
        
        pagerView.snp.makeConstraints{(make)-> Void in
            make.height.equalTo(SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 165))
            make.width.equalTo(scrollHomeView)
            make.top.equalTo(scrollHomeView.snp.top)
            
            
        }
        
        
        // Top Categories
        
        viewTopCategory.snp.makeConstraints{(make)-> Void in
            make.height.equalTo(SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 90))
            make.width.equalTo(scrollHomeView).offset(-44)
            make.centerY.equalTo(pagerView.snp.bottom)
            make.centerX.equalToSuperview()
            
            
            
        }
        
        viewTopCategory.addSubview(lblTopMenu)
        
        lblTopMenu.snp.makeConstraints{(make)-> Void in
            
            make.top.equalTo(viewTopCategory.snp.top).offset(8)
            make.centerX.equalToSuperview()
            
        }
        
        
        
        
        scrollHomeView.addSubview(collectionCircleView)
        
        
        collectionCircleView.snp.makeConstraints{(make)-> Void in
            make.height.equalTo(SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 110))
            make.width.equalTo(viewTopCategory)
            make.centerY.equalTo(viewTopCategory.snp.bottom)
            make.centerX.equalTo(viewTopCategory)
        }
        
        scrollHomeView.addSubview(lblTopSelling)
        
        lblTopSelling.snp.makeConstraints{(make)-> Void in
            make.top.equalTo(collectionCircleView.snp.bottom).offset(15)
            make.centerX.equalToSuperview()
            
        }
        scrollHomeView.addSubview(collectionViewCategories)
    }
    
}

extension HomeViewController: FSPagerViewDelegate, FSPagerViewDataSource{
    
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.arrayBanners.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        // cell.imageView?.image =  UIImage(named: "\(arrayBanners[""])")   //UIImage(named: self.arrayBanners[index])
        cell.imageView?.setImageFromUrl(urlStr: arrayBanners[index].image!)
        
        cell.imageView?.contentMode = .scaleAspectFit
        cell.imageView?.clipsToBounds = true
        cell.textLabel?.text =  self.arrayBanners[index].name!  //index.description+index.description
        return cell
    }
    
    // MARK:- FSPagerView Delegate
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        self.pageControl.currentPage = index
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        guard self.pageControl.currentPage != pagerView.currentIndex else {
            return
        }
        self.pageControl.currentPage = pagerView.currentIndex // Or Use KVO with property "currentIndex"
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        switch sender.tag {
        case 1:
            let newScale = 0.5+CGFloat(sender.value)*0.5 // [0.5 - 1.0]
            self.pagerView.itemSize = self.pagerView.frame.size.applying(CGAffineTransform(scaleX: newScale, y: newScale))
        case 2:
            self.pagerView.interitemSpacing = CGFloat(sender.value) * 20 // [0 - 20]
        case 3:
            self.numberOfItems = Int(roundf(sender.value * Float(arrayBanners.count)))
            self.pageControl.numberOfPages = self.numberOfItems!
            self.pagerView.reloadData()
        default:
            break
        }
    }
    
    func getBanners(){
        
        SuperMedServices.GetBanners(param: ["":""], completionHandler:{(success, response, error) in
            if !success{
                //print("Response: \(response)")
                return
            }
            print("response:\(response!)")
            let banners = response!["banners"].arrayValue
            if banners.count > 0{
                for banner in banners{
                    let b = Banners(json: banner)
                    self.arrayBanners.append(b)
                    
                    
                }
                
                self.pagerView.reloadData()
                
                
            }
            
            
            
        })
        
    }
    
    // MARK :- Get Products From Server
    
    func getProductsFromServer(){
        
        //  let user_id = UserManager.getUserObjectFromUserDefaults()!.id!
        //print("user_id:",user_id)
        Alert.showLoader(message: "Loading")
        
        SuperMedServices.GetProducts(param: ["userid": UserManager.getUserObjectFromUserDefaults()!.id!], completionHandler: { (success, response, error) in
            Alert.hideLoader()
            if !success{
                return
            }
            else{
                self.arrayProduct.removeAll()
                
                let products = response!["products"].arrayValue
                let cartCounter = response!["cartCounter"].stringValue
                let wishlistCounter = response!["wishlistCounter"].stringValue
                print(cartCounter)
                print(wishlistCounter)
                let user = UserDefaults.standard
                user.set("\(cartCounter)", forKey: "cart")
                user.set("\(wishlistCounter)", forKey: "wishCounter")
                NotificationCenter.default.post(name: .cart, object: nil)
                
                print(wishlistCounter)
                
                print(products)
                
                if products.count > 0{
                    for product in products{
                        
                        print("welcome:",product)
                        let pro = Products(json: product)
                        self.arrayProduct.append(pro)
                        
                    }
                    print(self.arrayProduct)
                    //                    self.collectionViewCategories.snp.makeConstraints{(make)-> Void in
                    //
                    //                        let constant = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: CGFloat(self.arrayProduct.count/2 * 240))
                    //                        print(constant)
                    //                        make.height.equalTo(constant)
                    //                        make.top.equalTo(self.lblTopSelling.snp.bottom).offset(16)
                    //                        make.centerX.equalTo(self.scrollHomeView)
                    //                        make.width.equalTo(self.scrollHomeView).offset(-16)
                    //                        make.bottom.equalTo(self.scrollHomeView.snp.bottom)
                    //                        print(self.arrayProduct.count)
                    //
                    //                    }
                    // self.collectionViewCategories.au
                    self.collectionViewCategories.reloadData()
                }
            }
        })
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        
        if collectionView ==  collectionCircleView  && arrayHome.count > 0{
            return arrayHome.count
        }
        if collectionView ==  collectionViewCategories && arrayProduct.count > 0{
            
            
            return arrayProduct.count
        }else{
            
            return 0
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let p = "Rs"
        let base_image = "https://www.supermed.pk/pos/public/uploads/products/"
        if collectionViewCategories != nil && arrayProduct.count > 0{
            let cellHome = collectionViewCategories.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as! HomeCell
            
            if base_image == arrayProduct[indexPath.row].productImage {
                cellHome.imageProduct.setImageFromUrl(urlStr: "https://www.supermed.pk/pos/public/images/tab-missing.png")
            } else {
                cellHome.imageProduct.setImageFromUrl(urlStr: arrayProduct[indexPath.row].productImage ?? "https://www.supermed.pk/pos/public/images/tab-missing.png")
            }
            cellHome.lblTitleProduct.text = arrayProduct[indexPath.row].productName ?? ""
            cellHome.lbl_product_price.text = p + (arrayProduct[indexPath.row].productPrice ?? "")
            cellHome.lbl_product_oprice.text = ""//p + (arrayProduct[indexPath.row].productOldPrice ?? "0")
            //          cellHome.lbl_product_count.text =  "\(String(describing: arrayProduct[indexPath.row].productQty!))" + " items"
            let productQty = Int(arrayProduct[indexPath.row].productQty ?? "0") ?? 0
            if productQty > 0{
                cellHome.add_to_cart.setTitle("ADD TO CART", for: .normal)
                cellHome.add_to_cart.isUserInteractionEnabled = true
            }
            else{
                cellHome.add_to_cart.setTitle("ADD TO CART", for: .normal)   // *change
                cellHome.add_to_cart.isUserInteractionEnabled = false
            }
            cellHome.add_to_cart.tag = indexPath.item
            cellHome.add_to_cart.addTarget(self, action: #selector(onBtnAddToCart(_:)), for: .touchUpInside)
            return cellHome
        }else{
            
            let cell = collectionCircleView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionCell", for: indexPath) as! HomeCollectionCell
            cell.lbltitle.text = arrayHome[indexPath.row].title
            cell.image.image = UIImage(named: arrayHome[indexPath.row].image)
            return cell
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.collectionViewCategories{
            return CGSize(width:  SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 170), height: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 240))
        }else{
            return CGSize(width:  SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 80), height:  SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 120))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionCircleView{
            
            if indexPath.row == 0 {
                
//                Alert.showLoader(message: "Loading")
//                let userDefaults = UserDefaults.standard
//                let user = userDefaults.bool(forKey: "AsGuest")
//                print(user)
//
//                if user {
//                    let alert = UIAlertController(title: "GuestUser can't have access to Lab Test ", message: "", preferredStyle: UIAlertControllerStyle.alert)
//                    let actionLogin = UIAlertAction(title: "Login", style: UIAlertActionStyle.default) { (alert) in
//                        let vc = LoginViewController.instantiateFromStoryboard()
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//                    let actionRegistration = UIAlertAction(title: "Register", style: UIAlertActionStyle.default) { (alert) in
//                        let vc = RegisterViewController.instantiateFromStoryboard()
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//                    alert.addAction(actionLogin)
//                    alert.addAction(actionRegistration)
//                    self.present(alert, animated: true, completion: nil)
//                }else{
//                    self.dismiss(animated: true, completion: nil)
//                }
//
//                Alert.hideLoader()
                let lab = LabTestViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(lab, animated: true)
            }
            if indexPath.row == 1{
//                Alert.showLoader(message: "Loading")
//                let userDefaults = UserDefaults.standard
//                let user = userDefaults.bool(forKey: "AsGuest")
//                print(user)
//
//                if user{
//                    let alert = UIAlertController(title: "GuestUser can't have access to Doctor's Information ", message: "", preferredStyle: UIAlertControllerStyle.alert)
//                    let actionLogin = UIAlertAction(title: "Login", style: UIAlertActionStyle.default) { (alert) in
//                        let vc = LoginViewController.instantiateFromStoryboard()
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//                    let actionRegistration = UIAlertAction(title: "Register", style: UIAlertActionStyle.default) { (alert) in
//                        let vc = RegisterViewController.instantiateFromStoryboard()
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//                    alert.addAction(actionLogin)
//                    alert.addAction(actionRegistration)
//                    self.present(alert, animated: true, completion: nil)
//                }else{
//                    self.dismiss(animated: true, completion: nil)
//                }
//
//                Alert.hideLoader()
                let doc = DoctorsInfoViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(doc, animated: true)
            }
            if indexPath.row == 2{
//                Alert.showLoader(message: "Loading")
//                let userDefaults = UserDefaults.standard
//                let user = userDefaults.bool(forKey: "AsGuest")
//                print(user)
//
//                if user{
//                    let alert = UIAlertController(title: "GuestUser can't have access to Emergency Contacts ", message: "", preferredStyle: UIAlertControllerStyle.alert)
//                    let actionLogin = UIAlertAction(title: "Login", style: UIAlertActionStyle.default) { (alert) in
//                        let vc = LoginViewController.instantiateFromStoryboard()
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//                    let actionRegistration = UIAlertAction(title: "Register", style: UIAlertActionStyle.default) { (alert) in
//                        let vc = RegisterViewController.instantiateFromStoryboard()
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//                    alert.addAction(actionLogin)
//                    alert.addAction(actionRegistration)
//                    self.present(alert, animated: true, completion: nil)
//                }else{
//                    self.dismiss(animated: true, completion: nil)
//                }
//
//                Alert.hideLoader()
                let emergency = EmergencyViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(emergency, animated: true)
            }
            if indexPath.row == 3 {
                
                //Alert.showLoader(message: "Loading")
//                let userDefaults = UserDefaults.standard
//                let user = userDefaults.bool(forKey: "AsGuest")
//                print(user)
//
//                if user{
//                    let alert = UIAlertController(title: "GuestUser can't have access to Appointment History ", message: "", preferredStyle: UIAlertControllerStyle.alert)
//                    let actionLogin = UIAlertAction(title: "Login", style: UIAlertActionStyle.default) { (alert) in
//                        let vc = LoginViewController.instantiateFromStoryboard()
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//                    let actionRegistration = UIAlertAction(title: "Register", style: UIAlertActionStyle.default) { (alert) in
//                        let vc = RegisterViewController.instantiateFromStoryboard()
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//                    alert.addAction(actionLogin)
//                    alert.addAction(actionRegistration)
//                    self.present(alert, animated: true, completion: nil)
//                }else{
//                    self.dismiss(animated: true, completion: nil)
//                }
                
                let user = UserDefaults.standard.bool(forKey: "AsGuest")
                print(user)
                
                if user {
                    Alert.showAlert(title: "Alert", message: "You have logged in as Guest Member, in order to access this feature. you need to Login OR Register on App")
                    return
                }
                
                Alert.hideLoader()
                
                let appoint = AppointmentViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(appoint, animated: true)
                
            }
            
        }else{
            let product_detail = ProductDetailViewController.instantiateFromStoryboard()
            product_detail.product = [arrayProduct[indexPath.row]]
            self.navigationController?.pushViewController(product_detail, animated: true)
        }
    }
}
//Action add to cart

extension HomeViewController {
    
    @objc func onBtnAddToCart(_ sender: UIButton){
        
        let user = UserDefaults.standard.bool(forKey: "AsGuest")
        print(user)
        
        if user {
            Alert.showAlert(title: "Alert", message: "You have logged in as Guest Member, in order to access this feature. you need to Login OR Register on App")
            return
        }
        
        let product = self.arrayProduct[sender.tag]
        self.addCartItem(product: product)
    }
    
    func addCartItem(product: Products){
        let param : [String: Any] = ["userid": UserManager.getUserObjectFromUserDefaults()!.id!, "productid": product.productID ?? "0", "qty": "1", "action": "0"]
        SuperMedServices.cart_action(param: param, completionHandler: {(success, response, error) in
            if !success {
                //print(response)
                return
            }else{
                Alert.showToast(message: "Product Added to Cart Successfully")
                print("response: \(response!)")
                self.getProductsFromServer()
            }
        })
    }
}


