//
//  HelpCentreDetail.swift
//  MedPharma
//
//  Created by M Usman Bin Rehan on 12/10/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit
import SwiftyJSON

class HelpCentreDetail: UIViewController {

    class func instantiateFromStoryboard() -> HelpCentreDetail {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "HelpCentreDetail") as! HelpCentreDetail
    }
    
    @IBOutlet weak var tableView: UITableView!
    var flagIsShowDesc = false
    var topic_id = ""
    var topic = Topics(json: JSON())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.PlaceNavigationButtonsSearch(selectorForLeft: #selector(self.didTapOnBack), leftImage: BACK_IMAGE, selectorForRight:  #selector(goToSearch) , rightImage: SEARCH_IMAGE)
        self.title = "FAQS"
        self.getTopicDescription()
        // Do any additional setup after loading the view.
    }
    @objc func didTapOnBack() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func goToSearch(){
        let search = SearchViewController.instantiateFromStoryboard()
        search.from = "Home"
        self.navigationController?.pushViewController(search, animated: false)
    }
}
extension HelpCentreDetail: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.flagIsShowDesc{
            return 2
        }
        else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellHeading = tableView.dequeueReusableCell(withIdentifier: "HeadingCell", for: indexPath) as? HeadingCell else {return UITableViewCell()}
        guard let cellDescription = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell", for: indexPath) as? DescriptionCell else {return UITableViewCell()}
        cellHeading.setData(isShowDesc: self.flagIsShowDesc, heading: self.topic.question ?? "-")
        cellDescription.setData(description: self.topic.answer ?? "-")
        if self.flagIsShowDesc{
            switch indexPath.row{
            case 0:
                return cellHeading
            default:
                return cellDescription
            }
        }
        else{
            return cellHeading
        }
    }
}
extension HelpCentreDetail: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 {return}
        self.flagIsShowDesc = !self.flagIsShowDesc
        tableView.reloadData()
    }
}
//MARK:- Services
extension HelpCentreDetail{
    func getTopicDescription(){
        Alert.showLoader(message: "Loading")
        SuperMedServices.getHelpCenterByTopic(param: ["topic_id":self.topic_id]) { (success, response, error) in
            Alert.hideLoader()
            if !success{return}
            else{
                guard let res = response!["topics"].arrayValue.first else {return}
                self.topic = Topics(json: res)
                self.tableView.reloadData()
            }
        }
    }
}
