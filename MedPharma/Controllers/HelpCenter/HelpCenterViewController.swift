//
//  HelpCenterViewController.swift
//  MedPharma
//
//  Created by Protege Global on 12/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class HelpCenterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    class func instantiateFromStoryboard() -> HelpCenterViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HelpCenterViewController
    }
    
    
    @IBOutlet weak var tableHelpCenter: UITableView!
    
   
    
    var hcTopics = [Topics]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.PlaceNavigationButtonsSearch(selectorForLeft: #selector(MenuAction), leftImage: MENU_IMAGE, selectorForRight:  #selector(goToSearch) , rightImage: SEARCH_IMAGE)
        self.title = "Help Center"
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        
        // Do any additional setup after loading the view.
        let nib = UINib(nibName: "HelpCenterTableCell", bundle: nil)
        self.tableHelpCenter.register(nib, forCellReuseIdentifier: "HelpCenterTableCell")
        
        tableHelpCenter.delegate = self
        tableHelpCenter.dataSource = self
        
        getHelpCenterTopicsFromServer()
        
    }
    @objc func MenuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @objc func goToSearch(){
        
        let search = SearchViewController.instantiateFromStoryboard()
        search.from = "Home"
        self.navigationController?.pushViewController(search, animated: false)
        
    }
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hcTopics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableHelpCenter.dequeueReusableCell(withIdentifier: "HelpCenterTableCell") as! HelpCenterTableCell
        cell.lblTopics.text = hcTopics[indexPath.row].topic
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 56)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let controller = HelpCentreDetail.instantiateFromStoryboard()
        controller.topic_id = self.hcTopics[indexPath.row].id ?? "0"
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func getHelpCenterTopicsFromServer(){
        Alert.showLoader(message: "Loading")
        SuperMedServices.helpCenterTopics(param: [:]) { (success, response, error) in
            Alert.hideLoader()
            if !success{
                print("Failure response: \(response)")
                return
            } else{
                print(response)
                
                let array = response!["topics"].arrayValue
                if array.count > 0 {
                for topic in array{
                    let to = Topics(json: topic)
                    self.hcTopics.append(to)
                }
            }
                print(self.hcTopics)
                self.tableHelpCenter.reloadData()
            }
        }
    }
}
