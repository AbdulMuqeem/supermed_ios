//
//  DescriptionCell.swift
//  MedPharma
//
//  Created by M Usman Bin Rehan on 12/10/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class DescriptionCell: UITableViewCell {

    @IBOutlet weak var lblDescription: UILabel!
    
    func setData(description:String){
        self.selectionStyle = .none
        self.lblDescription.text = description
    }
}
