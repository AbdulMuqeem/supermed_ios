//
//  HeadingCell.swift
//  MedPharma
//
//  Created by M Usman Bin Rehan on 12/10/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit

class HeadingCell: UITableViewCell {

    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var viewSeparator: UIView!
    
    func setData(isShowDesc:Bool,heading:String){
        self.selectionStyle = .none
        self.lblHeading.text = heading
        self.viewSeparator.isHidden = isShowDesc
    }
}
