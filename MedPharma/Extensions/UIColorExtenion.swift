//
//  UIColorExtenion.swift
//  SanamTech
//
//  Created by Muhammad Maaz Ul haq on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience init(rgb: UInt) {
        
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func randomFloat() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
    
    static func random() -> UIColor {
        return UIColor(red:   UIColor.randomFloat(),
                       green: UIColor.randomFloat(),
                       blue:  UIColor.randomFloat(),
                       alpha: 1.0)
    }
}

extension UIColor {
    
    
    static var sideMenu: UIColor = {
        return hexStringToUIColor(hex: "#72BF44")
    }()
    static  var navColor: UIColor = {
        return hexStringToUIColor(hex:"#0E7E3f")
    }()
    
    static var lightGreen : UIColor = {
        return hexStringToUIColor(hex: "#72bf44")
    }()
    class func review()-> UIColor{
        return hexStringToUIColor(hex: "#daad9c")
    }
    
    class func misc()->UIColor{
        return hexStringToUIColor(hex: "#ba8ae5")
    }
    
    
    
   
    
    static var searchBarColor : UIColor = {
        return hexStringToUIColor(hex: "#C5C5C5")
    }()
    
    static var sideMenuGray : UIColor = {
        return hexStringToUIColor(hex: "#696969")
    }()
    
    
    
    
    
   class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
