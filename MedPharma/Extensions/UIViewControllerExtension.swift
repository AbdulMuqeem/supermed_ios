//
//  UIViewControllerExtension.swift
//  SanamTech
//
//  Created by Muhammad Maaz Ul haq on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import UIKit


extension Notification.Name {
    static let cart = Notification.Name("cart")
}

extension UIViewController {
    
    //MARK:- Left Button Code
    
    func PlaceLeftButton(image: UIImage, selector: Selector) {
        
        var btnLeft: UIButton?
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setImage(image, for: .normal)
        btnLeft?.addTarget(self, action: selector, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 30, height: 30)
        
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    //MARK:-  Left Button & right Button Code
    
    func PlaceNavigationButtons(selectorForLeft : Selector, leftImage:UIImage ,  selectorForRight : Selector? , rightImage:UIImage) {
        
        var btnLeft: UIButton?
        var btnRight: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setImage(leftImage, for: .normal)
        btnLeft?.addTarget(self, action: selectorForLeft, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 30, height: 30)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.leftBarButtonItems = [ leftBarButton ]
        
        guard let rightSelector = selectorForRight else {return}
        btnRight = UIButton(type: .custom)
        btnRight?.setImage(rightImage, for: .normal)
        btnRight?.addTarget(self, action: rightSelector, for: .touchUpInside)
        btnRight?.frame = CGRect(x: 0, y: 0 , width: 20, height: 20)
        //btnRight?.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: -70)
        
       
        
        
        let badge = UIButton()
        let badgeButton = BBBadgeBarButtonItem(customUIButton: badge)
        
        let user = UserDefaults.standard
        let cartV = user.value(forKey: "cart")
        
        badgeButton?.badgeValue = (cartV == nil)  ? "" : cartV as! String
        badgeButton?.badgeMinSize = 16
        badgeButton?.badgeOriginX = 20
        badgeButton?.badgeOriginY = 0
        badgeButton?.badgeBGColor = UIColor.red
        
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnRight!)
        self.navigationItem.rightBarButtonItems = [rightBarButton,badgeButton] as! [UIBarButtonItem]
        
    }
    
    func PlaceNavigationButtonsSearch(selectorForLeft : Selector, leftImage:UIImage ,  selectorForRight : Selector? , rightImage:UIImage) {
        
        var btnLeft: UIButton?
        var btnRight: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setImage(leftImage, for: .normal)
        btnLeft?.addTarget(self, action: selectorForLeft, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 30, height: 30)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.leftBarButtonItems = [ leftBarButton ]
        
        guard let rightSelector = selectorForRight else {return}
        btnRight = UIButton(type: .custom)
        btnRight?.setImage(rightImage, for: .normal)
        btnRight?.addTarget(self, action: rightSelector, for: .touchUpInside)
        btnRight?.frame = CGRect(x: 0, y: 0 , width: 20, height: 20)
        //btnRight?.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: -70)
        
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnRight!)
        self.navigationItem.rightBarButtonItems = [rightBarButton]
        
    }
    
    
    //MARK:-  Left Buttons with right button title Code
    
    func PlaceLeftButtons(selectorForLeft : Selector, leftImage : UIImage , selectorForRightText : Selector , rightTitle : String, cart: String) {
        
        var btnLeft: UIButton?
        var btnRightText: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setImage(leftImage, for: .normal)
        btnLeft?.addTarget(self, action: selectorForLeft, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 30, height: 30)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.leftBarButtonItems = [ leftBarButton ]
        
        btnRightText = UIButton(type: .custom)
        btnRightText?.setTitle(rightTitle, for: .normal)
        btnRightText?.addTarget(self, action: selectorForRightText, for: .touchUpInside)
        btnRightText?.frame = CGRect(x: 0, y: 0 , width: 30, height: 30)
      
        //
        let badge = UIButton()
        let badgeButton = BBBadgeBarButtonItem(customUIButton: badge)
        badgeButton?.badgeValue = "\(cart)"
        badgeButton?.badgeMinSize = 16
        badgeButton?.badgeOriginX = -15
        badgeButton?.badgeOriginY = 6
        
        let rightTextBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnRightText!)
        self.navigationItem.rightBarButtonItems = [ rightTextBarButton,badgeButton] as! [UIBarButtonItem]
        
    }
    
    func placeRightButtons(selectForCart:Selector, selectorForFavorite: Selector, selectorForSearch: Selector, cartValue: String){
        let search = UIImage(named: "icon_search")
        let cart = UIImage(named: "icon_cart")
        let bookMark = UIImage(named:"icon_bookMark")
        
        var btnCart: UIButton?
        btnCart = UIButton(type: .custom)
        btnCart?.setImage(cart, for: .normal)
        btnCart?.addTarget(self, action: selectForCart, for: .touchUpInside)
        btnCart?.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        //btnCart?.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: -120)
        
        var btnBookMark: UIButton?
        btnBookMark = UIButton(type: .custom)
        btnBookMark?.setImage(bookMark, for: .normal)
        btnBookMark?.addTarget(self, action: selectorForFavorite, for: .touchUpInside)
        btnBookMark?.frame = CGRect(x: 0, y: 0 , width: 20, height: 20)
        //btnBookMark?.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: -70)
        
        var btnSearch: UIButton?
        btnSearch = UIButton(type: .custom)
        btnSearch?.setImage(search, for: .normal)
        btnSearch?.addTarget(self, action: selectorForSearch, for: .touchUpInside)
        btnSearch?.frame = CGRect(x: 0, y: 0 , width: 20, height: 20)
        //btnSearch?.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: -20)
        
        // cart counter
        let badge = UIButton()
        let badgeButton = BBBadgeBarButtonItem(customUIButton: badge)
            let user = UserDefaults.standard
                let cartV = user.value(forKey: "cart")
            badgeButton?.badgeValue = cartV! as! String
            badgeButton?.badgeMinSize = 16
            badgeButton?.badgeOriginX = 20
            badgeButton?.badgeOriginY = 0
            badgeButton?.badgeBGColor = UIColor.red
        
        let badge_nil = UIButton()
        let badgeButtonNil = BBBadgeBarButtonItem(customUIButton: badge_nil)
        badgeButtonNil?.badgeValue = ""
        badgeButtonNil?.badgeMinSize = 16
        badgeButtonNil?.badgeOriginX = 20
        badgeButtonNil?.badgeOriginY = 0
        
        let wish = UIButton()
        let wishButton = BBBadgeBarButtonItem(customUIButton: wish)
        let u = UserDefaults.standard
        let w = u.value(forKey: "wishCounter")
        if w != nil{
             wishButton?.badgeValue = w as! String
        }else{
            wishButton?.badgeValue = "0"
        }

        wishButton?.badgeMinSize = 16
        wishButton?.badgeOriginX = 20
        wishButton?.badgeOriginY = 0
        wishButton?.badgeBGColor = UIColor.red
        
        let rightSearchBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnSearch!)
        let rightCartBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnCart!)
        let rightWishButton: UIBarButtonItem = UIBarButtonItem(customView: btnBookMark!)
        
        let navigationArray = [rightSearchBarButton,badgeButtonNil,rightWishButton,wishButton,rightCartBarButton,badgeButton] as? [UIBarButtonItem]
        self.navigationItem.rightBarButtonItems = navigationArray
    }
}
