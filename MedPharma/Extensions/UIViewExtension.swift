//
//  UIViewExtension.swift
//  MedPharma
//
//  Created by Protege Global on 22/06/2018.
//  Copyright © 2018 Protegeglobal. All rights reserved.
//

import UIKit


@IBDesignable
class GradientView: UIView {
    
    @IBInspectable var startColor: UIColor = UIColor.white {
        didSet {
            gradientLayer.colors = [ startColor.cgColor, endColor.cgColor ]
        }
    }
    
    @IBInspectable var endColor: UIColor = UIColor.black {
        didSet {
            gradientLayer.colors = [ startColor.cgColor, endColor.cgColor ]
        }
    }
    
    @IBInspectable var startPoint: CGPoint = CGPoint(x:0.5, y:0) {
        didSet {
            gradientLayer.startPoint = startPoint
        }
    }
    @IBInspectable var endPoint: CGPoint = CGPoint(x:0.5, y:1) {
        didSet {
            gradientLayer.endPoint = endPoint
        }
    }
    
    private var gradientLayer: CAGradientLayer = CAGradientLayer()
    

    
//    override class func   layerClass() -> AnyClass {
//        return CAGradientLayer.self
//    }
//
    
    
    private func setupLayer() {
        self.layer.insertSublayer(gradientLayer, at: 0)
        
    }

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayer()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupLayer()
    }
    
    override func layoutSubviews() {
        gradientLayer.frame = self.bounds
    }
}

extension CALayer {
    func applySketchShadow(
        color: UIColor = .black,
        alpha: Float = 0.5,
        x: CGFloat = 0,
        y: CGFloat = 2,
        blur: CGFloat = 4,
        spread: CGFloat = 0)
    {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
}


extension UIView{
    
   
    
    func addDashedBorder(strokeColor: UIColor, lineWidth: CGFloat) {
       self.layoutIfNeeded()
       let strokeColor = strokeColor.cgColor
       let shapeLayer:CAShapeLayer = CAShapeLayer()
       let frameSize = self.frame.size
       let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
    
       shapeLayer.bounds = shapeRect
       shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
       shapeLayer.fillColor = UIColor.clear.cgColor
       shapeLayer.strokeColor = strokeColor
       shapeLayer.lineWidth = lineWidth
       shapeLayer.lineJoin = kCALineJoinRound
    
       shapeLayer.lineDashPattern = [5,5] // adjust to your liking
        shapeLayer.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: shapeRect.width, height: shapeRect.height), cornerRadius: self.layer.cornerRadius).cgPath
    
        self.layer.addSublayer(shapeLayer)
    
    
    }

    
}
final class PriceLabel: UILabel {
    
    override func drawText(in rect: CGRect) {
        guard let attributedText = attributedText else {
            super.drawText(in: rect)
            return
        }
        
        if #available(iOS 10.3, *) {
            attributedText.draw(in: rect)
        } else {
            super.drawText(in: rect)
        }
    }
}
import UIKit

extension UIStackView {
    
    func removeAllArrangedSubviews() {
        
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        
        // Deactivate all constraints
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))
        
        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}
