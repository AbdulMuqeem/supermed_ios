//
//  StringExtension.swift
//  SanamTech
//
//  Created by Muhammad Maaz Ul haq on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation

extension String {
    
    func isEmptyOrWhitespace() -> Bool {
        if(self.isEmpty) {
            return true
        }
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces) == ""
    }
    
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func GetDate(format: String) -> String {
        
        var serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            return result
        }()
        
            let serverDateFormatter2: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            return result
        }()
        
        let serverDateFormatter3: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            return result
        }()
        
        let serverDateFormatter4: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            return result
        }()
        
        let serverDateFormatter5: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "dd-MM-yyyy HH:mm:ss a"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 5) as TimeZone!
            return result
        }()
        
        let serverDateFormatter6: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 5) as TimeZone!
            return result
        }()
        
        
        
        if let localDate = serverDateFormatter.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
        // let dateString = dateFormatter(date: "\(date)", format: "yyyy-MM-dd HH:mm:ss Z", newFormat: "EEEE,d")
       // print(dateString!)
        if let day = serverDateFormatter6.date(from: self){
            
            
            let localDateFormat : DateFormatter = {
                let result = DateFormatter()
                    result.dateFormat = format
                    return result
            }()
            
            return localDateFormat.string(from: day)
            
        }
        if let localDate = serverDateFormatter2.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
        
        if let localDate = serverDateFormatter3.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
        if let localDate = serverDateFormatter4.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
            
        if let localDate = serverDateFormatter5.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }

            
        else {
            serverDateFormatter = {
                let result = DateFormatter()
                result.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
                return result
            }()
            
            let localDate = serverDateFormatter.date(from: self)!
            
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
    }
    
    
    
    func GetTime() -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "dd/mm/yyyy hh:mm:ss a"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            return result
        }()
        
        let serverTime = self
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localTimeFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "hh:mm a"
            return result
        }()
        
        return localTimeFormatter.string(from: localTime)
    }
    
    func GetTime(toFormat: String) -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "dd/mm/yyyy hh:mm:ss a"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            return result
        }()
        
        let serverTime = self
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localTimeFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = toFormat
            return result
        }()
        
        
        return localTimeFormatter.string(from: localTime)
    }
    
    func GetTime(fromFormat: String) -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = fromFormat
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            return result
        }()
        
        let serverTime = self
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localTimeFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "hh:mm a"
            return result
        }()
        
        
        return localTimeFormatter.string(from: localTime)
    }
    
    func GetTime(fromFormat: String, toFormat: String) -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = fromFormat
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            return result
        }()
        
        let serverTime = self
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localTimeFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = toFormat
            return result
        }()
        
        
        return localTimeFormatter.string(from: localTime)
    }
    
    
}
extension NSMutableAttributedString {
    
    /**
     Applies a text alignment to the entire string.
     
     - parameter alignment: The text alignment.
     */

    static func findPercentage(discountValue: Float, originalValue: Float)-> NSMutableAttributedString {
        
        
        let percentage  = 1.0 - (discountValue / originalValue)
        let v = (percentage * 100.0)
        let p = Int(v)
        let string = "(\(p)%)"
        let txtRange = NSMakeRange(0, string.count)
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttributes([NSAttributedStringKey.foregroundColor: UIColor.red], range: txtRange)
        return attributedString
    }
    
    public static func strikeText(text: String)-> NSMutableAttributedString{

        
        
        let textRange = NSMakeRange(0, text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSAttributedStringKey.strikethroughStyle,
                                    value: NSUnderlineStyle.styleSingle.rawValue,
                                    range: textRange)
    
        let attributes = [NSAttributedStringKey.foregroundColor : UIColor.red]
        attributedText.addAttributes(attributes, range: textRange)
        return attributedText
    }
}
