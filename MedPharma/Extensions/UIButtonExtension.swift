//
//  UIButtonExtension.swift
//  SanamTech
//
//  Created by Muhammad Maaz Ul haq on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIButton {
    
    func setImageURL(urlStr:String) {
        
        let url = URL(string: urlStr)!
        print(url)
        self.kf.setImage(with: url, for: .normal)
        
    }
}
