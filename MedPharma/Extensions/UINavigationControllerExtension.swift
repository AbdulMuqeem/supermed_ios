//
//  UINavigationControllerExtension.swift
//  SanamTech
//
//  Created by Muhammad Maaz Ul haq on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    
    func TransparentNavigationBar() {
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.isTranslucent = true
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.backgroundColor = UIColor.clear
        setNavigationBarHidden(false, animated: true)
    }
    
    func ThemedNavigationBar() {
        let image = UIImage(named: "topbar")!
        self.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        self.navigationBar.barTintColor = UIColor.white
        self.navigationBar.isTranslucent = false

       
    }
    
    func setNavigationTitle(name: String)-> UILabel{
        
        let navigationTitleFont = UIFont(name: "SteelfishRg-BoldItalic", size: 26)!
        let attr = [NSAttributedStringKey .font: navigationTitleFont, NSAttributedStringKey .foregroundColor : UIColor.white]
        let titleLabel = UILabel()
        let attrString = NSMutableAttributedString(string: name, attributes: attr)
        titleLabel.attributedText = attrString
        titleLabel.frame.size = CGSize(width: 200, height: 50)
        titleLabel.textAlignment = .center
        
        return titleLabel
    }
    
    func ChangeNavigationBarColor(color: UIColor) {
        self.navigationBar.barTintColor = color
        self.navigationBar.isTranslucent = false
    }
    
    func HideNavigationBar() {
        self.isNavigationBarHidden = true
    }
    
    func ShowNavigationBar() {
        self.isNavigationBarHidden = false
    }
    
    func ChangeTitleColor(color: UIColor) {
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: color]
    }
    
}
extension UIApplication {
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
}
