//
//  UIImageViewExtension.swift
//  SanamTech
//
//  Created by Muhammad Maaz Ul haq on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
    
    func setImageFromUrl(urlStr:String) {
    
      var tempStr = urlStr.replacingOccurrences(of: "\\", with: "/")
      tempStr = tempStr.replacingOccurrences(of: " ", with: "%20")

        let url = URL(string: tempStr)!
      print(url)
        
        self.kf.indicatorType = .activity
        self.kf.setImage(with: url,
                         placeholder: nil,
                         options: [.transition(.fade(1))],
                         progressBlock: nil,
                         completionHandler: nil)
        
    }
    
}
