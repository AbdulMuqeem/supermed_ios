//
//  UIImageExtension.swift
//  Syllabit
//
//  Created by Ayaz Akbar on 6/8/18.
//  Copyright © 2018 Protege Global. All rights reserved.
//

import UIKit

    extension UIImage {
        func tinted(with color: UIColor) -> UIImage? {
            UIGraphicsBeginImageContextWithOptions(size, false, scale)
            defer { UIGraphicsEndImageContext() }
            color.set()
            withRenderingMode(.alwaysTemplate)
                .draw(in: CGRect(origin: .zero, size: size))
            return UIGraphicsGetImageFromCurrentImageContext()
        }
    }

