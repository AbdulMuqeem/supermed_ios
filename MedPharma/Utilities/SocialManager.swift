//
//  SocialManager.swift
//  Syllabit
//
//  Created by Protege Global on 20/03/2018.
//  Copyright © 2018 Protege Global. All rights reserved.
//

/*
 AppDelegate
 
 Include these lines in didFinishLaunchingWithOptions function in AppDelegate
 
 SocialManager.GoogleAuth(clientID: "446265902599-v5bt4ltp2765052ba2sjvrchkrpo3v72.apps.googleusercontent.com")
 SocialManager.TwitterAuth(consumerKey: "olaXZZaHr3qF7ETwZTg0TazML", consumerSecret: "s7OOLKLe1Tm5RlFYH7U5LkqhC1n7tXx32Ou72MfqgI0UGLbeJ2")
 SocialManager.EnableFacebook(application: application, launchOptions: launchOptions)
*/

import Foundation
import TwitterKit
import GoogleSignIn
import FBSDKLoginKit

class SocialManager: NSObject {
    
    //MARK: URL Scheme
    static func URLScheme(app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if(url.scheme!.isEqual("fbauth2:/")) {
            print("Facebook url scheme")
            
            let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: (options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String), annotation: options[UIApplicationOpenURLOptionsKey.annotation])
            return handled
        }
        else if(url.scheme!.isEqual("twitterkit-\(TWTRTwitter.sharedInstance().authConfig.consumerKey.lowercased())")) {
            print("Twitter url scheme")
            
            let handled: Bool = TWTRTwitter.sharedInstance().application(app, open: url, options: options)
            return handled
        } else {
            print("Google+ url scheme")
            
            let handled: Bool = GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
            return handled
        }
    }
    
    //MARK: Twitter
    static func TwitterAuth(consumerKey: String, consumerSecret: String) {
        TWTRTwitter.sharedInstance().start(withConsumerKey: consumerKey, consumerSecret: consumerSecret)
    }
    static func TwitterLogin(callback: ((TWTRSession?, Error?) -> Void)?) {
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                print("Social Manager - Twitter - SignIn User: \(session!.userName)")
                callback?(session, nil)
            } else {
                print("Social Manager - Twitter - SignIn Error: \(error!.localizedDescription)")
                callback?(nil, error)
            }
        })
    }
    
    static func TwitterIsUserLogedIn() -> Bool {
        if SocialManager.TwitterGetSession() != nil {
            print("Social Manager - Twitter - User: Signed In")
            return true
        }
        else {
            print("Social Manager - Twitter - User: Not Signed In")
            return false
        }
    }
    
    static func TwitterGetStore() -> TWTRSessionStore {
        return TWTRTwitter.sharedInstance().sessionStore
    }
    
    static func TwitterGetSession() -> TWTRAuthSession? {
        if let session = SocialManager.TwitterGetStore().session() {
            print("Social Manager - Twitter - Session: Get Successfully")
            return session
        }
        print("Social Manager - Twitter - Session: Not Found")
        return nil
    }
    
    static func TwitterLogout(callback: ((Bool) -> Void)?) {
        let store = SocialManager.TwitterGetStore()
        if let session = SocialManager.TwitterGetSession() {
            store.logOutUserID(session.userID)
            print("Social Manager - Twitter - User: Logout Successfully")
            callback?(true)
        }
        else {
            print("Social Manager - Twitter - User: Session Not Found")
            callback?(false)
        }
    }
    
    //MARK: Google
    static func GoogleAuth(clientID: String) {
        GIDSignIn.sharedInstance().clientID = clientID
    }
    
    static func GoogleSetDelegate(delegate : GIDSignInDelegate, uiDelegate: GIDSignInUIDelegate) {
        GIDSignIn.sharedInstance().delegate = delegate
        GIDSignIn.sharedInstance().uiDelegate = uiDelegate
        print("Social Manager - Google - Delegate: Set Successfully")
    }
    
    static func GoogleLogin() {
        if GIDSignIn.sharedInstance().delegate == nil {
        if GIDSignIn.sharedInstance().uiDelegate == nil {
                print("Social Manager - Google - UIDelegate: Error")
                return
            }
            else {
                print("Social Manager - Google - Delegate: Error")
                return
            }
        }
        else if GIDSignIn.sharedInstance().uiDelegate == nil {
            print("Social Manager - Google - UIDelegate: Error")
            return
        }
        GIDSignIn.sharedInstance().signIn()
    }
    
    static func GoogleIsUserLogedIn() -> Bool {
        if SocialManager.GoogleGetUser() != nil {
            print("Social Manager - Google - User: Signed In")
            return true
        }
        else {
            print("Social Manager - Google - User: Not Signed In")
            return false
        }
    }
    
    static func GoogleGetSharedInstance() -> GIDSignIn? {
        return GIDSignIn.sharedInstance()
    }
    
    static func GoogleGetUser() -> GIDGoogleUser? {
        if let user = SocialManager.GoogleGetSharedInstance()?.currentUser {
            print("Social Manager - Google - User: Get Successfully")
            return user
        }
        print("Social Manager - Google - User: Not Found")
        return nil
    }
    
    static func GoogleLogout() -> Bool {
        GIDSignIn.sharedInstance().signOut()
        return true
    }
    
    //MARK: Facebook
    static func EnableFacebook(application: UIApplication, launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    static func FacebookLogin(from: UIViewController, permisions: [String], callback: ((FBSDKLoginManagerLoginResult?, Error?) -> Void)?) {
        
        let loginManager = FBSDKLoginManager()
        Alert.hideLoader()
        
        loginManager.logIn(withReadPermissions: permisions, from: from) { (loginResult, error) in
            
            // Issue****
    
            if error != nil {
                print("Social Manager - Facebook - SignIn Error: \(error!.localizedDescription)")
                callback?(nil, error)
                return
            }
            if (loginResult?.isCancelled)! {
                print("Social Manager - Facebook - SignIn Error: Cancelled by user")
                let error = NSError.init(domain: "user", code: 1000, userInfo: [:])
                callback?(nil, error as Error)
                return
            }
            print("Social Manager - Facebook - SignIn User: Successfull")
            callback?(loginResult, nil)
            
        }
    }
    
    static func FacebookIsUserLogedIn() -> Bool {
        if FBSDKAccessToken.current() != nil {
            print("Social Manager - Facebook - User: Signed In")
            return true
        }
        else {
            print("Social Manager - Facebook - User: Not Signed In")
            return false
        }
    }
    
    static func FacebookGetUser(graphPath: String, parameters: [String: String], callback: ((FBSDKGraphRequestConnection?, Any?, Error?) -> Void)?) {
        
        if SocialManager.FacebookIsUserLogedIn() {
            
            let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: graphPath, parameters: parameters)
            
            graphRequest.start(completionHandler: { (connection, result, error) -> Void in
                
                if ((error) != nil) {
                    print("Social Manager - Facebook - User: Error: \(error!.localizedDescription)")
                    callback?(nil, nil, error)
                }
                else {
                    print("Social Manager - Facebook - User: Get Successfully")
                    callback?(connection, result, nil)
                }
            })
            return
        }
        
        print("Social Manager - Facebook - SignIn User: Not Signed In")
        let error = NSError.init(domain: "user", code: 1000, userInfo: [:])
        callback?(nil, nil, error as Error)
        
    }
    
    static func FacebookLogout() -> Bool {
        if SocialManager.FacebookIsUserLogedIn() {
            FBSDKLoginManager().logOut()
            return true
        }
        print("Social Manager - Facebook - User: Already logout")
        return true
    }
}
