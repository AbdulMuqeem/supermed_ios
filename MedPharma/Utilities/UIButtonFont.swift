//
//  UIButtonFont.swift
//  SanamTech
//
//  Created by Muhammad Maaz Ul haq on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import AMXFontAutoScale

extension UIButton: AMXFontAutoScalable {
    public var amx_originalFontPointSize: CGFloat {
        get {
            return 0.0
        }
        set(amx_originalFontPointSize) {
            
        }
    }

    public var amx_fontSizeUpdateHandler: AMXFontUpdateHandler! {
        get {
            return nil
        }
        set(amx_fontSizeUpdateHandler) {
            
        }
    }
    
    public static var amx_autoScaleEnabled: Bool {
        get {
            return true
        }
        set {
            //self.amx_autoScaleEnabled = newValue
        }
    }
    
    public var amx_autoScaleEnabled: Bool {
        get {
            return true
        }
        set {
            //self.amx_autoScaleEnabled = newValue
        }
    }
    
    public var amx_referenceScreenSize: AMXScreenSize {
        get {
            return self.amx_referenceScreenSize
        }
        set {
            //self.amx_referenceScreenSize = newValue
        }
    }
    
    public static var amx_referenceScreenSize: AMXScreenSize {
        get {
            return self.amx_referenceScreenSize
        }
        set {
            //self.amx_referenceScreenSize = newValue
        }
    }
    
    public static func amx_autoScaleFont(forReferenceScreenSize screenSize: AMXScreenSize) {
        self.amx_referenceScreenSize = screenSize
    }
    
    public func amx_autoScaleFont(forReferenceScreenSize screenSize: AMXScreenSize) {
        self.amx_referenceScreenSize = screenSize
    }
}

extension UITextField: AMXFontAutoScalable {
    public var amx_originalFontPointSize: CGFloat {
        get {
            return 0.0
        }
        set(amx_originalFontPointSize) {
            
        }
    }

    public var amx_fontSizeUpdateHandler: AMXFontUpdateHandler! {
        get {
            return nil
        }
        set(amx_fontSizeUpdateHandler) {
            
        }
    }
    
    public static var amx_autoScaleEnabled: Bool {
        get {
            return true
        }
        set {
            //self.amx_autoScaleEnabled = newValue
        }
    }
    
    public var amx_autoScaleEnabled: Bool {
        get {
            return true
        }
        set {
            //self.amx_autoScaleEnabled = newValue
        }
    }
    
    
    public var amx_referenceScreenSize: AMXScreenSize {
        get {
            return self.amx_referenceScreenSize
        }
        set {
            //self.amx_referenceScreenSize = newValue
        }
    }
    
    public static var amx_referenceScreenSize: AMXScreenSize {
        get {
            return self.amx_referenceScreenSize
        }
        set {
            //self.amx_referenceScreenSize = newValue
        }
    }
    
    public static func amx_autoScaleFont(forReferenceScreenSize screenSize: AMXScreenSize) {
        self.amx_referenceScreenSize = screenSize
    }
    
    public func amx_autoScaleFont(forReferenceScreenSize screenSize: AMXScreenSize) {
        self.amx_referenceScreenSize = screenSize
    }
}
