//
//  Enums.swift
//  SanamTech
//
//  Created by Muhammad Maaz Ul haq on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation

enum BackButton: String {
    case LIGHT
    case DARK
}
enum Duration: String{
    case Day = "Day"
    case Week = "Week"
    case Month = "Month"
}
enum ClassType:String {
    case Homework
    case Test
    case Review
    case Quiz
    case Misc
}
