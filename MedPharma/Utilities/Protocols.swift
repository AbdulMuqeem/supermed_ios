//
//  Protocols.swift
//  SanamTech
//
//  Created by Muhammad Maaz Ul haq on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import UIKit

protocol RefreshDelegate {
    func StartRefresh()
}

protocol AddFriendDelegate {
    func requestSent(indexpath:IndexPath)
}

protocol HomeTaskDelegate {
    func homeAction(indexpath:IndexPath)
}

protocol DeleteSyllabusDelegate {
    func deleteSyllabus(indexpath:IndexPath)
}

protocol AcceptDeclineDelegate {
    func requestAccept(indexpath:IndexPath)
    func requestDecline(indexpath:IndexPath)
}
protocol SelectAgendaDelegate{
    func didTapOnButton(type:String)
}
protocol SearchDelegate {
   // func goToDetail(data: Task)
}
//protocol SearchDelegate : class where self : SearchView {
//    func goToDetail(data: Task)
//}

