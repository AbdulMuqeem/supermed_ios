//
//  Constants.swift
//  SanamTech
//
//  Created by Muhammad Maaz Ul haq on 12/23/17.
//  Copyright © 2017 Ingic. All rights reserved.
//

import Foundation
import UIKit

public let THEME_COLOR: UInt = 0x063f5a
//public let THEME_LOGO: UIImage = UIImage(named: "logo")!
//public let GRAY_SCALE_LOGO: UIImage = UIImage(named: "logo")!

public let BACK_IMAGE: UIImage = UIImage(named:"icon_back")!
public let MENU_IMAGE: UIImage = UIImage(named: "menu")!
public let CART_IMAGE: UIImage = UIImage(named: "icon_cart")!
public let BACK_IMAGE_LIGHT: UIImage = UIImage(named: "backbtn")!
public let SEARCH_IMAGE: UIImage = UIImage(named: "icon_search")!
public let FILTER_IMAGE: UIImage = UIImage(named: "filter")!
public let FILTER_WHITE_IMAGE: UIImage = UIImage(named: "filter_white")!
public let NOTIFICATION_IMAGE: UIImage = UIImage(named: "notifications")!
public let MESSAGE_IMAGE: UIImage = UIImage(named: "message_white")!
public let ADD_IMAGE: UIImage = UIImage(named: "add")!
public let EDIT_IMAGE: UIImage = UIImage(named: "edit")!
public let EYE_IMAGE: UIImage = UIImage(named: "eye")!

public let User_data_userDefault   = "User_data_userDefault"
public let token_userDefault   = "token_userDefault"
public let NOTIFICATION_USERUPDATE = "user_update"
public let NOTIFICATIOIN_CARTUPDATE = "cart_update"

public let GOOGLE_CLIENT_ID = "135777747100-jb9gjsj9i0auvapm7uumaiq7tj8l4re9.apps.googleusercontent.com"
public let TWITTER_CONSUMER_KEY = "olaXZZaHr3qF7ETwZTg0TazML"
public let TWITTER_CONSUMER_SECRET = "s7OOLKLe1Tm5RlFYH7U5LkqhC1n7tXx32Ou72MfqgI0UGLbeJ2"

public let imageURLAddFriend = "http://66.171.248.10:8083/assets/profilepictures/"
public let imageURLTasks = "http://66.171.248.10:8083/assets/tasks/"
public let fileURL = "http://66.171.248.10:8083/assets/syllabus/"

// anlge for TEXT

public let ANGLE_TEXT = CGFloat(45 * Float.pi / -500)

