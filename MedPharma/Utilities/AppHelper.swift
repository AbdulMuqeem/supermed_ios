//
//  AppHelper.swift
//  SanamTech
//
//  Created by Muhammad Maaz Ul haq on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class AppHelper: NSObject {
    
    static func isValidEmail(testStr:String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: testStr)
    }
    
    static func isValidPassword(testStr:String) -> Bool {
        if testStr.characters.count < 3 {
            return false
        }
        return true
    }
    
    static func isValidAge(testStr:String) -> Bool {
        if testStr == "0" {
            return false
        }
        if Int.init(testStr)! > 150 {
            return false
        }
        if testStr.characters.count > 3 {
            return false
        }
        return true
    }
    
    static func isValidPhone(testStr:String) -> Bool {
        if testStr.characters.count < 9 {
            return false
        } else if testStr.characters.count > 15 {
            return false
        }
        return true
    }
}
