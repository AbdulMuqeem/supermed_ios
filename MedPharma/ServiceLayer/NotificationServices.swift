//
//  NotificationServices.swift
//  Syllabit
//
//  Created by Protege Global on 19/04/2018.
//  Copyright © 2018 Protege Global. All rights reserved.
//

import Foundation
import SwiftyJSON

class NotificationServices {
    
    //MARK:- Update Notification Service Method
    
    static func UpdateNotification(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.updateNotification, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].stringValue != "success" {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Terms Service Method
    
    static func TermsAndConditions(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.get_terms, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].stringValue != "true" {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
}

