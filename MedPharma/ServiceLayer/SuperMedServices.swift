//
//  SyllabiServices.swift
//  Syllabit
//
//  Created by Protege Global on 04/04/2018.
//  Copyright © 2018 Protege Global. All rights reserved.
//

import Foundation
import SwiftyJSON

class SuperMedServices {
    
    
    //MARK:-  Banner Images Service Method
    
    static func GetBanners(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.getBanners, parameters: param, callback:{(response, error) in
           Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
              //  Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    
    
    // MARK:- Get Categories
    
    static func GetCategories(param:[String:Any],completionHandler: @escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.getCategories, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
              
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    
    // MARK:- Get Categories Detail By ID with Post method
    static func GetCategoriesByID(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.getCategoriesDetail, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
               
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
   
    // MARK:- Get Products Data Method Get
    static func GetProducts(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.getProducts, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
               // Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    
    // MARK:- Get Products sub categories By ID with Post method
    static func GetProductSubCategoriesByID(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.getProductSubCategories, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
               // Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    
    //MARK:-  Get My Specia List Service Get Method
    
    static func getSpecialization(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.getSpecialization, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
               // Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    
    // MARK :- POST To Get Doctors List By Specialization ID: spec_id = "4"
    
    static func getDoctorsListBySpecID(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.list_of_doctor, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
               // Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // Mark:- Add Wishlist by the post  funtions
    
    static func addWishlist(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.addWishlist, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
              //  Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // Mark:- Get Wishlist by the post  funtions
    
    static func getWishlist(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.getWishlist, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
              //  Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // Mark:- Delete WishList Item
    
    static func deleteWishlistItem(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.deleteWishlist, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
               // Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // Mark:- Move WishList Item
    
    static func moveWishlistItem(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.moveWishlist, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
             //   Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }

    // Mark:- Cart  Actions
    
    static func cart_action(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.cart_Action, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
              //  Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    // Mark:- Get Cart Products
    
    static func getCartProducts(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.get_cart, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
               // Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }

    // Mark:- Validate Promo code
    
    static func validatePromoCodeORCoupon(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.validate_coupon, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
               // Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // MARK:- Get List Of LAB TEST
    
    static func getLABTest(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.get_list_lab, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
               // Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // MARK:- Load List Of LAB TEST
    
    static func loadLABTest(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.load_lab_test, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
               // Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // Mark:- List of LAB Test By POST lab_id
    
    static func LabTestByLab_ID(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.list_test, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
               // Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // Mark:- List of Search Products POST search
    
    static func searchProducts(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.searchRelative, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
//              let cartCounter = response?["cartCounter"].int
//              let wishlistCounter = response?["wishlistCounter"].int
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
              //  Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            print(response)
            guard let res = response else {return}
            completionHandler(true, res, nil)
        })
    }
    
    
    
    
    // MARK :- Upload Prescription Post userid and image
    
    
    static func uploadPrescription(param:[String:Any], image: UIImage,completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.uploadImageAndParam(url: ServiceApiEndPoints.uploadPrescription, image: image, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
              //  Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    // MARK:- Appointment History Lab
    
    static func appointmentHistory(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.appointmentHistory, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
             //   Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // MARK:- Book Appointment History Lab
    
    static func bookAppointment(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        
        
        Request.PostRequestString(fromSavedUrl: ServiceApiEndPoints.book_appointment, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
              
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // MARK:- Contact us
    
    static func contactUS(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.contactus, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
               // Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // MARK:- Subscribe News letter
    
    static func subscribeNewsLetter(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.subscribeNewsLetter, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    // MARK:- Alternative search
    static func alternativeProductSearch(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.searchAlternateMed, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            guard let res = response else {return}
            completionHandler(true, res, nil)
        })
    }
    
    // MARK:- Checkout search
    static func checkout(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.checkout, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    static func helpCenterTopics(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.getTopics, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                // Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    static func getHelpCenterByTopic(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.getHelpCenterByTopic, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    static func GetCountries(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.getCountries, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                // Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    static func GetCities(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.getCities, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    static func GetEmergency(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.getEmergency, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                // Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // Doctor Info By ID
    static func doctorInfoById(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.list_of_doctorById, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    // MARK:- Login As Guest
    
    static func GetGuestUser(param:[String:Any],completionHandler:@escaping ( _ success:Bool, _ response:JSON?,_ error:Error?)->Void) {
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.getGuestUserID, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                //  Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    static func OrderHistory(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.list_of_order, parameters: param, callback:{(response, error) in
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
}
