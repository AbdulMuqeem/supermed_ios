//
//  Singleton.swift
//  AdHouse
//
//  Created by muhammadmaazulhaq on 04/08/2017.
//  Copyright © 2017 Abdul Muqeem. All rights reserved.
//

import Foundation
import CoreData

private let singleton = Singleton()

class Singleton {
    
    var  CurrentUser: Profile? = UserManager.getUserObjectFromUserDefaults()
    
    class var sharedInstance: Singleton {
        return singleton
    }
}

final class ExampleSingleton {
    
    static let sharedInstance = ExampleSingleton()
    static let name = ""
    private init(){
    }
}
 class Test{
       static let sharedInstance = Test()
    private init(){
    }
}
