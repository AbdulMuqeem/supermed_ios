 //
//  Request.swift
//  Tutorial
//
//  Created by muhammadmaazulhaq on 15/06/2017.
//  Copyright © 2017 muhammadmaazulhaq. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Request: NSObject {
    
    static func GetRequestWHeader(fromSavedUrl url: String, header:[String:String], parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        Alamofire.request(url, method: .get, parameters: parameters, headers: header).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func GetRequest(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        Alamofire.request(url, method: .get, parameters: parameters, headers: [:]).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func PostRequestWHeader(fromSavedUrl url: String, header:[String:String], parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: header).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func PostRequest(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: [:]).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        
                    
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                     print(response)
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print(response)
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    
    static func PostRequestString(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        let u = URL(string: url)
        
        var request = URLRequest(url: u!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters)
        
        Alamofire.request(request)
            .responseJSON { response in
               switch response.result {
                case .failure(let error):
                    print(error)
                
                                            print("Request failed with error: \(response.result.error!.localizedDescription)")
                                            print(response)
                                            callback?(nil, response.result.error!)
                                      break
                case .success(let responseObject):
                    if response.result.isSuccess{
                        if let value: Any = response.result.value as AnyObject? {
                                                        print(value)
                            
                                                    let response = JSON(value)
                                                    callback?(response, nil)
                    }
                        
                    }else {
                                            print("Request failed with error: \(response.result.error!.localizedDescription)")
                                            print(response)
                                            callback?(nil, response.result.error!)
                                        }
                }
        }
        
        
//
//        let headers = ["Content-Type": "application/json;charset=utf-8"]
//
////        Alamofire.request(.POST, "https://httpbin.org/post", parameters: parameters, encoding: .JSON)
////
////
////        Alamofire.request(url, method:.post, parameters: parameters, encoding: .JSON, headers: [:]).responseJSON(data:DataResponse<Any> ){
////
////        }
////
//
//        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseString{ (response: DataResponse <String>) in
//
//            switch(response.result) {
//            case .success(_):
//
//                if response.result.isSuccess {
//                    if let value: Any = response.result.value as AnyObject? {
//                            print(value)
//
//                        let response = JSON(value)
//                        callback?(response, nil)
//                    }
//                }
//                else {
//                    print("Request failed with error: \(response.result.error!.localizedDescription)")
//                    print(response)
//                    callback?(nil, response.result.error!)
//                }
//            case .failure(_):
//                print(response)
//                print("Request failed with error: \(response.result.error!.localizedDescription)")
//                callback?(nil, response.result.error!)
//                break
//            }
//        }
//        Alamofire.request(url, method: .post, parameters: parameters, headers: [:]).responseJSON{ (response:DataResponse<Any>) in
//
//            switch(response.result) {
//            case .success(_):
//
//                if response.result.isSuccess {
//                    if let value: Any = response.result.value as AnyObject? {
//
//
//                        let response = JSON(value)
//                        callback?(response, nil)
//                    }
//                }
//                else {
//                    print("Request failed with error: \(response.result.error!.localizedDescription)")
//                    print(response)
//                    callback?(nil, response.result.error!)
//                }
//            case .failure(_):
//                print(response)
//                print("Request failed with error: \(response.result.error!.localizedDescription)")
//                callback?(nil, response.result.error!)
//                break
//            }
//        }
    }
    
    
    static func PostRequestwithQuery(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        Alamofire.request(url, method: .post, parameters: parameters , encoding: URLEncoding.queryString, headers: [:]).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func DeleteRequestWHeader(fromSavedUrl url: String, header:[String:String], parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        Alamofire.request(url, method: .delete, parameters: parameters, headers: header).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func DeleteRequest(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        print("API - Request URL: \(url)")
        
        Alamofire.request(url, method: .delete, parameters: parameters, headers: [:]).responseJSON{ (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.isSuccess {
                    if let value: Any = response.result.value as AnyObject? {
                        let response = JSON(value)
                        callback?(response, nil)
                    }
                }
                else {
                    print("Request failed with error: \(response.result.error!.localizedDescription)")
                    callback?(nil, response.result.error!)
                }
            case .failure(_):
                print("Request failed with error: \(response.result.error!.localizedDescription)")
                callback?(nil, response.result.error!)
                break
            }
        }
    }
    
    static func downloadFile(url: String, callback: ((String?) -> Void)?) {
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let file = directoryURL.appendingPathComponent("myFileName.pdf", isDirectory: false)
            return (file, [.createIntermediateDirectories, .removePreviousFile])
        }
        
        Alamofire.download(url, to:destination)
            .downloadProgress { (progress) in
                print((String)(progress.fractionCompleted))
            }
            .responseData { (data) in
                
                //Get the local docs directory and append your local filename.
                var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last as NSURL?
                
                docURL = docURL?.appendingPathComponent( "myFileName.pdf") as NSURL?
                
                //Lastly, write your file to the disk.
                do {
                    try data.value?.write(to: docURL! as URL)//.writeToURL(docURL!, atomically: true)
                }
                catch {
                    
                }
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                let path = paths[0]
                
                let pathToFile = (path as NSString).appendingPathComponent("myFileName.pdf")
                
                callback!(pathToFile)
                if FileManager.default.fileExists(atPath: pathToFile){
                    print("file Exist")
                    return
                }
                Alert.showAlert(title: "Alert", message: "Your download has been completed")
                print(data)
        }
        
    }
    
    
    func getSaveFileUrl(fileName: String) -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL;
    }
    
    
//    static  func changProfilePicture(){
//
//
//
//         Alamofire.upload(multipartFormData: { multipartFormData in
//        for (key,value) in parameters {
//                 multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
//        }
//                 multipartFormData.append(data, withName: "avatar", fileName: fileName!,mimeType: "image/jpeg")
//    },
//         usingTreshold: UInt64.init(),
//    to: "",
//    method: .put,
//    encodingCompletion: { encodingResult in
//    switch encodingResult {
//    case .success(let upload, _, _):
//    upload.responJSON { response in
//    debugPrint(response)
//    }
//    case .failure(let encodingError):
//    print(encodingError)
//    }
//    })
//    }
    
    
    /*
     for (key , value) in parameters {
     if let data:Data = value as? Data {
     //multipartFormData.append(data, withName: key, fileName: "fileName.jpeg", mimeType: "image/jpeg")
     multipartFormData.append(data, withName: key, fileName: "\(data.getExtension)", mimeType: "\(data.mimeType)")
     } else {
     multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
     }
     }
     */
    
    static func postProfileImage(url: String,imageData: Data,fileName: String,parameters: [String: Any],callback:((JSON?, Error?) -> Void)?){
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key , value) in parameters {
                if let data:Data = value as? Data {
                    multipartFormData.append(data, withName: key, fileName: "\(data.getExtension)", mimeType: "\(data.mimeType)")
                } else {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                }
            }
        },
                         to: url, method: .post,
                         headers: [:])
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("progress::\(progress)")
                })
                upload.responseData(completionHandler: { response in
                    
                    if response.result.isSuccess {
                        if let value: Any = response.result.value as AnyObject? {
                            let response = JSON(value)
                            print(response)
                            callback?(response, nil)
                            
                        }
                    }
                    else {
                        print("FAILURE")
                        print("\(String(describing: (response.error?.localizedDescription)!))")
                        //print(response.error)
                        callback?(nil, response.error)
                        
                    }
                })
                
            case .failure(let encodingError):
                print("Fail:: \(encodingError.localizedDescription)")
                callback?(nil, encodingError)
                
                break
            }
        }
        
        
        
    }
    
    // MARK:- image And Param On server
    static func uploadImageAndParam(url: String,image: UIImage,parameters: [String: Any], callback:((JSON?, Error?) -> Void)?){
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key,value) in parameters {
               
                multipartFormData.append(("\(value)" as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                
                if key == "image"{
                    multipartFormData.append(UIImageJPEGRepresentation(image,  0.025)!, withName: "image", fileName: "newImage.jpeg", mimeType: "image/jpeg")
                }
            }
            
        },
                         to: url, method: .post,
                         headers: [:])
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("progress::\(progress)")
                })
                upload.responseData(completionHandler: { response in
                    
                    if response.result.isSuccess {
                        if let value: Any = response.result.value as AnyObject? {
                            let response = JSON(value)
                            print(response)
                            callback?(response, nil)
                            
                        }
                    }
                    else {
                        print("FAILURE")
                        print("\(String(describing: (response.error?.localizedDescription)!))")
                        //print(response.error)
                        callback?(nil, response.error)
                        
                    }
                })
                
            case .failure(let encodingError):
                print("Fail:: \(encodingError.localizedDescription)")
                callback?(nil, encodingError)
                
                break
            }
        }
        
        
        
    }
    
    
    
    
    
    static func postImageOnServer(url: String,data: Data, fileName: String, parameters: [String: Any], callback:((JSON?, Error?) -> Void)?){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
           
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                
            }
            multipartFormData.append(data, withName: "image", fileName: fileName,mimeType: "image/jpeg")
        }, to:url)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    //self.delegate?.showSuccessAlert()
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    //                        self.showSuccesAlert()
                    //self.removeImage("frame", fileExtension: "txt")
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                    }
                }
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                print(encodingError)
            }
            
        }
        
    }
    
    
    
    static func postDataWithImage(url: String,data: Data, fileName: String, parameters: [String: Any], callback:((JSON?, Error?) -> Void)?){
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key,value) in parameters {
                multipartFormData.append(("\(value)" as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                if key == "image"{
                    multipartFormData.append(("\(value)" as AnyObject).data(using : String.Encoding.utf8.rawValue)!, withName: "image", fileName: fileName,mimeType: "image/jpeg")
                    print("key:\(key)")
                    print("value:\(value)")
                }
                
            }
            multipartFormData.append(data, withName: "file", fileName: fileName,mimeType: "image/jpeg")
        },
                         to: url, method: .post,
                         headers: [:])
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("progress::\(progress)")
                })
                upload.responseData(completionHandler: { response in
                    
                    if response.result.isSuccess {
                        if let value: Any = response.result.value as AnyObject? {
                            let response = JSON(value)
                            print(response)
                            callback?(response, nil)

                        }
                    }
                    else {
                        print("FAILURE")
                        print("\(String(describing: (response.error?.localizedDescription)!))")
                        //print(response.error)
                        callback?(nil, response.error)

                    }
                })
                
            case .failure(let encodingError):
                print("Fail:: \(encodingError.localizedDescription)")
                callback?(nil, encodingError)
                
                break
            }
        }
        
        
        
    }
    
    
    static func PostRequestwithData(fromSavedUrl url: String, parameters: [String: Any], callback: ((JSON?, Error?) -> Void)?) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
//                if let document = value as? Document {
//                    if document.type == CollectionViewType.DOCUMENT {
//                        multipartFormData.append(document.data, withName: key, fileName: "\(key).pdf", mimeType: "application/pdf")
//                    } else if document.type == CollectionViewType.VIDEO {
//                        multipartFormData.append(document.data, withName: key, fileName: "\(key).mp4", mimeType: "video/mp4")
//                    } else {
//                        multipartFormData.append(document.data, withName: key, fileName: "\(key).jpg", mimeType: "image/jpg")
//                    }
//                    print("Key::\(key) -> Value::\(document.type)")
//                }
//                else {
                    multipartFormData.append(("\(value)" as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    print("Key::\(key) -> Value::\(value)")
//                }
                
            }
        }, to: url, method: .post,
           headers: [:])
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("progress::\(progress)")
                })
                upload.responseData(completionHandler: { response in
                    
                    if response.result.isSuccess {
                        if let value: Any = response.result.value as AnyObject? {
                            let response = JSON(value)
                            callback?(response, nil)
                        }
                    }
                    else {
                        print("FAILURE")
                        print("\(String(describing: (response.error?.localizedDescription)!))")
                        callback?(nil, response.error)
                    }
                })
                
            case .failure(let encodingError):
                print("Fail:: \(encodingError.localizedDescription)")
                callback?(nil, encodingError)
                
                break
            }
        }
    }
}
 public extension Data {
    public var mimeType:String {
        get {
            var c = [UInt32](repeating: 0, count: 1)
            (self as NSData).getBytes(&c, length: 1)
            switch (c[0]) {
            case 0xFF:
                return "image/jpeg";
            case 0x89:
                return "image/png";
            case 0x47:
                return "image/gif";
            case 0x49, 0x4D:
                return "image/tiff";
            case 0x25:
                return "application/pdf";
            case 0xD0:
                return "application/vnd";
            case 0x46:
                return "text/plain";
            default:
                print("mimeType for \(c[0]) in available");
                //return "application/octet-stream";
                return "image/png";
            }
        }
    }
    public var getExtension: String {
        get {
            var c = [UInt32](repeating: 0, count: 1)
            (self as NSData).getBytes(&c, length: 1)
            switch (c[0]) {
            case 0xFF:
                return "_IMG.jpeg";
            case 0x89:
                return "_IMG.png";
            case 0x47:
                return "_IMG.gif";
            case 0x49, 0x4D:
                return "_IMG.tiff";
            case 0x25:
                return "_FILE.pdf";
            case 0xD0:
                return "_FILE.vnd";
            case 0x46:
                return "_FILE.txt";
            default:
                print("mimeType for \(c[0]) in available");
                //return "_video.mp4";
                return "_IMG.png"
            }
        }
    }
 }
 

