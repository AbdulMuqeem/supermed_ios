//
//  UserServices.swift
//  Syllabit
//
//  Created by Protege Global on 22/03/2018.
//  Copyright © 2018 Protege Global. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserServices {
    
    
    //MARK:-  Login Service Method
    
    static func Login(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.login, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                Alert.showAlert(title: "Login", message:"\(msg!)" )
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    
    
    
    //MARK:-  SignUp Service Method
    
    static func SignUp(param:[String:Any],
                       completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.signup, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                             return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  LoginWSocialMedia Service Method
    
    static func LoginWSocialMedia(param:[String:Any],
                                  completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.socialLogin, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].stringValue != "true" {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            guard let response_server = response else {return}
            completionHandler(true, response_server, nil)
        })
    }
    
    //MARK:-  Update Profile Service Method
    
    static func Update(param:[String:Any],
                               completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.updateProfile, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].stringValue != "success" {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    // MARK:- Update Picture User
    static func changeProfilePicture(param:[String:Any], fileName: String, ImageDate: Data,
                       completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.changePicture, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].stringValue != "success" {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    
    // MARK: - Forgot Password Services
    
    static func forgotPassword(param:[String:Any],
                               completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.forgotPassword, parameters: param, callback: {(response, error) in
            
            Alert.hideLoader()
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response!["status"].stringValue != "success" {
                let msg = response!["message"].stringValue
                print("Message: \(String(describing: msg))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: msg)
                return
            }
            
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Change Password Service Method
    
    static func ChangePassword(param:[String:Any],
                      completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.changePassword, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                print(response)
                
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].boolValue != true {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Delete User Service Method
    
//    static func DeleteUser(param:[String:Any],
//                               completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
//
//        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.deleteUser, parameters: param, callback: { (response, error) in
//
//            Alert.hideLoader()
//
//            if error != nil {
//                print("Error: \(String(describing: (error?.localizedDescription)!))")
//                completionHandler(false, nil, error)
//                return
//            }
//
//            if response?["status"].stringValue != "success" {
//                let msg = response?["message"].stringValue
//                print("Message: \(String(describing: msg!))")
//                completionHandler(false, response!, nil)
//
//                Alert.showAlert(title: "Alert", message: msg!)
//                return
//            }
//            completionHandler(true, response!, nil)
//        })
//    }
    
    //MARK:- Reset Password Service Method
    
    static func ResetPassword(param:[String:Any],
                           completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.resetPassword, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].stringValue != "success" {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
}
