//
//  FriendServices.swift
//  Syllabit
//
//  Created by Protege Global on 04/04/2018.
//  Copyright © 2018 Protege Global. All rights reserved.
//

import Foundation
import SwiftyJSON

class FriendServices {
    
    //MARK:- Friend List Service Method
    
//    static func FriendList(param:[String:Any],
//                               completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
//        
//        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.getUser, parameters: param, callback: { (response, error) in
//            
//            Alert.hideLoader()
//            
//            if error != nil {
//                print("Error: \(String(describing: (error?.localizedDescription)!))")
//                completionHandler(false, nil, error)
//                return
//            }
//            
//            if response?["status"].stringValue != "success" {
//                let msg = response?["message"].stringValue
//                print("Message: \(String(describing: msg!))")
//                completionHandler(false, response!, nil)
//                
//                Alert.showAlert(title: "Alert", message: msg!)
//                return
//            }
//            completionHandler(true, response!, nil)
//        })
//    }
    
    //MARK:- Request Friend List Service Method
    
    static func RequestFriendList(param:[String:Any],
                           completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.requestList, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].stringValue != "success" {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }

    //MARK:- My Friend List Service Method
    
//    static func MyFriendList(param:[String:Any],
//                                  completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
//
//        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.myFriend, parameters: param, callback: { (response, error) in
//
//            Alert.hideLoader()
//
//            if error != nil {
//                print("Error: \(String(describing: (error?.localizedDescription)!))")
//                completionHandler(false, nil, error)
//                return
//            }
//
//            if response?["status"].stringValue != "success" {
//                let msg = response?["message"].stringValue
//                print("Message: \(String(describing: msg!))")
//                completionHandler(false, response!, nil)
//
//                Alert.showAlert(title: "Alert", message: msg!)
//                return
//            }
//            completionHandler(true, response!, nil)
//        })
//    }
    
    //MARK:- Add Friend List Service Method
    
//    static func AddFriendRequest(param:[String:Any],
//                           completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
//        
//        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.addFriend, parameters: param, callback: { (response, error) in
//            
//            Alert.hideLoader()
//            
//            if error != nil {
//                print("Error: \(String(describing: (error?.localizedDescription)!))")
//                completionHandler(false, nil, error)
//                return
//            }
//            
//            if response?["status"].stringValue != "success" {
//                let msg = response?["message"].stringValue
//                print("Message: \(String(describing: msg!))")
//                completionHandler(false, response!, nil)
//                
//                Alert.showAlert(title: "Alert", message: msg!)
//                return
//            }
//            completionHandler(true, response!, nil)
//        })
//    }
    
    //MARK:- Accept Request Service Method
    
//    static func AcceptRequest(param:[String:Any],
//                                 completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
//
//        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.acceptRequest, parameters: param, callback: { (response, error) in
//
//            Alert.hideLoader()
//
//            if error != nil {
//                print("Error: \(String(describing: (error?.localizedDescription)!))")
//                completionHandler(false, nil, error)
//                return
//            }
//
//            if response?["status"].stringValue != "success" {
//                let msg = response?["message"].stringValue
//                print("Message: \(String(describing: msg!))")
//                completionHandler(false, response!, nil)
//
//                Alert.showAlert(title: "Alert", message: msg!)
//                return
//            }
//            completionHandler(true, response!, nil)
//        })
//    }
    
    //MARK:- Decline Request Service Method
    
    static func DeclineRequest(param:[String:Any],
                              completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.declineRequest, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].stringValue != "success" {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: msg!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
}
