//
//  Images.swift
//
//  Created by Protege Global on 23/05/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Images: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let imageName = "ImageName"
    static let taskId = "TaskId"
    static let dateAdded = "DateAdded"
    static let imageId = "ImageId"
  }

  // MARK: Properties
  public var imageName: String?
  public var taskId: String?
  public var dateAdded: String?
  public var imageId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    imageName = json[SerializationKeys.imageName].string
    taskId = json[SerializationKeys.taskId].string
    dateAdded = json[SerializationKeys.dateAdded].string
    imageId = json[SerializationKeys.imageId].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = imageName { dictionary[SerializationKeys.imageName] = value }
    if let value = taskId { dictionary[SerializationKeys.taskId] = value }
    if let value = dateAdded { dictionary[SerializationKeys.dateAdded] = value }
    if let value = imageId { dictionary[SerializationKeys.imageId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.imageName = aDecoder.decodeObject(forKey: SerializationKeys.imageName) as? String
    self.taskId = aDecoder.decodeObject(forKey: SerializationKeys.taskId) as? String
    self.dateAdded = aDecoder.decodeObject(forKey: SerializationKeys.dateAdded) as? String
    self.imageId = aDecoder.decodeObject(forKey: SerializationKeys.imageId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(imageName, forKey: SerializationKeys.imageName)
    aCoder.encode(taskId, forKey: SerializationKeys.taskId)
    aCoder.encode(dateAdded, forKey: SerializationKeys.dateAdded)
    aCoder.encode(imageId, forKey: SerializationKeys.imageId)
  }

}
