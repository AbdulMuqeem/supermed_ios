//
//  UserManager.swift
//  AdHouse
//
//  Created by muhammadmaazulhaq on 04/08/2017.
//  Copyright © 2017 Abdul Muqeem. All rights reserved.
//

import UIKit
import Foundation

class UserManager {

    var sessionDeviceToken: String?

    static func saveUserObjectToUserDefaults(userResult: Profile){
    let archiveData = NSKeyedArchiver.archivedData(withRootObject: [userResult])
        UserDefaults.standard.set(archiveData, forKey: User_data_userDefault)
    }

    static func removeUserObjectFromUserDefaults() {
        UserDefaults.standard.set(nil, forKey: User_data_userDefault)
        UserDefaults.standard.removeObject(forKey: User_data_userDefault)
        UserDefaults.standard.synchronize()
    }

    static func getUserObjectFromUserDefaults()->Profile? {

        let userData  = UserDefaults.standard.value(forKey: User_data_userDefault)

        
        
        if userData != nil {
            let userDataObj = NSKeyedUnarchiver.unarchiveObject(with: userData as! Data) as! [Profile]
            if let user = userDataObj as? [Profile]{
                return userDataObj[0]
            }
         
        }
        else{
            return nil
        }
    }

    static func isUserLogin()->Bool{
        if  self.getUserObjectFromUserDefaults() != nil{
            return true
        }
        else{
            return false
        }

    }

}

