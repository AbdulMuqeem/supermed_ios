//
//  ServiceApiEndPoints.swift
//  Syllabit
//
//  Created by Protege Global on 22/03/2018.
//  Copyright © 2018 Protege Global. All rights reserved.
//

import Foundation

 class ServiceApiEndPoints: NSObject  {
    //http://40.113.216.40:8087
    
    //"https://www.protegeglobal.com/supermed/ecommerce/api/api"
    // https://protegeglobal.com/inventoryM/supermed/api/api  // Use this from now on
    // https://www.protegeglobal.com/supermed/ecommerce/api/api
    
   // http://66.171.248.10:8087/api/api/signupUser
   
    static let baseURL = "https://www.supermed.pk/api/api"

    
    // User Module
    static let login = baseURL + "/getUserByEmailandPass"
    static let signup = baseURL + "/signupUser"
    static let socialLogin = baseURL + "/socialLogin"
    static let updateProfile = baseURL + "/updateProfileUser"
    static let changePassword = baseURL + "/changePassword"
    static let forgotPassword = baseURL + "/forgotPass"
    static let resetPassword = baseURL + "/resetpassword"
    static let changePicture = baseURL + "/uploadpicture"  /* userid, image*/
   // http://66.171.248.10:8087/api/api/getBanners
    
    // My Syllabi
    static let getBanners = baseURL +  "/getBanners"
    static let getCategories = baseURL +  "/getCategories"
    static let getCategoriesDetail = baseURL + "/getSubCategoriesById"
    static let getProductSubCategories = baseURL + "/getProductsBySubCatId"
    static let getProducts = baseURL + "/getProducts"
    
    // Doctor Info controller
    static let getSpecialization = baseURL + "/getSpecialization"
    static let list_of_doctor = baseURL + "/list_of_doctor"
    static let list_of_doctorById = baseURL + "/list_of_doctorById"
    
    static let addWishlist = baseURL + "/add_wish"
    static let getWishlist = baseURL + "/get_wish"
    static let deleteWishlist = baseURL + "/delete_wish"
    static let moveWishlist = baseURL + "/move_wish"
    static let cart_Action = baseURL + "/cart_action"
    static let get_cart = baseURL +  "/get_cart"
    static let validate_coupon = baseURL +  "/valCoupon"
    static let getCountries = baseURL + "/getCountries"
    static let getCities = baseURL + "/getCities"
    static let getEmergency = baseURL + "/getEmergency"
    
    // LAB TEST
    static let get_list_lab = baseURL + "/list_lab"
    static let load_lab_test = baseURL + "/load_lab_test"
    static let list_test = baseURL + "/list_test"
    static let book_appointment = baseURL + "/book_appointment"
    
    
    // Search
    static let  searchRelative = baseURL + "/searchMed"
    static let uploadPrescription = baseURL + "/uploadPrescription"
    static let appointmentHistory = baseURL + "/history_appointment"
    static let checkout = baseURL + "/checkOut"
    
    // Friend
    static let contactus = baseURL + "/getContactUS"
    static let subscribeNewsLetter = baseURL + "/SubscribeNewsLetter"
    static let searchAlternateMed = baseURL + "/searchAlternateMed"
    
    static let requestList = baseURL + "/MyFriendRequest"
    static let acceptRequest = baseURL + "/AcceptFriend"
    static let declineRequest = baseURL + "/DeclineFriend"
    
    // Notification
    static let updateNotification = baseURL + "/updatepushsettings"
    static let getGuestUserID = baseURL + "/getGuestUserID"
    //CMS
    static let get_terms = baseURL +  "/getTermStatus"
    
    //Order History
    static let list_of_order = baseURL + "/list_of_order"
    
    // Help
    static let getTopics = baseURL + "/getTopics"
    static let getHelpCenterByTopic = baseURL + "/getHelpCenterByTopic"
}
